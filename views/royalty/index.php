<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RoyaltySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Nhuận bút';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="royalty-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <label class="form-check-label">
            <input type="checkbox" class="form-check-input" id="check-all">
            Chọn tất cả
        </label>
        <?= Html::a('Gửi email', false, ['class' => 'btn btn-success', 'id' => 'btn-send-mail']) ?>
        <?= Html::a('Import nhuận bút', ['royalty-file-import/create'], ['class' => 'btn btn-primary']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            ['attribute' => '', 'format' => 'raw', 'value' => function($model) {
                return '<div class="form-check">
                            <input type="checkbox" class="form-check-input check-send-mail" value='.$model->id.' />
                        </div>';
            }],
            'full_name',
            'address:ntext',
            'royalty_month',
            'royalty_week',
            //'article_count',
            //'post_count',
            //'photo_count',
            //'rate',
            //'royalty_amount',
            //'tax_amount',
            //'royalty_total_amount',
            ['attribute' => 'Thực nhận', 'format' => 'raw', 'value' => function($model) {
                return Yii::$app->formatter->asDecimal($model->royalty_total_amount,0);
            }],
            // 'paid_type',
            'email:email',
            ['attribute' => 'Trạng thái', 'format' => 'raw', 'value' => function($model) {
                $label = 'label-danger';
                if(isset(app\models\Royalty::getSendLabels()[$model->is_sended]) 
                        && $model->is_sended == app\models\Royalty::IS_SENDED) {
                    $label = 'label-success';
                }
                return isset(app\models\Royalty::getSendLabels()[$model->is_sended])?
                '<span class="label '.$label.'">'.app\models\Royalty::getSendLabels()[$model->is_sended].'</span>':'';
            }],
            // 'created_by',
            // 'updated_by',
            // 'created_at',
            // 'is_sended',
            // 'sended_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Email đang được gửi đi...</h4>
            </div>
            <div class="modal-body">
                Bạn vui lòng không đóng trình duyệt khi quá trình gửi email chưa hoàn tất...
            </div>
        </div>
    </div>
</div>

<?php 
$sendMailUrl = '/royalty/ajax-send-mail';
$formatJs = "$('#check-all').click(function(event) {   
    if(this.checked) {
        $('.check-send-mail').each(function() {
            this.checked = true;                        
        });
    } else {
        $('.check-send-mail').each(function() {
            this.checked = false;                        
        });
    }
});

$('#btn-send-mail').click(function(){
    var ids = [];
    $('.check-send-mail').each(function() {
        if(this.checked == true) {
            ids.push($(this).val());
        }
    });
    if(ids.length > 0) {
        console.log(ids);
        $.ajax({
            type: 'POST',
            data: {ids:ids},
            url: '".$sendMailUrl."',
            beforeSend: function() {
                $('#myModal').modal('show');
            },
            success: function(msg){
                $('#myModal').modal('hide');
                if(typeof msg.error != 'undefined' && msg.error == 0) {
                    alert('Đã gửi email thành công!');
                } else {
                    alert('Quá trình gửi mail đã xảy ra lỗi, bạn vui lòng thử lại sau!');
                }
            }
        });
    } else {
        alert('Bạn chưa chọn đối tượng để gửi mail');
    }
});";

$this->registerJs($formatJs, View::POS_END);
?>
