<?php

namespace app\controllers;

use Yii;
use app\models\FelicitationDiscipline;
use app\models\FelicitationDisciplineSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \app\models\EmployeeFelicitationDiscipline;
use yii\web\UploadedFile;
use app\components\AdminController;
/**
 * FelicitationDisciplineController implements the CRUD actions for FelicitationDiscipline model.
 */
class FelicitationDisciplineController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FelicitationDiscipline models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FelicitationDisciplineSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FelicitationDiscipline model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $employeeIds = $model->getEmployeeAssign();
        $assignString = "Không có";
        if(!empty($employeeIds)) {
            $m = new \app\models\Employee();
            $employeeData = $m->findAll($employeeIds);
            if($employeeData) {
                $dat = [];
                foreach($employeeData as $data) {
                    $dat[] = '<span class="label label-info"><a href="'.\Yii::$app->urlManager->createUrl(['/employee/view','id' => $data->id]).'">'.$data->full_name.'</a></span>';
                }
                $assignString = implode(" ", $dat);
            }
        }
        return $this->render('view', [
            'model' => $model,
            'employeeAssign' => $assignString
        ]);
    }

    /**
     * Creates a new FelicitationDiscipline model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FelicitationDiscipline();

        if ($model->load(Yii::$app->request->post())) {
            $model->created_at = time();
            $model->updated_at = time();
            $model->templateFile = UploadedFile::getInstance($model, 'templateFile');
            if($model->templateFile) {
                $model->upload();
            }
            if($model->save()) {
                $post = Yii::$app->request->post('FelicitationDiscipline');
                
                if(isset($post['EmployeeAssignId']) && !empty($post['EmployeeAssignId'])) {
                    $lesAssign = [];
                    foreach($post['EmployeeAssignId'] as $employeeId) {
                        $lesAssign[$employeeId] = $employeeId;
                    }
                    foreach($lesAssign as $eid) {
                        $m = new EmployeeFelicitationDiscipline();
                        $m->employee_id = $eid;
                        $m->felicitation_discipline_id = $model->id;
                        $m->felicitation_discipline_number = $model->number;
                        $m->felicitation_discipline_type = $model->type;
                        $m->created_at = time();
                        $m->updated_at = time();
                        $m->save();
                    }
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing FelicitationDiscipline model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        $m = new EmployeeFelicitationDiscipline();
        $mData = $m->findAll(['felicitation_discipline_id' => $id]);
       
        if ($model->load(Yii::$app->request->post())) {
            $model->updated_at = time();
            $model->templateFile = UploadedFile::getInstance($model, 'templateFile');
            if($model->templateFile) {
                $model->upload();
            }
            if($model->save()) {
                $post = Yii::$app->request->post('FelicitationDiscipline');
                
                if(isset($post['EmployeeAssignId']) && !empty($post['EmployeeAssignId'])) {
                    $lesAssign = [];
                    $lesCurent = [];
                    $uAss = [];
                    $uCur = [];
                    foreach($post['EmployeeAssignId'] as $employeeId) {
                        $lesAssign[$employeeId] = $employeeId;
                    }
                    foreach($lesAssign as $v1) {
                        $uAss[] = $v1;
                    }
                    
                    if($mData) {
                        foreach($mData as $md) {
                            $lesCurent[$md->employee_id] = $md->employee_id;
                        }
                        foreach($lesCurent as $v2) {
                            $uCur[] = $v2;
                        }
                    }
                    
                    $delete = false;
                    if(!empty($uCur)) {
                        if(sizeof($uCur) != sizeof($uAss)) {
                            $delete = true;
                        } else {
                            $result = array_diff($uCur, $uAss);
                            if(!empty($result)) {
                                $delete = true;
                            }
                        }
                    }  else {
                        foreach($lesAssign as $eidf) {
                            $mxf = new EmployeeFelicitationDiscipline();
                            $mxf->employee_id = $eidf;
                            $mxf->felicitation_discipline_id = $model->id;
                            $mxf->felicitation_discipline_number = $model->number;
                            $mxf->felicitation_discipline_type = $model->type;
                            $mxf->created_at = time();
                            $mxf->updated_at = time();
                            $mxf->save();
                        }
                    }
                    
                    if($delete) {
                        $dd = EmployeeFelicitationDiscipline::deleteAll(['felicitation_discipline_id' => $id]);
                        if($dd) {
                            foreach($lesAssign as $eid) {
                                $mx = new EmployeeFelicitationDiscipline();
                                $mx->employee_id = $eid;
                                $mx->felicitation_discipline_id = $model->id;
                                $mx->felicitation_discipline_number = $model->number;
                                $mx->felicitation_discipline_type = $model->type;
                                $mx->created_at = time();
                                $mx->updated_at = time();
                                $mx->save();
                            }
                        }
                    }
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $eData = [];
            if($mData) {
                $employeeIds = [];
                foreach($mData as $dat) {
                    $employeeIds[] = $dat->employee_id;
                }
                $employees = \app\models\Employee::findAll($employeeIds);
                if($employees) {
                    foreach($employees as $e) {
                        $eData[] = ['employee_id' => $e->id, 'full_name' => $e->full_name, 'felicitation_discipline_id' => $id];
                    }
                }
            }
            
            return $this->render('update', [
                'model' => $model,
                'assignData' => $eData
            ]);
        }
    }

    /**
     * Deletes an existing FelicitationDiscipline model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();
        $m = $this->findModel($id);
        if($m->status != FelicitationDiscipline::STATUS_INACTIVE) {
            $m->status = FelicitationDiscipline::STATUS_INACTIVE;
            $m->save(true,['status']);
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the FelicitationDiscipline model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FelicitationDiscipline the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FelicitationDiscipline::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
