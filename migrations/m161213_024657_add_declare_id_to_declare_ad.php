<?php

use yii\db\Migration;

class m161213_024657_add_declare_id_to_declare_ad extends Migration
{
    public function up()
    {
        $this->addColumn('declare_ad', 'declare_ad_code', $this->string());
    }

    public function down()
    {
        $this->dropColumn('declare_ad', 'declare_ad_code');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
