<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ActionLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Action Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="action-log-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'action',
            'object_data',
            'result_data',
            'action_user',
            // 'created_at',
            ['attribute' => '', 'format' => 'raw', 'value' => function($model) {
                return date('d-m-Y h:i:s', $model->created_at);                
            }],

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
