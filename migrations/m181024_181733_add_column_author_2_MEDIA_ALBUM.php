<?php

use yii\db\Migration;

/**
 * Class m181024_181733_add_column_author_2_MEDIA_ALBUM
 */
class m181024_181733_add_column_author_2_MEDIA_ALBUM extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('media_album', 'author_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('media_album', 'author_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181024_181733_add_column_author_2_MEDIA_ALBUM cannot be reverted.\n";

        return false;
    }
    */
}
