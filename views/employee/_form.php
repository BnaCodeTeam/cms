<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>
<?php if(Yii::$app->session->hasFlash('error')):?>
<div class="alert alert-danger" role="alert">
    <?= Yii::$app->session->getFlash('error'); ?>
</div>
<?php endif;?>
<?php if(Yii::$app->session->hasFlash('success')):?>
<div class="alert alert-success" role="alert">
    <?= Yii::$app->session->getFlash('success'); ?>
</div>
<?php endif;?>
<div class="user-form">
<!--<div class="row">-->
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        
        <?php $options = ['maxlength' => true]; !$model->isNewRecord? $options = $options + ['readonly' => true]:'';?>   
        <div class="table-responsive"> 
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr> 
                        <td>
                            <div class="form-group">
                                <?php echo $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>                
                            </div>
                        </td> 
                        <td>
                            <div class="form-group">
                                <?= $form->field($model,'birthString')->widget(DatePicker::className(),['dateFormat' => 'dd-MM-yyyy','options' => ['class' => 'form-control'],'clientOptions' => ['defaultDate' => '01-01-2016']]) ?>
                            </div>
                        </td> 
                        <td>
                            <div class="form-group">
                                <?php echo $form->field($model, 'sex')->dropDownList(app\models\Employee::getSexLabels(),['prompt'=>'---- Chọn giới tính ----']) ?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group">
                                <?php echo $form->field($model, 'id_number')->textInput();?>
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <?= $form->field($model,'idNumberTimeString')->widget(DatePicker::className(),['dateFormat' => 'dd-MM-yyyy','options' => ['class' => 'form-control'],'clientOptions' => ['defaultDate' => '01-01-2016']]) ?>
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <?php echo $form->field($model, 'id_number_place')->textInput();?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group">
                                <?php echo $form->field($model, 'birthplace')->textInput();?>
                            </div>
                        </td>
                        <td colspan="2">
                            <div class="form-group">
                                <?php echo $form->field($model, 'resident')->textInput();?>
                            </div>
                        </td>
                    </tr>
                     <tr>
                        <td colspan="3">
                            <div class="form-group">
                                <?php echo $form->field($model, 'native_land')->textInput();?>
                            </div>
                        </td>
                    </tr>
                    
                    <tr>
                        <?php if($model->isNewRecord && isset($userType) && $userType == app\models\Employee::TYPE_CTV):?>
                        <?php else:?>
                        <td>
                            <div class="form-group">
                                <?php echo $form->field($model, 'username')->textInput($options) ?>  
                            </div>
                        </td>
                        <?php endif;?>
                        <td>
                            <div class="form-group">
                                <?php echo $form->field($model, 'email')->textInput(['maxlength' => true]) ?>                
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <?php echo $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group">
                                <?= $form->field($model,'joinedDateString')->widget(DatePicker::className(),['dateFormat' => 'dd-MM-yyyy','options' => ['class' => 'form-control'],'clientOptions' => ['defaultDate' => '01-01-2016']]) ?>
                            </div>
                        </td>
                        <td colspan="2">
                            <div class="form-group">
                                <?php echo $form->field($model, 'address')->textInput();?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group">
                                <?php echo $form->field($model, 'bank_account')->textInput() ?>                
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <?php echo $form->field($model, 'bank_id')->dropDownList(app\models\Employee::getBankLabels())?>
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <?php echo $form->field($model, 'employee_coefficient')->textInput();?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group">
                                <?php echo $form->field($model, 'ethnic_group')->textInput(['maxlength' => true]) ?>                
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <?php echo $form->field($model, 'religion')->textInput(['maxlength' => true]) ?>
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <?php echo $form->field($model, 'health')->textInput();?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group">
                                <?php echo $form->field($model, 'height')->textInput(['placeholder' => 'Cm']);?>
                            </div>
                        </td>
                        <td >
                            <div class="form-group">
                                <?php echo $form->field($model, 'weight')->textInput(['placeholder' => 'Kg']);?>
                            </div>
                        </td>
                        <td >
                            <div class="form-group">
                                <?php echo $form->field($model, 'blood_group')->dropDownList(app\models\Employee::getBloodGroupLabels(), ['prompt' => '---- Chọn nhóm máu ----']);?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group">
                                <?= $form->field($model, 'imageFile')->fileInput() ?>
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <div class="media-left">
                                    <a href="#"> 
                                        <img alt="64x64" class="media-object" data-src="holder.js/64x64" src="<?php echo $model->image?'/'.$model->image:'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTZkNTlmMjI5ZSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1NmQ1OWYyMjllIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMy40Njg3NSIgeT0iMzYuMzk2ODc1Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==' ?>" data-holder-rendered="true" style="width: 64px; height: 64px;"> 
                                    </a>
                                </div>
                            </div>
                        </td>
                        <?php if(!$model->isNewRecord):?>
                        <td>
                            <div class="form-group">
                                <?php echo $form->field($model, 'user_type')->dropDownList(app\models\Employee::getUserTypeLabels(),['prompt'=>'---- Chọn đối tượng ----']) ?>
                            </div>
                        </td>
                        <?php endif;?>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div class="form-group" style="margin-bottom: 0px;">
                                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Thêm mới') : Yii::t('app', 'Cập nhật'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                            </div>
                        </td>
                    </tr>
                    
                </tbody>
            </table>
        </div>
        <?php ActiveForm::end(); ?>
<!--</div>-->
</div>
