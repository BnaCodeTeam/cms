<?php

use yii\db\Migration;

class m161020_110147_add_column_deploy_employee extends Migration
{
    public function up()
    {
        $this->addColumn('declare_ad', 'deploy_employee', yii\db\Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn('declare_ad', 'deploy_employee');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
