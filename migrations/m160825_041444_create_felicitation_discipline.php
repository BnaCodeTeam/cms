<?php

use yii\db\Migration;

class m160825_041444_create_felicitation_discipline extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('felicitation_discipline', [
            'id' => $this->primaryKey(),
            //'employee_id' => $this->integer()->notNull(),
            'type' => $this->integer()->notNull(),
            'number' => $this->string()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'sign_time' => $this->integer(),
            'title' => $this->string(),
            'content' => $this->text(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        return $this->dropTable("felicitation_discipline");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
