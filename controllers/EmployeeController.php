<?php

namespace app\controllers;

use Yii;
use app\models\Employee;
use app\models\EmployeeSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;

use yii\web\UploadedFile;
use yii\db\Query;
// thay doi ne
/**
 * UserController implements the CRUD actions for User model.
 */
class EmployeeController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmployeeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex2()
    {
        $searchModel = new EmployeeSearch();
        $dataProvider = $searchModel->search2(Yii::$app->request->queryParams);

        return $this->render('index2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        //return true;
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Employee();
        
        $userType = Yii::$app->request->get('user_type',Employee::TYPE_NV);
        
        if ($model->load(Yii::$app->request->post())) { 
            
            if($userType == Employee::TYPE_CTV) {
                $model->username = $model->email;
                $model->password_hash = $model->email;
            } else {
                $model->password_hash = Employee::DEFAULT_PASSWORD;
            }
            
            $model->setPassword($model->password_hash);
            $model->generateAuthKey();
            $model->status = Employee::STATUS_ACTIVE;
            $model->created_at = time();
            $model->updated_at = time();
            
            $model->user_type = $userType;
            
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if($model->imageFile) {
                $model->upload();
            }
            
            $ss = '';
            if($userType != Employee::TYPE_CTV) {
                $ss = '. Mật khẩu: ' . Employee::DEFAULT_PASSWORD;
            }
            
            if ($model->save()) {
                
                if(Yii::$app->user->identity->username == 'admin') {
                    Yii::$app->session->setFlash('success', 'Tạo mới nhân viên thành công' . $ss .'. Bạn có thể cấp quyền cho nhân viên này.');
                    return $this->redirect(['auth-management/assign', 'id' => $model->id]);
                }
                Yii::$app->session->setFlash('success', 'Tạo mới nhân viên thành công');
                return $this->redirect(['employee/create']);
            } else {
                $errorMessage = "";
                foreach ($model->getErrors() as $er) {
                    if(sizeof($er) > 0) {
                        $errorMessage = $errorMessage. "<li>" . $er[0] . "</li>";
                    }
                }
                if($errorMessage != "") {
                    Yii::$app->session->setFlash('error', $errorMessage);
                }
            }                      
        } 
        return $this->render('create', [
            'model' => $model,
            'userType' => $userType,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if($model->imageFile) {
                $model->upload();
            }
            if($model->save()) {
                Yii::$app->session->setFlash('success', 'Cập nhật thành công');
                return $this->redirect(['index']);
            }
        } 
        return $this->render('update', [
            'model' => $model,
        ]);
    }
    
    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();
        $m = $this->findModel($id);
        $m->status = \app\models\Employee::STATUS_DELETED;
        if($m->save(true, ['status'])) {
            return $this->redirect(['index']);
        }        
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Employee::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * 
     * @param type $id
     * @return type
     */
    public function actionUpdatePassword($id) {
        $resetPasswordModel = new \app\models\ResetPasswordForm();
        $user = $this->findModel($id);
        if($resetPasswordModel->load(Yii::$app->request->post())) {
            if($resetPasswordModel->validate(['change_password','change_password_repeate'])) {
                $user->setPassword($resetPasswordModel->change_password);
                $user->generateAuthKey();
                $user->reset_password_require = Employee::RESET_PWD_REQUIRE;
                if ($user->save(true, ['password_hash','auth_key','reset_password_require'])) {
                    if ($user->id == Yii::$app->user->id) {
                        Yii::$app->user->logout();
                        return $this->redirect(['/site/login']);
                    }
                    Yii::$app->session->setFlash('success', 'Cập nhật mật khẩu thành công');
                    return $this->redirect(['/employee/index']);
                } else {
                    Yii::$app->session->setFlash ('error','Không thay đổi được mật khẩu');
                }
            }
        }
        return $this->render('update_password', [
            'model' => $resetPasswordModel,
            'user' => $user->username
        ]);
    }


    public function actionResetPassword() {
        $this->checkLogin();
        $resetPasswordModel = new \app\models\ResetPasswordForm();
        if ($resetPasswordModel->load(Yii::$app->request->post())) {
            $user = $this->findModel(Yii::$app->user->id);
            if ($resetPasswordModel->validate()) {
                $user->setPassword($resetPasswordModel->change_password);
                $user->generateAuthKey();
                if($user->reset_password_require == Employee::RESET_PWD_REQUIRE) {
                    $user->reset_password_require = Employee::RESET_PWD_NOT_REQUIRE;
                }
                if ($user->save(true, ['password_hash','auth_key', 'reset_password_require'])) {
                    Yii::$app->user->logout();
                    return $this->redirect(['/site/login']);
                } else {
                    Yii::$app->session->setFlash ('error','Không thay đổi được mật khẩu');
                }
            }
        }
        return $this->render('change_password', [
            'model' => $resetPasswordModel
        ]);
    }

    public function actionAjaxGetEmployeeList($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '', 'image' => '']];
        if (!is_null($q)) {
            $query = new \yii\db\Query();
            $query->select('id, username, full_name as text, image')
                ->from('employee')
                ->where(['like', 'full_name', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Employee::find($id)->full_name, 'image' => Employee::find($id)->image];
        }
        return $out;
    }
    
    public function actionAjaxGetEmployeeBirthday () {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        //\Yii::$app->db->createCommand("SET time_zone = '+07:00'")->execute();
        $query = new Query;
        $query->select('id, username, full_name, email, phone, birth, sex, user_type, image')
            ->from(Employee::tableName())
            ->where('MONTH(FROM_UNIXTIME(birth)) = MONTH(NOW()) AND status='.Employee::STATUS_ACTIVE . ' AND username != "admin" ORDER BY DAY(FROM_UNIXTIME(birth)) ASC');
        $employees = $query->all();
        if($employees) {
            $today = strtotime('today');
            $dayMonthToday = date('d-m',$today);
            $dayToday = date('d',$today);
            $yearToday = date('Y',$today);
            foreach($employees as $e) {
                if($e['birth'] && $e['birth'] != null && $e['birth'] != '' && $e['birth'] > 0) {
                    $dayMonthBirth = date('d-m', $e['birth']);
                    $dayBirth = date('d',$e['birth']);
                    $yearBirth = date('Y',$e['birth']);
                    $yearOld = $yearToday - $yearBirth;
                    $e['year_old'] = $yearOld;
                    $exArrayName = explode(" ", $e['full_name']);
                    $e['short_name'] = end($exArrayName);
                    $e['birth_day_format'] = date('d-m',$e['birth']);
                    if($e['image'] == null) {
                        $e['image'] = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNThmYmUzYWNjYyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1OGZiZTNhY2NjIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMy4xNzk2ODc1IiB5PSIzNi41NTYyNSI+NjR4NjQ8L3RleHQ+PC9nPjwvZz48L3N2Zz4=";
                    } else {
                        $e['image'] = "/" . $e['image'];
                    }
                    if($dayMonthBirth == $dayMonthToday) {
                        $out['inday'][] = $e;
                    } else {
                        if($dayBirth > $dayToday) {
                            $out['coming'][] = $e;
                        } else {
                            $out['last'][] = $e;
                        }
                    }
                }
            }
        }
        return $out;
    }
    
    public function actionAjaxCreate() {
        if(Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $employeeFullName = Yii::$app->request->post('full_name','');
            //$employeeFullName = preg_replace('/\W/i', '', $employeeFullName);
            if($employeeFullName != '') {
                $employee = new Employee();
                $employee->full_name = $employeeFullName;
                $employee->generateEmployee();
                if($employee->save(true,[
                    'full_name','username','email','phone','auth_key',
                    'password_hash','status','created_at','updated_at',
                    'bank_id','bank_account','user_type'])){
                    $return = $employee->toArray();
                    unset($return['auth_key']);
                    unset($return['password_hash']);
                    unset($return['status']);
                    unset($return['created_at']);
                    unset($return['updated_at']);
                    unset($return['bank_id']);
                    unset($return['bank_account']);
                    unset($return['user_type']);
                    return $return;
                } else {
                    return $employee->getErrors();
                }
            }
            throw new \yii\web\BadRequestHttpException('Yêu cầu nhập tên tác giả hợp lệ');
        }
    }
    

    public function actionCreateSuperAdmin() {
        Employee::createUserSuperAdmin();
    }
}