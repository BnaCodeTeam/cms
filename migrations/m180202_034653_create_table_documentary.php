<?php

use yii\db\Migration;

/**
 * Class m180202_034653_create_table_documentary
 */
class m180202_034653_create_table_documentary extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('documentary', [
            'id' => $this->primaryKey(),
            'name' => $this->text(),//Tên công văn
            'type' => $this->integer(),//Loại công văn (Đi or Đến)
            'send_number' => $this->string(),//Số hiệu đi
            'receive_number' => $this->string(),//Số hiệu đến
            'send_by_id' => $this->integer(),//Id đơn vị gửi
            'receive_by_id' => $this->integer(),//Id đơn vị nhận
            'send_by_name' => $this->string(),//Tên đơn vị gửi
            'receive_by_name' => $this->string(),//Tên đơn vị nhận
            'send_time' => $this->integer(),//Ngày gửi
            'receive_time' => $this->integer(),//Ngày nhận
            'sign' => $this->string(),//Ký nhận
            'note' => $this->text(),//Ghi chú
            'attachment' => $this->text(),//Lưu đường dẫn các file được attack
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->string(),
            'updated_by' => $this->string(),
            
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        return $this->dropTable('documentary');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180202_034653_create_table_documentary cannot be reverted.\n";

        return false;
    }
    */
}
