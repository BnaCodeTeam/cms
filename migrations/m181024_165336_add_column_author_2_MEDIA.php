<?php

use yii\db\Migration;

/**
 * Class m181024_165336_add_column_author_2_MEDIA
 */
class m181024_165336_add_column_author_2_MEDIA extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('media', 'author_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('media', 'author_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181024_165336_add_column_author_2_MEDIA cannot be reverted.\n";

        return false;
    }
    */
}
