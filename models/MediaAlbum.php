<?php

namespace app\models;

use Yii;
use \yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "media_album".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property string $created_by
 * @property string $updated_by
 * @property int $category_id
 * @property string $google_drive_id
 * @property string $google_drive_link
 * @property int $author_id
 */
class MediaAlbum extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 10;
    const STATUS_DELETE = 0;
    
    private $_mediaIds;
    public function setMediaIdString($mediaIds) {
        $this->_mediaIds = trim($mediaIds, ',');
    }
    public function getMediaIdString() {
        return $this->_mediaIds;
    }

    public function getMediaIdArray() {
        if($this->_mediaIds 
                && $this->_mediaIds != NULL 
                && $this->_mediaIds != '') {
            return array_map('intval', explode(',', $this->_mediaIds));
        }
        return [];
    }
    
    private $_employeeSearch;
    public function setEmployeeSearch($employeeSearch) {
        $this->_employeeSearch = $employeeSearch;
    }
    public function getEmployeeSearch() {
        return $this->_employeeSearch;
    }
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'media_album';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description', 'category_id'], 'required'],
            [['status', 'created_at', 'updated_at', 'category_id'], 'integer'],
            [['author_id'], 'integer', 'message' => '{attribute} không hợp lệ, hãy thêm mới tác giả này'],
            [['name', 'description', 'created_by', 'updated_by','google_drive_link','google_drive_id', 'mediaIdString'], 'string', 'max' => 255],
            ['mediaIdString', 'required', 'on' => 'create-ajax']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Tên',
            'description' => 'Mô tả',
            'status' => 'Trạng thái',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'category_id' => 'Chuyên mục',
            'google_drive_link' => 'Drive link',
            'google_drive_id' => 'Drive id',
            'author_id' => 'Tác giả',
            'mediaIdString' => 'Tải lên'
        ];
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className()
            ],
        ];
    }
    
    public function getCoverImage() {
        $mediaImage = Media::find()->where(['status' => Media::STATUS_ACTIVE, 'type' => Media::TYPE_PHOTO, 'album_id' => $this->id])->limit(3)->all();
        return $mediaImage;
    }
    
    public function getMedia() {
        return $this->hasMany(Media::className(), ['album_id' => 'id'])->where('media.status=:status',[':status' => Media::STATUS_ACTIVE])->orderBy(['id' => SORT_DESC]);
    }
    
    public function getMediaCategory() {
        return $this->hasOne(MediaCategory::className(), ['id' => 'category_id']);
    }
    
     public static function getListAlbum() {
        $models = MediaAlbum::find()->select(['id','name'])->where(['status' => MediaAlbum::STATUS_ACTIVE])->all();
        $return = [];
        foreach($models as $m) {
            $return[$m->id] = $m->name;
        }
        return $return;
    }
}
