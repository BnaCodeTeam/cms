<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DeclareAd */
/* @var $form yii\widgets\ActiveForm */

//use kartik\widgets\Select2;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\web\View;
use yii\jui\DatePicker;
?>

<?php 
$formatJs = <<< 'JS'
function IsNumeric(input){
    var RE = /^-{0,1}\d*\.{0,1}\d+$/;
    return (RE.test(input));
};
var formatEmployee = function(employee){
        if(employee.id != 'undefined' && IsNumeric(employee.id)) {
            var imx = 'uploads/images/users/no-user-image.png';
            if(employee.image != 'undefined' && employee.image != null) {
                imx = employee.image;
            }
            var ma = '<div class="row">' + 
                    '<div class="col-sm-8">' +
                        '<img data-holder-rendered="true" style="width: 64px; height: 64px; data-src="holder.js/64x64" src="/' + imx + '" class="img-rounded" />' + 
                        '<b style="margin-left:5px">' + employee.text + '</b>' + 
                    '</div>' + 
                '</div>';
            return '<div style="overflow:hidden;">' + ma + '</div>';
        }
    };
    
var formatSelectionEmployee = function(employee) {
    return employee.full_name || employee.text;
};

function setRemoveTag(eId) {
    $('#employee-tag-' + eId).remove();
    $('#declaread-ad_created_by').val("");
}

function setRemoveDeployTag(eId) {
    $('#employee-deploy-tag-' + eId).remove();
    $('#employee-deploy-id-' + eId).remove();
}

var eventSelectEmployee = function(item) {
    console.log(item);
    if(item.params.data.selected) {
        if(IsNumeric(item.params.data.id)) {
            $('#tags-name').html('<span onclick="setRemoveTag(' + item.params.data.id + ')" class="label label-info tag-name-label" style="margin-right:3px; cursor:pointer" data-id="' + item.params.data.id + '" id="employee-tag-' + item.params.data.id + '" >' + item.params.data.text + '</span>');
            $('#declaread-ad_created_by').val(item.params.data.id);
        }
    }
};

var eventSelectEmployeeDeploy = function(item) {
    if(item.params.data.selected) {
        if(IsNumeric(item.params.data.id)) {
            $('#tags-deploy').append('<span onclick="setRemoveDeployTag(' + item.params.data.id + ')" class="label label-info tag-name-label" style="margin-right:3px; cursor:pointer" data-id="' + item.params.data.id + '" id="employee-deploy-tag-' + item.params.data.id + '" >' + item.params.data.text + '</span>');
            $('#tags-deploy').append('<input type="hidden" value="' + item.params.data.id + '" id="employee-deploy-id-' + item.params.data.id + '" name="DeclareAd[EmployeeDeployString][]">');
            
            var numberTag = $('#tags-deploy .tag-name-label').length;
            var maxTagRow = 3;
            
            if(numberTag % maxTagRow == 0) {
                $('#tags-deploy').append("<br/>");
            }
        }
    }
};

JS;

$actionJs = 
"function changePaid() {
    $('#declaread-ad_pay_status').on('change', function() {
        if($(this).val() == ".app\models\DeclareAd::PAY_STATUS_WAIT.") {
            if($('#declaread-amount_paid').length == 0) {
                $('#amount-paid').append('<div class=\'form-group field-declaread-amount_paid\'><label class=\'control-label\' for=\'declaread-amount_paid\'>Số tiền đã thanh toán</label><input type=\'text\' id=\'declaread-amount_paid\' class=\'form-control\' name=\'DeclareAd[amount_paid]\'><div class=\'help-block\'></div></div>');
            }
        } else {
            if($('#declaread-amount_paid').length > 0) {
                $('.field-declaread-amount_paid').remove();
            }
        }
    });
}
changePaid();

function changePakageAd() {
    $('#declaread-ad_type').on('change', function() {
        if($(this).val() == ".app\models\DeclareAd::AD_TYPE_PACKAGE.") {
            if($('#ad-number-info').css('display') == 'none') {
                $('#ad-number-info').show();
            }
        } else {
            if($('#ad-number-info').css('display') !== 'none') {
                $('#ad-number-info').hide();
                $('.input-number').val(0);
            }
        }
    });
}
changePakageAd();
";
    

$this->registerJs($formatJs, View::POS_HEAD);
$this->registerJs($actionJs, View::POS_END);
$todayString = date('d-m-Y', time());
?>

<div class="declare-ad-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="table-responsive"> 
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <td colspan="1">
                        <?= $form->field($model, 'declare_ad_code')->textInput(['maxlength' => true]) ?>
                    </td>
                    <!--<td colspan="2">
                        <?php //echo $form->field($model, 'ad_partner')->textInput(['maxlength' => true]) ?>
                    </td>-->
                    <td colspan="2">
                        <?= $form->field($model, 'ad_partner_id')->dropDownList(\app\models\AdPartner::getAllAdPartner(), ['prompt'=>'---- Chọn đối tác ----']) ?>
                    </td>
                </tr>
                <tr>
                    <td id="ad-type-select">
                        <?= $form->field($model, 'ad_type')->dropDownList(app\models\DeclareAd::getAdTypeLabels(), ['prompt'=>'---- Chọn loại hình quảng cáo ----']) ?>
                        <div class="form-inline" id="ad-number-info" style="display: <?php echo ($model->ad_type == app\models\DeclareAd::AD_TYPE_PACKAGE)?'block':'none'?>">
                            <table class="table table-bordered table-striped"> 
                                <tbody> 
                                    <tr> 
                                        <th scope="row"> <code>Số clip</code> </th> 
                                        <td>
                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-danger btn-number btn-sm" data-type="minus" data-field="DeclareAd[number_clip]">
                                                      <span class="glyphicon glyphicon-minus"></span>
                                                    </button>
                                                </span>
                                                <?php if(isset($dataNumber)):?>
                                                <input type="text" name="DeclareAd[number_clip]" class="form-control input-number input-sm" placeholder="Số clip" min="0" max="100" 
                                                    <?php if(isset($dataNumber[app\models\DeclareAdPakageInfo::AD_TYPE_VIDEO])) echo 'value='.$dataNumber[app\models\DeclareAdPakageInfo::AD_TYPE_VIDEO];?> >
                                                <?php else:?>
                                                <input type="text" name="DeclareAd[number_clip]" class="form-control input-number input-sm" placeholder="Số clip" min="0" max="100">
                                                <?php endif;?>

                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-success btn-number btn-sm" data-type="plus" data-field="DeclareAd[number_clip]">
                                                        <span class="glyphicon glyphicon-plus"></span>
                                                    </button>
                                                </span>
                                            </div>
                                        </td> 
                                    </tr> 
                                    <tr> 
                                        <th scope="row"> <code>Số bài báo in</code> </th> 
                                        <td>
                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-danger btn-number btn-sm" data-type="minus" data-field="DeclareAd[number_article]">
                                                      <span class="glyphicon glyphicon-minus"></span>
                                                    </button>
                                                </span>
                                                <?php if(isset($dataNumber)):?>
                                                <input type="text" name="DeclareAd[number_article]" class="form-control input-number input-sm" placeholder="Bài báo in" min="0" max="100" 
                                                    <?php if(isset($dataNumber[app\models\DeclareAdPakageInfo::AD_TYPE_ARTICLE])) echo 'value='.$dataNumber[app\models\DeclareAdPakageInfo::AD_TYPE_ARTICLE];?> >
                                                <?php else:?>
                                                <input type="text" name="DeclareAd[number_article]" class="form-control input-number input-sm" placeholder="Bài báo in" min="0" max="100">
                                                <?php endif;?>
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-success btn-number btn-sm" data-type="plus" data-field="DeclareAd[number_article]">
                                                        <span class="glyphicon glyphicon-plus"></span>
                                                    </button>
                                                </span>
                                            </div>
                                        </td> 
                                    </tr> 
                                    <tr> 
                                        <th scope="row"> <code>Số banner</code> </th> 
                                        <td>
                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-danger btn-number btn-sm" data-type="minus" data-field="DeclareAd[number_banner]">
                                                      <span class="glyphicon glyphicon-minus"></span>
                                                    </button>
                                                </span>
                                                <?php if(isset($dataNumber)):?>
                                                <input type="text" name="DeclareAd[number_banner]" class="form-control input-number input-sm" placeholder="Số banner" min="0" max="100" 
                                                    <?php if(isset($dataNumber[app\models\DeclareAdPakageInfo::AD_TYPE_BANNER])) echo 'value='.$dataNumber[app\models\DeclareAdPakageInfo::AD_TYPE_BANNER];?> >
                                                <?php else:?>
                                                <input type="text" name="DeclareAd[number_banner]" class="form-control input-number input-sm" placeholder="Số banner" min="0" max="100">
                                                <?php endif;?>
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-success btn-number btn-sm" data-type="plus" data-field="DeclareAd[number_banner]">
                                                        <span class="glyphicon glyphicon-plus"></span>
                                                    </button>
                                                </span>
                                            </div>
                                        </td> 
                                    </tr> 
                                    <tr> 
                                        <th scope="row"> <code>Số bài điện tử</code> </th> 
                                        <td>
                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-danger btn-number btn-sm" data-type="minus" data-field="DeclareAd[number_e_article]">
                                                      <span class="glyphicon glyphicon-minus"></span>
                                                    </button>
                                                </span>
                                                <?php if(isset($dataNumber)):?>
                                                <input type="text" name="DeclareAd[number_e_article]" class="form-control input-number input-sm" placeholder="Bài điện tử" min="0" max="100" 
                                                    <?php if(isset($dataNumber[app\models\DeclareAdPakageInfo::AD_TYPE_E_ARTICLE])) echo 'value='.$dataNumber[app\models\DeclareAdPakageInfo::AD_TYPE_E_ARTICLE];?> >
                                                <?php else:?>
                                                <input type="text" name="DeclareAd[number_e_article]" class="form-control input-number input-sm" placeholder="Bài điện tử" min="0" max="100">
                                                <?php endif;?>
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-success btn-number btn-sm" data-type="plus" data-field="DeclareAd[number_e_article]">
                                                        <span class="glyphicon glyphicon-plus"></span>
                                                    </button>
                                                </span>
                                            </div>
                                        </td> 
                                    </tr> 
                                </tbody> 
                            </table>   
                        </div>
                    </td>
                    <td>
                        <?= $form->field($model,'AdFromTimeString')->widget(DatePicker::className(),['dateFormat' => 'dd-MM-yyyy','options' => ['class' => 'form-control'],'clientOptions' => ['defaultDate' => $todayString]]) ?>
                    </td>
                    <td>
                        <?= $form->field($model,'AdToTimeString')->widget(DatePicker::className(),['dateFormat' => 'dd-MM-yyyy','options' => ['class' => 'form-control'],'clientOptions' => ['defaultDate' => $todayString]]) ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= $form->field($model, 'ad_price')->textInput() ?>
                    </td>
                    <td>
                        <?php $url = Yii::$app->urlManager->createUrl(['employee/ajax-get-employee-list']);?>
                        <?php echo $form->field($model, 'EmployeeSearch')->widget(Select2::classname(), [
                            'initValueText' => '',
                            'options' => ['placeholder' => 'Tìm kiếm nhân viên...'],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 2,
                                'language' => [
                                    'errorLoading' => new JsExpression("function () { return 'Đang tìm dữ liệu...'; }"),
                                ],
                                'ajax' => [
                                    'url' => $url,
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                                    'delay' => 250,
                                ],
                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                'templateResult' => new JsExpression('formatEmployee'),
                                'templateSelection' => new JsExpression('formatSelectionEmployee'),
                                'tags' => true,
                            ],
                            'pluginEvents' => [
                                'select2:select' => new JsExpression('eventSelectEmployee'),
                            ]

                        ]);?>
                    </td>
                    <td>
                        <?= $form->field($model, 'ad_created_by')->hiddenInput()->label(false) ?>
                        <div class="form-group" id="tags-name">
                            <?php 
                            if($model->ad_created_by && $model->ad_created_by != null) {
                                echo '<span onclick="setRemoveTag('.$model->ad_created_by.')" '
                                        . 'class="label label-info tag-name-label" style="margin-right:3px; cursor:pointer" '
                                        . 'data-id="'.$model->ad_created_by.'" id="employee-tag-'.$model->ad_created_by.'" >'.$model->getEmployeeCreateInfo().'</span>';
                            }
                            ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <?php echo $form->field($model, 'EmployeeDeployString')->widget(Select2::classname(), [
                            'initValueText' => '',
                            'options' => ['placeholder' => 'Tìm kiếm nhân viên...'],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 2,
                                'language' => [
                                    'errorLoading' => new JsExpression("function () { return 'Đang tìm dữ liệu...'; }"),
                                ],
                                'ajax' => [
                                    'url' => $url,
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                                    'delay' => 250,
                                ],
                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                'templateResult' => new JsExpression('formatEmployee'),
                                'templateSelection' => new JsExpression('formatSelectionEmployee'),
                                'tags' => true,
                            ],
                            'pluginEvents' => [
                                'select2:select' => new JsExpression('eventSelectEmployeeDeploy'),
                            ]

                        ]);?>
                    </td>
                    <td>
                        <div class="form-group" id="tags-deploy">
                            <?php
                            if($model->deploy_employee && $model->deploy_employee != null) {
                                $deployment = $model->getEmployeeDeployInfo(true);
                                if(is_array($deployment) && !empty($deployment)) {
                                    $br = 1;
                                    foreach($deployment as $i => $de) {
                                        echo '<span onclick="setRemoveDeployTag('.$i.')" class="label label-info tag-name-label" style="margin-right:3px; cursor:pointer" data-id="' .$i. '" id="employee-deploy-tag-' .$i. '" >' .$de. '</span>';
                                        echo '<input type="hidden" value="' .$i. '" id="employee-deploy-id-' .$i. '" name="DeclareAd[EmployeeDeployString][]">';
                                        if($br % 3 == 0) {
                                            echo "<br/>";
                                        }
                                        $br ++;
                                    }
                                }
                            }
                            ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <?= $form->field($model, 'ad_note')->textarea(['rows' => 6]) ?>
                    </td>
                </tr>
                
                <tr>
                    <td colspan="3">
                        <b>THÔNG TIN THANH TOÁN</b>
                    </td>
                </tr>
                <tr>
                    <td id="amount-paid">
                        <?= $form->field($model, 'ad_pay_status')->dropDownList(app\models\DeclareAd::getPaidStatusLabels(), ['prompt' => '---- Trạng thái thanh toán ----']) ?>
                        <?php 
                        if($model->ad_pay_status == app\models\DeclareAd::PAY_STATUS_WAIT) {
                            echo $form->field($model, 'amount_paid')->textInput();
                        }
                        ?>
                    </td>
                    <td colspan="2">
                        <?= $form->field($model, 'ad_pay_note')->textarea(['rows' => 6]) ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                        <div class="form-group">
                            <?= $form->field($model, 'templateFile')->fileInput() ?>
                        </div>
                    </td>
                    <td colspan="2">
                        <?php echo $model->declare_template?'<a type="button" class="btn btn-primary" href="/'.$model->declare_template.'"><span class="glyphicon glyphicon-cloud-download" aria-hidden="true"></span> Tải hợp đồng</a>':'<label class="label label-default">Chưa có file hợp đồng</label>' ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo $form->field($model, 'is_order_paper')->checkbox()?>
                    </td>
                    <td>
                        <?php echo $form->field($model, 'order_paper_number')->textInput()?>
                    </td>
                    <td>
                        <?php echo $form->field($model, 'order_paper_price')->textInput()?>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? 'Xong' : 'Sửa', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $this->registerJsFile('/js/declare-ad.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>