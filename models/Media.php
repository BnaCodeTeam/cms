<?php

namespace app\models;

use Yii;
use yii\helpers\FileHelper;
use \yii\behaviors\TimestampBehavior;
use yii\imagine\Image;
/**
 * This is the model class for table "media".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $url
 * @property int $type
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property string $created_by
 * @property string $updated_by
 * @property string $album_id
 * @property int $category_id
 * @property string $google_drive_id
 * @property string $google_drive_link
 * @property int $author_id
 */
class Media extends \yii\db\ActiveRecord
{
    
    public $mediaFile;
    /**
     * @var UploadedFile[]
     */
    public $multiMediaFiles;
    
    const TYPE_VIDEO = 1;
    const TYPE_PHOTO = 2;
    
    const STATUS_ACTIVE = 10;
    const STATUS_DELETE = 0;


    const MEDIA_DIR = 'uploads/media/';
    
    private $_employeeSearch;
    public function setEmployeeSearch($employeeSearch) {
        $this->_employeeSearch = $employeeSearch;
    }
    public function getEmployeeSearch() {
        return $this->_employeeSearch;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'media';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url'], 'string'],
            [['url', 'title', 'description'], 'required'],
            [['category_id'], 'required', 'except' => 'create-by-ajax'],
            
            
            [['type', 'status', 'created_at', 'updated_at', 'album_id', 'category_id'], 'integer'],
            [['author_id'], 'integer', 'message' => '{attribute} không hợp lệ, hãy thêm mới tác giả này'],
            [['title', 'description', 'created_by', 'updated_by','google_drive_link','google_drive_id'], 'string', 'max' => 255],
            [['mediaFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, mp4', 'maxSize' => 1024 * 1024 * 30],
            [['multiMediaFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, mp4', 'maxFiles' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Mô tả',
            'url' => 'Url',
            'type' => 'Type',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'mediaFile' => 'Tải lên',
            'multiMediaFiles' => 'Tải lên', 
            'album_id' => 'Album Id',
            'category_id' => 'Chuyên mục',
            'google_drive_link' => 'Drive link',
            'google_drive_id' => 'Drive id',
            'author_id' => 'Tác giả'
        ];
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className()
            ],
        ];
    }
    
    public function upload() {
        if ($this->validate('mediaFile')) {
            $dir = self::MEDIA_DIR.Yii::$app->user->identity->id . '/';
            $type = 'images';
            $this->type = Media::TYPE_PHOTO;
            if($this->mediaFile->extension == 'mp4') {
                $type = 'videos';
                $this->type = Media::TYPE_VIDEO;
            }
            
            $dir = $dir.$type;
            if(!file_exists($dir)) {
                FileHelper::createDirectory($dir);
            }
            
            $path = $dir.'/'.time() . '_' . Yii::$app->security->generateRandomString(5) . '_'. md5($this->mediaFile->baseName) . '.' . $this->mediaFile->extension;
            $this->mediaFile->saveAs($path);
            $this->url = '/'.$path;
            $this->mediaFile = null;
            if($this->type == Media::TYPE_PHOTO) $this->createImageThumb();
            return true;
        } else {
            return false;
        }
    }
    
    public function multiUpload() {
        if ($this->validate('multiMediaFiles')) { 
            $data = [];
            foreach ($this->multiMediaFiles as $file) {
                $dir = self::MEDIA_DIR.Yii::$app->user->identity->id . '/';
                $type = 'images';
                if($file->extension == 'mp4') {
                    $type = 'videos';
                }
                $dir = $dir.$type;
                if(!file_exists($dir)) {
                    FileHelper::createDirectory($dir);
                }

                $path = $dir.'/'.time() . '_' . Yii::$app->security->generateRandomString(5) . '_'. md5($file->baseName) . '.' . $file->extension;
                $file->saveAs($path);
                $data[] = ['url' => '/'.$path, 'type' => $type, 'base_name' => $file->baseName];
                if($type == 'images') $this->createImageThumb('/'.$path);
            }
            return $data;
        } else {
            return false;
        }
    }
    
    public function createImageThumb($source = false) {
        if($source == false) {
            $source = $this->url;
        }
        $thumbName = $this->getThumbName($source);
        $coverThumbName = $this->getThumbName($source, '/cover_');
        
        $origin = Yii::getAlias('@webroot'.$source);
        $thumb = Yii::getAlias('@webroot'.$thumbName);
        $cover = Yii::getAlias('@webroot'.$coverThumbName);
        
        if(file_exists($origin)) {
            Image::thumbnail($origin, 300, 200)
                ->save($thumb, ['quality' => 70]);
            
            Image::thumbnail($origin, 171, 180)
                ->save($cover, ['quality' => 70]);
            return true;
        }
        return false;
    }
    
    public function getThumbName($source = false, $prefix='/thumb_') {
        if($source == false) {
            $source = $this->url;
        }
        $baseName = basename($source);
        $dirName = dirname($source);
        $thumbName = $prefix . $baseName;
        return $dirName.$thumbName;
    }
    
    public function getMediaAlbum() {
        return $this->hasOne(MediaAlbum::className(), ['id' => 'album_id']);
    }
    
    public function getMediaCategory() {
        return $this->hasOne(MediaCategory::className(), ['id' => 'category_id']);
    }
}
