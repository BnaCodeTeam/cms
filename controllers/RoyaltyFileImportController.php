<?php

namespace app\controllers;

use Yii;
use app\models\RoyaltyFileImport;
use app\models\RoyaltyFileImportSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use \app\models\Royalty;
use \moonland\phpexcel\Excel;
use app\components\AdminController;

/**
 * RoyaltyFileImportController implements the CRUD actions for RoyaltyFileImport model.
 */
class RoyaltyFileImportController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RoyaltyFileImport models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RoyaltyFileImportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RoyaltyFileImport model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RoyaltyFileImport model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RoyaltyFileImport();
     
        if ($model->load(Yii::$app->request->post())) {
            $model->created_at = time();
            $model->updated_at = time();
            $model->created_by = Yii::$app->user->identity->username;
            $model->templateFile = UploadedFile::getInstance($model, 'templateFile');
            if($model->templateFile) {
                $model->upload();
            }
            if($model->save()) {
                if($model->file_dir && $model->file_dir != '') {
                    if($this->importRoyaltyByFile('./'.$model->file_dir, $model->royalty_date)) {
                        return $this->redirect(['royalty/index','RoyaltySearch[royalty_month]' => $model->royalty_date]);
                    }
                }
            } else {
                print_r($model->getErrors());
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RoyaltyFileImport model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        return $this->redirect(['index']);
        
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RoyaltyFileImport model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RoyaltyFileImport model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RoyaltyFileImport the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RoyaltyFileImport::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function importRoyaltyByFile($fileDir, $royaltyMonth) {
        $data = Excel::import($fileDir, [
            'setFirstRecordAsKeys' => false, 
            'setIndexSheetByName' => true, 
            //'getOnlySheet' => 'Tuan 1',
        ]);
        
        if($data && is_array($data) && !empty($data)) {
            foreach ($data as $sheet => $shValues) {
                if($shValues && is_array($shValues)) {
                    foreach ($shValues as $row) {
                        if($row && is_array($row)) {
                            if(isset($row['A']) && intval($row['A']) > 0) {
                                $royaltyModel = new Royalty();
                                
                                $royaltyModel->full_name = trim((isset($row['B'])?$row['B'].' ':'').(isset($row['C'])?$row['C']:''));
                                $royaltyModel->address = trim(isset($row['D'])?$row['D']:'');
                                $royaltyModel->article_count = intval(isset($row['E'])?$row['E']:0);
                                $royaltyModel->post_count = intval(isset($row['F'])?$row['F']:0);
                                $royaltyModel->photo_count = intval(isset($row['G'])?$row['G']:0);
                                $royaltyModel->rate = floatval(isset($row['H'])?$row['H']:0);
                                $royaltyModel->royalty_amount = doubleval(isset($row['I'])?str_replace(',','',$row['I']):0);
                                $royaltyModel->tax_amount = doubleval(isset($row['J'])?str_replace(',','',$row['J']):0);
                                $royaltyModel->royalty_total_amount = doubleval(isset($row['K'])?str_replace(',','',$row['K']):0);
                                $royaltyModel->paid_type = trim(isset($row['L'])?$row['L']:'');
                                $royaltyModel->email = trim(isset($row['M'])?$row['M']:'');
                                $royaltyModel->royalty_month = $royaltyMonth;
                                $royaltyModel->royalty_week = trim($sheet);
                                $royaltyModel->created_at = time();
                                $royaltyModel->created_by = Yii::$app->user->identity->username;
                                $royaltyModel->is_sended = 0;
                                
                                $royaltyModel->save();
                            }
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }
}
