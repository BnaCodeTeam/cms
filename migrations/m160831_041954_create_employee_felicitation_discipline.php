<?php

use yii\db\Migration;

class m160831_041954_create_employee_felicitation_discipline extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('employee_felicitation_discipline', [
            'id' => $this->primaryKey(),
            'employee_id' => $this->integer()->notNull(),
            'felicitation_discipline_id' => $this->integer()->notNull(),
            'felicitation_discipline_type' => $this->integer()->notNull(),
            'felicitation_discipline_number' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        return $this->dropTable('employee_felicitation_discipline');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
