<?php

use yii\db\Migration;

/**
 * Class m181024_174851_add_column_author_alias_2_EMPLOYEE
 */
class m181024_174851_add_column_author_alias_2_EMPLOYEE extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('employee', 'alias_author', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('employee', 'alias_author');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181024_174851_add_column_author_alias_2_EMPLOYEE cannot be reverted.\n";

        return false;
    }
    */
}
