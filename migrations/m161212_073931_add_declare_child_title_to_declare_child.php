<?php

use yii\db\Migration;

class m161212_073931_add_declare_child_title_to_declare_child extends Migration
{
    public function up()
    {
        $this->addColumn('declare_ad_child', 'title', $this->string());
    }

    public function down()
    {
        $this->dropColumn('declare_ad_child', 'title');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
