<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "royalty_file_import".
 *
 * @property integer $id
 * @property string $file_dir
 * @property string $royalty_date
 * @property string $created_by
 * @property string $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 */
class RoyaltyFileImport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'royalty_file_import';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'royalty_date'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['file_dir', 'royalty_date', 'created_by', 'updated_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file_dir' => 'Đường dẫn file',
            'royalty_date' => 'Kỳ nhuận bút',
            'created_by' => 'Tạo bởi',
            'updated_by' => 'Cập nhật bởi',
            'created_at' => 'Tạo lúc',
            'updated_at' => 'Cập nhật lúc',
            'templateFile' => 'File nhuận bút'
        ];
    }
    
    /**
    * @var UploadedFile
    */
    public $templateFile;
    
    public function upload()
    {
        if ($this->validate()) {
            $path = 'uploads/attack_file/royalty/' . md5(Yii::$app->user->identity->username.'_'.time().'_'.$this->templateFile->baseName).'.'. $this->templateFile->extension;
            $this->templateFile->saveAs($path, false);
            $this->file_dir = $path;
            return true;
        } else {
            return false;
        }
    }
}
