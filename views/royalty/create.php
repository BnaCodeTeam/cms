<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Royalty */

$this->title = 'Create Royalty';
$this->params['breadcrumbs'][] = ['label' => 'Royalties', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="royalty-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
