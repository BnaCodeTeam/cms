<?php

namespace app\controllers;

use Yii;
use app\models\Media;
use app\models\MediaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\models\MediaAlbum;
use app\models\MediaAlbumSearch;
use app\components\AdminController;


ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3000); 
/**
 * MediaController implements the CRUD actions for Media model.
 */
class MediaController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Media models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MediaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    

    /**
     * Displays a single Media model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Media model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Media();
        if ($model->load(Yii::$app->request->post())) {
            if($model->author_id == NULL)$model->author_id = Yii::$app->user->identity->id;
            $model->mediaFile = UploadedFile::getInstance($model, 'mediaFile');
            if ($model->upload()) {
                $model->created_by = Yii::$app->user->identity->username;
                $model->updated_by = Yii::$app->user->identity->username;
                if($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    public function actionMultiCreate() {
        $model = new Media();

        if (Yii::$app->request->isPost) {
            $model->multiMediaFiles = UploadedFile::getInstances($model, 'multiMediaFiles');
            $uploadData = $model->multiUpload();
            if ($uploadData && is_array($uploadData) && !empty($uploadData)) {
                foreach($uploadData as $data) {
                    if(isset($data['url']) && $data['url'] != '') {
                        $mediaModel = new Media();
                        $mediaModel->load(Yii::$app->request->post());
                        if($mediaModel->author_id == NULL)$mediaModel->author_id = Yii::$app->user->identity->id;
                        $mediaModel->url = $data['url'];
                        $mediaModel->created_by = Yii::$app->user->identity->username;
                        $mediaModel->updated_by = Yii::$app->user->identity->username;
                        
                        if(isset($data['type'])) {
                            $type = Media::TYPE_VIDEO;
                            if($data['type'] == 'images') {
                                $type = Media::TYPE_PHOTO;
                            }
                            $mediaModel->type = $type;
                        }
                        $mediaModel->save();
                    }
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'multiFile' => true,
        ]);
    }

    /**
     * Updates an existing Media model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if(Yii::$app->user->identity->username != 'admin' 
            && $model->created_by != Yii::$app->user->identity->username) {
            throw new \yii\web\ForbiddenHttpException('Bạn không có quyền sửa Media này'); 
        }

        if ($model->load(Yii::$app->request->post())) {
            if($model->author_id == NULL)$model->author_id = Yii::$app->user->identity->id;
            $model->mediaFile = UploadedFile::getInstance($model, 'mediaFile');
            if($model->mediaFile) {
                $model->upload();
            }
            $model->updated_by = Yii::$app->user->identity->username;
            if($model->save()) {
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Media model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if(Yii::$app->user->identity->username != 'admin' 
            && $model->created_by != Yii::$app->user->identity->username) {
            throw new \yii\web\ForbiddenHttpException('Bạn không có quyền xóa Media này'); 
        }
        $model->status = Media::STATUS_DELETE;
        $model->save();

        return $this->redirect(['index']);
    }
    
    public function actionCreateAlbum() {
        $mediaModel = new Media();
        $albumModel = new MediaAlbum();
        
        if (Yii::$app->request->isPost) {
            $mediaModel->multiMediaFiles = UploadedFile::getInstances($mediaModel, 'multiMediaFiles');
            $uploadData = $mediaModel->multiUpload();
            if ($uploadData && is_array($uploadData) && !empty($uploadData)) {
                $albumModel->load(Yii::$app->request->post());
                if($albumModel->author_id == NULL) $albumModel->author_id = Yii::$app->user->identity->id;
                $albumModel->created_by = Yii::$app->user->identity->username;
                $albumModel->updated_by = Yii::$app->user->identity->username;
                if($albumModel->save()) {
                    foreach($uploadData as $data) {
                        if(isset($data['url']) && $data['url'] != '') {
                            $media = new Media();
                            $media->url = $data['url'];
                            $media->created_by = Yii::$app->user->identity->username;
                            $media->updated_by = Yii::$app->user->identity->username;
                            $media->album_id = $albumModel->id;
                            $media->title = $data['base_name'];
                            $media->description = $albumModel->description;
                            $media->category_id = $albumModel->category_id;
                            $media->author_id = $albumModel->author_id;
                            if(isset($data['type'])) {
                                $type = Media::TYPE_VIDEO;
                                if($data['type'] == 'images') {
                                    $type = Media::TYPE_PHOTO;
                                }
                                $media->type = $type;
                            }
                            $media->save();
                        }
                    }
                    return $this->redirect(['index']);
                }
            }
        }
        
        return $this->render('create-album', [
            'media' => $mediaModel,
            'album' => $albumModel,
        ]);
    }
    
    public function actionAddToAlbum() {
        
    }

    public function actionAlbum() {
        $searchModel = new MediaAlbumSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('album', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionAjaxCreateAlbumUpload() {
        $model = new Media();
        $model->multiMediaFiles = UploadedFile::getInstances($model, 'multiMediaFiles');
        $uploadData = $model->multiUpload();
        
        $return = [];
        if ($uploadData && is_array($uploadData) && !empty($uploadData)) {  
            foreach($uploadData as $data) {
                if(isset($data['url']) && $data['url'] != '') {
                    $media = new Media();
                    $media->scenario = 'create-by-ajax';
                    $media->url = $data['url'];
                    $media->created_by = Yii::$app->user->identity->username;
                    $media->updated_by = Yii::$app->user->identity->username;
                    $media->title = $data['base_name'];
                    $media->description = $data['base_name'];
                    if(isset($data['type'])) {
                        $type = Media::TYPE_VIDEO;
                        if($data['type'] == 'images') {
                            $type = Media::TYPE_PHOTO;
                        }
                        $media->type = $type;
                    }
                    if($media->save()){
                        $return[] = $media->id;
                    }
                }
            }         
        }
        return json_encode($return);
    }


    public function actionCreateAlbumAjax() {
        $mediaModel = new Media();
        $albumModel = new MediaAlbum();
        $albumModel->scenario = 'create-ajax';
        if (Yii::$app->request->isPost) {
            
            $albumModel->load(Yii::$app->request->post());
            if($albumModel->author_id == NULL) $albumModel->author_id = Yii::$app->user->identity->id;
            $albumModel->created_by = Yii::$app->user->identity->username;
            $albumModel->updated_by = Yii::$app->user->identity->username;
            if($albumModel->save()) {
                Media::updateAll(['album_id' => $albumModel->id, 
                                    'description' => $albumModel->description, 
                                    'category_id' => $albumModel->category_id, 
                                    'author_id' => $albumModel->author_id],
                                ['id' => $albumModel->mediaIdArray]);
                
                return $this->redirect(['album']);
                
            }
        }
        
        return $this->render('create-album-ajax', [
            'media' => $mediaModel,
            'album' => $albumModel,
        ]);
    }
    
    public function actionAjaxDelete() {
        $id = Yii::$app->request->post('id', '');
        $id = intval($id);
        if($id > 0) {
            $model = $this->findModel($id);
            if(Yii::$app->user->identity->username != 'admin' 
                && $model->created_by != Yii::$app->user->identity->username) {
                throw new \yii\web\ForbiddenHttpException('Bạn không có quyền xóa Media này'); 
            }
            $model->status = Media::STATUS_DELETE;
            $model->save(true,['status']);
            return json_encode(['id' => $model->id]);
        }
        throw new \yii\web\BadRequestHttpException('Id không hợp lệ');
    }
    
    public function actionEditor($id) {
        $id = Yii::$app->request->get('id', '');
        $image = '';
        $maxW = 900;
        $w = 900;
        $h = 1500;
        if($id != '') {
            $id = intval($id);
            if($id > 0) {
                $model = $this->findModel($id);
                if($model->type == Media::TYPE_PHOTO) {
                    $image = $model->url;
                    if($model->google_drive_id != NULL) {
                        $image = '/media/view-image?id='.$model->google_drive_id;
                    }
                    if($image && $image != NULL && $image != '') {
                        list($width, $height) = getimagesize(Yii::$app->params['domain']['base_url'].$image);
                        
                        if($width <= $maxW) {
                            $w = $width;
                            $h = $height + 350;
                        } else {
                            $w = $maxW;
                            $h = $height * ($w/$width) + 350;
                        }
                    }
                }
            }
        }
        
        return $this->render('editor',['image' => $image, 'w' => $w, 'h' => $h, 'name' => $model->title]);
    }
    
    public function actionViewImage($id) {
        header("Content-type: image/jpg");
        $command = Yii::$app->params['domain']['view_image_command']. ' ' . $id;
        passthru($command);
    }
    

    /**
     * Finds the Media model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Media the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Media::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Media này không tồn tại');
    }
    
    
}
