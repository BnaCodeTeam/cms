<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;
use yii\web\JsExpression;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MediaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Media';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .row .summary {
        margin-left: 15px;
    }
    .row .pagination {
        margin-left: 15px;
    }
    .row .empty {
        margin-left: 15px;
    }
</style>
<div class="media-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p><?php echo $this->render('_search', ['model' => $searchModel]); ?></p>

    <p>
        <?= Html::a('Tạo Media', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Albums', ['album'], ['class' => 'btn btn-primary']) ?>
    </p>
    <div class="row">
        <?php 
        echo ListView::widget([
        'dataProvider' => $dataProvider,
            'itemView' => '_img',
        ]);
        ?>
    </div>
   
</div>
<?php $this->registerJsFile('/js/media-item.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
