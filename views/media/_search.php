<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MediaSearch */
/* @var $form yii\widgets\ActiveForm */
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\web\View;
?>

<?php 
$formatJs = <<< 'JS'
function IsNumeric(input){
    var RE = /^-{0,1}\d*\.{0,1}\d+$/;
    return (RE.test(input));
};
var formatEmployee = function(employee){
        if(employee.id != 'undefined' && IsNumeric(employee.id)) {
            var imx = 'uploads/images/users/no-user-image.png';
            if(employee.image != 'undefined' && employee.image != null) {
                imx = employee.image;
            }
            var ma = '<div class="row">' + 
                    '<div class="col-md-12">' +
                        '<img data-holder-rendered="true" style="width: 64px; height: 64px; data-src="holder.js/64x64" src="/' + imx + '" class="img-rounded" />' + 
                        '<b style="margin-left:5px">' + employee.text + '</b>' + 
                    '</div>' + 
                '</div>';
            return '<div style="overflow:hidden;">' + ma + '</div>';
        }
    };
    
var formatSelectionEmployee = function(employee) {
    return employee.full_name || employee.text;
};


var eventSelectEmployee = function(item) {
    if(item.params.data.selected) {
        $('#mediasearch-author_id').val(item.params.data.id);
    }
};
JS;

$this->registerJs($formatJs, View::POS_HEAD);?>

<div class="media-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => ['class' => 'form-inline'],
    ]); ?>

    <?php //echo $form->field($model, 'id') ?>

    <?= $form->field($model, 'title')->textInput(['placeholder' => 'Nhập tiêu đề để tìm kiếm'])->label(false) ?>

    <?= $form->field($model, 'description')->textInput(['placeholder' => 'Nhập mô tả để tìm kiếm'])->label(false) ?>
    
    <?= $form->field($model, 'category_id')->dropDownList([''=>'Tất cả chuyên mục'] + \app\models\MediaCategory::getListCategory())->label(false) ?>
    
    <?= $form->field($model, 'author_id')->hiddenInput()->label(false); ?>
    
    <?php $url = Yii::$app->urlManager->createUrl(['employee/ajax-get-employee-list']);?>
    <?php echo $form->field($model, 'EmployeeSearch')->widget(Select2::classname(), [
        'initValueText' => '',
        'options' => ['placeholder' => 'Tìm kiếm theo tác giả...'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 2,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Đang tìm dữ liệu...'; }"),
            ],
            'ajax' => [
                'url' => $url,
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                'delay' => 250,
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('formatEmployee'),
            'templateSelection' => new JsExpression('formatSelectionEmployee'),
            'tags' => true,
            'width' => '300px',
        ],
        'pluginEvents' => [
            'select2:select' => new JsExpression('eventSelectEmployee'),
        ]

    ])->label(false);?>

    <div class="form-group" style="margin-bottom: 10px;">
        <?= Html::submitButton('Tìm kiếm', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
