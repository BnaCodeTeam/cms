<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Documentary */

$this->title = 'Thêm công văn';
$this->params['breadcrumbs'][] = ['label' => 'Quản lý công văn', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="documentary-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
