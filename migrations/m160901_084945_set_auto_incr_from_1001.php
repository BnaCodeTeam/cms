<?php

use yii\db\Migration;

class m160901_084945_set_auto_incr_from_1001 extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE employee AUTO_INCREMENT = 1001;");
    }

    public function down()
    {
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
