<?php

use yii\db\Migration;

class m161208_085741_add_updated_by_to_declare_ad extends Migration
{
    public function up()
    {
        $this->addColumn('declare_ad', 'updated_by', $this->string());
    }

    public function down()
    {
        $this->dropColumn('declare_ad', 'updated_by');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
