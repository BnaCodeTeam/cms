<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use Google_Client;
use Google_Service_Drive;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";
    }
    
    public function actionList() {
        $client = $this->getClient();
        $service = new Google_Service_Drive($client);
        
        $optParams = array(
          //'pageSize' => 10,
          'fields' => 'nextPageToken, files(id, name, webContentLink, webViewLink)'
        );
        $results = $service->files->listFiles($optParams);

        if(count($results->getFiles()) == 0) {
            print "No files found.\n";
        } else {
            echo count($results->getFiles());
            echo "\n \n";
            print "Files:\n";
            foreach($results->getFiles() as $file) {
                printf("%s (%s) (%s)\n", $file->getName(), $file->getId(), $file->getWebViewLink());
            }
        }
    }
    
    protected function getClient() {
        $client = new Google_Client();
        $client->setApplicationName('Google Drive API PHP');
        $client->setScopes(Google_Service_Drive::DRIVE_METADATA_READONLY);
        $client->addScope(Google_Service_Drive::DRIVE_FILE);
        $client->setAuthConfig(\Yii::$app->params['credentials_dir']);
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');

        $tokenPath = \Yii::$app->params['token_dir'];
        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $client->setAccessToken($accessToken);
        }

        // If there is no previous token or it's expired.
        if ($client->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                // Request authorization from the user.
                $authUrl = $client->createAuthUrl();
                printf("Open the following link in your browser:\n%s\n", $authUrl);
                print 'Enter verification code: ';
                $authCode = trim(fgets(STDIN));

                // Exchange authorization code for an access token.
                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                $client->setAccessToken($accessToken);

                // Check to see if there was an error.
                if (array_key_exists('error', $accessToken)) {
                    throw new Exception(join(', ', $accessToken));
                }
            }
            // Save the token to a file.
            if (!file_exists(dirname($tokenPath))) {
                mkdir(dirname($tokenPath), 0700, true);
            }
            file_put_contents($tokenPath, json_encode($client->getAccessToken()));
        }
        return $client;
    }
}
