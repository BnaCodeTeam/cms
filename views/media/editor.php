
<style>
    @import url(http://fonts.googleapis.com/css?family=Noto+Sans);
</style>
<?php 
use yii\web\View;
use yii\helpers\Html;

$this->registerCssFile("/css/tui-color-picker.css");
$this->registerCssFile("/js/tui.image-editor/dist/tui-image-editor.css");

$this->registerJsFile('/js/fabric.js', ['position' => View::POS_END]); 
$this->registerJsFile('/js/tui-code-snippet.min.js', ['position' => View::POS_END]); 
$this->registerJsFile('/js/FileSaver.min.js', ['position' => View::POS_END]);
$this->registerJsFile('/js/tui-color-picker.js', ['position' => View::POS_END]);
$this->registerJsFile('/js/tui.image-editor/dist/tui-image-editor.js', ['position' => View::POS_END]); 
$this->registerJsFile('/js/tui.image-editor/examples/js/theme/white-theme.js', ['position' => View::POS_END]); 
$this->registerJsFile('/js/tui.image-editor/examples/js/theme/black-theme.js', ['position' => View::POS_END]);

$this->title = 'Trình chỉnh sửa';
$this->params['breadcrumbs'][] = ['label' => 'Media', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row" style="height: <?=$h?>px; margin-left: 0px; margin-right: 0px;">
    <div id="tui-image-editor-container"></div>
</div>

<?php 
$js = "var imageEditor = new tui.ImageEditor('#tui-image-editor-container', {
    includeUI: {
        loadImage: {
            path: '{$image}',
            name: '{$name}-{$w}x{$h}'
        },
        theme: blackTheme, // or whiteTheme
        initMenu: 'filter',
        menuBarPosition: 'bottom'
    },
    cssMaxWidth: {$w},
    cssMaxHeight: {$h}
});

window.onresize = function() {
    imageEditor.ui.resizeEditor();
}";
$this->registerJs($js, View::POS_END);

