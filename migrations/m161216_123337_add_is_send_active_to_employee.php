<?php

use yii\db\Migration;

class m161216_123337_add_is_send_active_to_employee extends Migration
{
    public function up()
    {
        $this->addColumn('employee', 'is_send_active', \yii\db\Schema::TYPE_INTEGER.'(5) DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('employee', 'is_send_active');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
