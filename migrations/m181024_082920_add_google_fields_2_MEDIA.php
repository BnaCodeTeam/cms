<?php

use yii\db\Migration;

/**
 * Class m181024_082920_add_google_fields_2_MEDIA
 */
class m181024_082920_add_google_fields_2_MEDIA extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('media', 'google_drive_id', $this->string());
        $this->addColumn('media', 'google_drive_link', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('media', 'google_drive_id');
        $this->dropColumn('media', 'google_drive_link');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181024_082920_add_google_fields_2_MEDIA cannot be reverted.\n";

        return false;
    }
    */
}
