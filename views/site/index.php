<?php

/* @var $this yii\web\View */

$this->title = 'Phần mềm quản lý tổng thể Báo Nghệ An';
use yii\web\View;
?>
<div class="site-index">
   
    <div class="row">
        <div class="col-md-10">
            <div class="carousel slide" id="carousel-example-generic" data-ride="carousel"> 
                <ol class="carousel-indicators"> 
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li> 
                    <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li> 
                    <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                    <li data-target="#carousel-example-generic" data-slide-to="3" class=""></li> 
                    <li data-target="#carousel-example-generic" data-slide-to="4" class=""></li>
                    <li data-target="#carousel-example-generic" data-slide-to="5" class=""></li> 
                    <li data-target="#carousel-example-generic" data-slide-to="6" class=""></li>
                </ol> 
                <div class="carousel-inner" role="listbox"> 
                    <div class="item active"> 
                        <img alt="" src="/uploads/images/slide/slide1.jpg" data-holder-rendered="true"> 
                    </div> 
                    <div class="item"> 
                        <img alt="" src="/uploads/images/slide/slide2.jpg" data-holder-rendered="true"> 
                    </div> 
                    <div class="item"> 
                        <img alt="" src="/uploads/images/slide/slide3.jpg" data-holder-rendered="true"> 
                    </div> 
                    <div class="item"> 
                        <img alt="" src="/uploads/images/slide/slide4.jpg" data-holder-rendered="true"> 
                    </div> 
                    <div class="item"> 
                        <img alt="" src="/uploads/images/slide/slide5.jpg" data-holder-rendered="true"> 
                    </div> 
                    <div class="item"> 
                        <img alt="" src="/uploads/images/slide/slide6.jpg" data-holder-rendered="true"> 
                    </div> 
                    <div class="item"> 
                        <img alt="" src="/uploads/images/slide/slide7.jpg" data-holder-rendered="true"> 
                    </div> 
                    <div class="item"> 
                        <img alt="" src="/uploads/images/slide/slide8.jpg" data-holder-rendered="true"> 
                    </div>
                </div> 
                <a href="#carousel-example-generic" class="left carousel-control" role="button" data-slide="prev"> 
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> 
                    <span class="sr-only">Previous</span> 
                </a> 
                <a href="#carousel-example-generic" class="right carousel-control" role="button" data-slide="next"> 
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> 
                    <span class="sr-only">Next</span> 
                </a> 
            </div> 
        </div>
    </div>
    
    <style>
        .list-group-item {
            background-color: rgba(60, 120, 120, 0.6);
            color: #fff;
            padding: 5px 10px;
        }
    </style>
    <div style="width: 315px; height: 610px; margin-top: 15px; right: 15px; position: absolute; top: 60px; background-color: rgba(60, 120, 120, 0.1); overflow: scroll;">
        <ul class="list-group" id="birth-today" style="display: none">
            <a href="#" class="list-group-item active">
                Sinh nhật vào hôm nay
            </a>
        </ul>
        <ul class="list-group" id="birth-last" style="display: none">
            <a href="#" class="list-group-item active">
                Sinh nhật gần đây
            </a>
        </ul>
        <ul class="list-group" id="birth-coming" style="display: none">
            <a href="#" class="list-group-item active">
                Sinh nhật sắp tới
            </a>
        </ul>
    </div>
</div>
<?php 
$urlGetBirth = Yii::$app->urlManager->createUrl(['/employee/ajax-get-employee-birthday']);
$js = 'function loadBirth() {
    $.ajax({url: "'.$urlGetBirth.'", success: function(result) {
        if($.isEmptyObject(result) == false) {
            if(result.hasOwnProperty("inday")) {
                if(result.inday.length > 0) {
                    var strInfo = "";
                    for(var ix in result.inday) {
                        var sex = "cô";
                        if(result.inday[ix]["sex"] == 1) {
                            sex = "anh"
                        }
                        strInfo = strInfo + 
                        "<li class=\"list-group-item\">"+
                            "<div class=\"media\" style=\"margin-top:10px;\">"+
                                "<div class=\"media-left\">"+
                                    "<a href=\"#\">"+
                                        "<img alt=\"64x64\" class=\"media-object img-thumbnail\" data-src=\"holder.js/80x80\" src=\""+result.inday[ix]["image"]+"\" data-holder-rendered=\"true\" style=\"width: 80px; height: 80px;\">"+
                                    "</a>"+
                                "</div>"+
                                "<div class=\"media-body\">"+
                                    "<h4 class=\"media-heading\">"+result.inday[ix]["full_name"]+"</h4>"+
                                    "<p>Hôm nay là sinh nhật "+result.inday[ix]["year_old"]+" tuổi của "+result.inday[ix]["short_name"]+"! Hãy gửi lời chúc mừng tới "+sex+" ấy</p>"+
                                "</div>"+
                            "</div>"+
                            "<div class=\"alert alert-danger\" role=\"alert\">"+
                                "<span class=\"glyphicon glyphicon-gift\" style=\"margin-right:5px;\" aria-hidden=\"true\"></span>"+
                                "<span class=\"glyphicon glyphicon-heart\" style=\"margin-right:5px;\" aria-hidden=\"true\"></span>"+
                                "<span class=\"glyphicon glyphicon-tree-conifer\" style=\"margin-right:5px;\" aria-hidden=\"true\"></span>"+
                                "<span class=\"glyphicon glyphicon-gift\" style=\"margin-right:5px;\" aria-hidden=\"true\"></span>"+
                                "<span class=\"glyphicon glyphicon-heart\" style=\"margin-right:5px;\" aria-hidden=\"true\"></span>"+
                                "<span class=\"glyphicon glyphicon-tree-conifer\" style=\"margin-right:5px;\" aria-hidden=\"true\"></span>"+
                                "<span class=\"glyphicon glyphicon-gift\" style=\"margin-right:5px;\" aria-hidden=\"true\"></span>"+
                                "<span class=\"glyphicon glyphicon-heart\" style=\"margin-right:5px;\" aria-hidden=\"true\"></span>"+
                                "<span class=\"glyphicon glyphicon-tree-conifer\" style=\"margin-right:5px;\" aria-hidden=\"true\"></span>"+
                                "<span class=\"glyphicon glyphicon-gift\" style=\"margin-right:5px;\" aria-hidden=\"true\"></span>"+
                                "<span class=\"glyphicon glyphicon-heart\" style=\"margin-right:5px;\" aria-hidden=\"true\"></span>"+
                                "<span class=\"glyphicon glyphicon-tree-conifer\" style=\"margin-right:5px;\" aria-hidden=\"true\"></span>"+
                            "</div>"+
                        "</li>";
                    }
                    $("#birth-today").append(strInfo);
                    $("#birth-today").show();
                }
            }
            if(result.hasOwnProperty("coming")) {
                if(result.coming.length > 0) {
                    var strInfo2 = "";
                    for(var iy in result.coming) {
                        strInfo2 = strInfo2 +
                        "<li class=\"list-group-item\">"+
                            "<div class=\"media\" style=\"margin-top:10px;\">"+
                                "<div class=\"media-left\">"+
                                    "<a href=\"#\">"+
                                        "<img alt=\"64x64\" class=\"media-object img-thumbnail\" data-src=\"holder.js/80x80\" src=\""+result.coming[iy]["image"]+"\" data-holder-rendered=\"true\" style=\"width: 80px; height: 80px;\">"+
                                    "</a>"+
                                "</div>"+
                                "<div class=\"media-body\">"+
                                    "<h4 class=\"media-heading\">"+result.coming[iy]["full_name"]+"</h4>"+
                                    "<p>Ngày sinh nhật: "+result.coming[iy]["birth_day_format"]+"</p>"+
                                    "<p>"+result.coming[iy]["email"]+"</p>"+
                                "</div>"+
                            "</div>"+
                        "</li>";
                    }
                    $("#birth-coming").append(strInfo2);
                    $("#birth-coming").show();
                }
            }
            if(result.hasOwnProperty("last")) {
                if(result.last.length > 0) {
                    var strInfo3 = "";
                    for(var iz in result.last) {
                        strInfo3 = strInfo3 +
                        "<li class=\"list-group-item\">"+
                            "<div class=\"media\" style=\"margin-top:10px;\">"+
                                "<div class=\"media-left\">"+
                                    "<a href=\"#\">"+
                                        "<img alt=\"64x64\" class=\"media-object img-thumbnail\" data-src=\"holder.js/80x80\" src=\""+result.last[iz]["image"]+"\" data-holder-rendered=\"true\" style=\"width: 80px; height: 80px;\">"+
                                    "</a>"+
                                "</div>"+
                                "<div class=\"media-body\">"+
                                    "<h4 class=\"media-heading\">"+result.last[iz]["full_name"]+"</h4>"+
                                    "<p>Ngày sinh nhật: "+result.last[iz]["birth_day_format"]+"</p>"+
                                    "<p>"+result.last[iz]["email"]+"</p>"+
                                "</div>"+
                            "</div>"+
                        "</li>";
                    }
                    $("#birth-last").append(strInfo3);
                    $("#birth-last").show();
                }
            }
        }
    }});
}
loadBirth();';

$this->registerJs($js, View::POS_END);
?>