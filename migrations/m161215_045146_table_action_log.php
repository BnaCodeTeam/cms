<?php

use yii\db\Migration;

class m161215_045146_table_action_log extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('action_log', [
            'id' => $this->primaryKey(),
            'action' => $this->string(),
            'object_data' => $this->string(),
            'result_data' => $this->integer(),
            'action_user' => $this->string(),
            'created_at' => $this->integer()
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('action_log');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
