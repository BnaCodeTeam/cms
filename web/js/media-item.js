var waitingDialog = waitingDialog || (function ($) {
    'use strict';
    // Creating modal dialog's DOM
    var $dialog = $(
            '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
            '<div class="modal-dialog modal-m">' +
            '<div class="modal-content">' +
                '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
                '<div class="modal-body">' +
                    '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
                '</div>' +
            '</div></div></div>');
    return {
        /**
         * Opens our dialog
         * @param message Custom message
         * @param options Custom options:
         * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
         * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
         */
        show: function (message, options) {
            // Assigning defaults
            if (typeof options === 'undefined') {
                options = {};
            }
            if (typeof message === 'undefined') {
                message = 'Loading';
            }
            var settings = $.extend({
                dialogSize: 'm',
                progressType: '',
                onHide: null // This callback runs after the dialog was hidden
            }, options);

            // Configuring dialog
            $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
            $dialog.find('.progress-bar').attr('class', 'progress-bar');
            if (settings.progressType) {
                $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
            }
            $dialog.find('h3').text(message);
            // Adding callbacks
            if (typeof settings.onHide === 'function') {
                $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                    settings.onHide.call($dialog);
                });
            }
            // Opening dialog
            $dialog.modal();
        },
        /**
         * Closes dialog
         */
        hide: function () {
            $dialog.modal('hide');
        }
    };
})(jQuery);   


$('.media-item-delete').click(function() {
    var confirmx = confirm('Bạn có chắc là sẽ xóa mục này không?');
    if(confirmx) {
        var mediaId = $(this).attr('data-media-id');
        $.ajax({
            url: '/media/ajax-delete',
            type: 'POST',
            dataType: 'json',
            data: {'id':mediaId},
            
            beforeSend: function() {
                waitingDialog.show('Đang xóa...', {dialogSize: 'sm', progressType: 'warning'});
            },
            success: function (data) {
                if(typeof data.id != 'undefined' && data.id > 0) {
                    $('#media-item-'+data.id).remove();
                }
                waitingDialog.hide();
            },
            error: function(e) {
                waitingDialog.hide();
                alert(e.responseText);
            },
        });
    }
});

$('#btn-add-author').click(function() {
    var authorName = $('#media-select-author-id').val();
    if(!IsNumeric(authorName)) {
        $.ajax({
            url: '/employee/ajax-create',
            type: 'POST',
            dataType: 'json',
            data: {'full_name':authorName},
            
            beforeSend: function() {
                waitingDialog.show('Đang thêm tác giả...', {dialogSize: 'sm', progressType: 'info'});
            },
            success: function (data) {
                var optString = '<option selected value="'+data.id+'">'+data.full_name+'</option>';
                $('#media-select-author-id').append(optString);
                waitingDialog.hide();
            },
            error: function(e) {
                waitingDialog.hide();
                alert("Đã xảy ra lỗi " + e.responseText);
            },
        });
    }
});