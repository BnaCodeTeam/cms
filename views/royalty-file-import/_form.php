<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RoyaltyFileImport */
/* @var $form yii\widgets\ActiveForm */
use yii\jui\DatePicker;
?>

<div class="royalty-file-import-form">
    
    <?php $form = ActiveForm::begin(); ?>
    
    <div class="table-responsive"> 
        <table class="table table-bordered table-striped">
            <tbody>
                <tr> 
                    <td style="width:300px">
                        <div class="form-group">
                            <?= $form->field($model,'royalty_date')->widget(DatePicker::className(),
                            ['dateFormat' => 'MM-yyyy','options' => ['class' => 'form-control'],
                            'clientOptions' => ['defaultDate' => '']]) ?>
                        </div>
                    </td>
                    
                    <td>
                        <div class="form-group">
                            <?= $form->field($model, 'templateFile')->fileInput() ?>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    
</div>
