<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AdPartner */

$this->title = 'Cập nhật đối tác: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Quản lý đối tác', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Cập nhật';
?>
<div class="ad-partner-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
