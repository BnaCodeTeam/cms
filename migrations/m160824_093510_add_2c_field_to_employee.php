<?php

use yii\db\Migration;
use \yii\db\Schema;

class m160824_093510_add_2c_field_to_employee extends Migration
{
    public function up()
    {
        $this->addColumn("employee", "native_land", Schema::TYPE_STRING);
        $this->addColumn("employee", "ethnic_group", Schema::TYPE_STRING);
        $this->addColumn("employee", "religion", Schema::TYPE_STRING);
        $this->addColumn("employee", "employee_coefficient", Schema::TYPE_STRING);//ngach cong chuc
        $this->addColumn("employee", "height", Schema::TYPE_STRING);
        $this->addColumn("employee", "weight", Schema::TYPE_STRING);
        $this->addColumn("employee", "blood_group", Schema::TYPE_STRING);
        $this->addColumn("employee", "health", Schema::TYPE_STRING);
        $this->addColumn("employee", "bank_id", Schema::TYPE_INTEGER);
    }

    public function down()
    {
        $this->dropColumn("employee", "native_land");
        $this->dropColumn("employee", "ethnic_group");
        $this->dropColumn("employee", "religion");
        $this->dropColumn("employee", "employee_coefficient");//ngach cong chuc
        $this->dropColumn("employee", "height");
        $this->dropColumn("employee", "weight");
        $this->dropColumn("employee", "blood_group");
        $this->dropColumn("employee", "health");
        $this->dropColumn("employee", "bank_id");
        

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
