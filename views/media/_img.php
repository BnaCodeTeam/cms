<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
//$model->createImageThumb();
?>
<div class="post" style="margin-top: 5px;">
    <?php 
    $title = ($model->author_id && $model->author_id != NULL)?$model->title . " (Ảnh: ".app\models\Employee::getAliasAuthorById($model->author_id).")":$model->title . " (Ảnh: ".app\models\Employee::getAliasAuthorById($model->created_by).")";
    $url = ($model->google_drive_link && $model->google_drive_link != NULL && $model->google_drive_link != '')?$model->google_drive_link:$model->url
    ?>
    <div class="col-xs-6 col-md-3" id="<?php echo 'media-item-'.$model->id;?>">
        <?php if($model->type == \app\models\Media::TYPE_PHOTO): ?>
        <div class="thumbnail">
            <a href="<?=$url?>" target="_blank" title="<?=$title;?>"><img src="<?=$model->getThumbName();?>" alt="<?=$title;?>"></a>
            <div class="caption">
                <p class="hls-caption" style="display: block; line-height: 18px; max-height: 36px; overflow: hidden;  height: 36px;"><?=$title?></p>
                <p>
                    <a style="margin-bottom: 5px;" href="<?=$url;?>" title="Download" download class="btn btn-primary" role="button"><span class="glyphicon glyphicon-download-alt"></span></a>
                    <a style="margin-bottom: 5px;" href="/media/update?id=<?=$model->id;?>" style="margin-left: 5px;" title="Sửa" class="btn btn-info" role="button"><span class="glyphicon glyphicon-edit"></span></a>
                    <a target="_blank" style="margin-bottom: 5px;" href="/media/editor?id=<?=$model->id;?>" style="margin-left: 5px;" title="Trình chỉnh sửa" class="btn btn-success" role="button"><span class="glyphicon glyphicon-eye-open"></span></a>
                    
                    <a style="margin-bottom: 5px;" 
                       class="btn btn-danger media-item-delete" 
                       style="margin-left: 5px;" 
                       title="Xóa" aria-label="Xóa" 
                       data-media-id="<?=$model->id;?>">
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
                </p>
            </div>
        </div>
        <?php else:?>
            
        <?php endif;?>
    </div>
</div>
