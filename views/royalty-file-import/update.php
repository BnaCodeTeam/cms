<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RoyaltyFileImport */

$this->title = 'Update Royalty File Import: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Royalty File Imports', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="royalty-file-import-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
