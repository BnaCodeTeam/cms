<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "royalty".
 *
 * @property integer $id
 * @property string $full_name
 * @property string $royalty_month
 * @property string $royalty_week
 * @property string $address
 * @property integer $article_count
 * @property integer $post_count
 * @property integer $photo_count
 * @property double $rate
 * @property double $royalty_amount
 * @property double $tax_amount
 * @property double $royalty_total_amount
 * @property string $paid_type
 * @property string $email
 * @property string $created_by
 * @property string $updated_by
 * @property integer $created_at
 * @property integer $is_sended
 * @property integer $sended_at
 */
class Royalty extends \yii\db\ActiveRecord
{
    const IS_SENDED = 1;
    const IS_NOT_SENDED = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'royalty';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address'], 'string'],
            [['article_count', 'post_count', 'photo_count', 'created_at', 'is_sended', 'sended_at'], 'integer'],
            [['rate', 'royalty_amount', 'tax_amount', 'royalty_total_amount'], 'number'],
            [['created_at'], 'required'],
            [['full_name', 'royalty_month', 'royalty_week', 'paid_type', 'email', 'created_by', 'updated_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'Họ tên',
            'royalty_month' => 'Tháng',
            'royalty_week' => 'Tuần',
            'address' => 'Địa chỉ',
            'article_count' => 'Số bài',
            'post_count' => 'Số tin',
            'photo_count' => 'Số ảnh',
            'rate' => 'Hệ số',
            'royalty_amount' => 'Nhuận bút',
            'tax_amount' => 'Trừ thuế',
            'royalty_total_amount' => 'Thực nhận',
            'paid_type' => 'Kiểu nhận',
            'email' => 'Email',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'is_sended' => 'Đã gửi mail',
            'sended_at' => 'Gửi mail lúc',
        ];
    }
    
    public static function getSendLabels() {
        return [
            self::IS_NOT_SENDED => 'Chưa gửi email',
            self::IS_SENDED => 'Đã gửi email'
        ];
    }
}
