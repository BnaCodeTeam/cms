<?php

use yii\db\Migration;

/**
 * Class m181023_031124_add_column_category_id_2_MEDIA_ALBUM
 */
class m181023_031124_add_column_category_id_2_MEDIA_ALBUM extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('media_album', 'category_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return $this->dropColumn('media_album', 'category_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181023_031124_add_column_category_id_2_MEDIA_ALBUM cannot be reverted.\n";

        return false;
    }
    */
}
