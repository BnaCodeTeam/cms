<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DeclareAdChild;

/**
 * DeclareAdChildSearch represents the model behind the search form about `app\models\DeclareAdChild`.
 */
class DeclareAdChildSearch extends DeclareAdChild
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'declare_ad_id', 'ad_type', 'ad_from_time', 'ad_to_time', 'created_at', 'updated_at'], 'integer'],
            [['ad_note', 'created_by', 'updated_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DeclareAdChild::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'declare_ad_id' => $this->declare_ad_id,
            'ad_type' => $this->ad_type,
            'ad_from_time' => $this->ad_from_time,
            'ad_to_time' => $this->ad_to_time,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'ad_note', $this->ad_note])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by]);

        return $dataProvider;
    }
}
