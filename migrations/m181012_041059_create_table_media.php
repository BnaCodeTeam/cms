<?php

use yii\db\Migration;

/**
 * Class m181012_041059_create_table_media
 */
class m181012_041059_create_table_media extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('media', [
            'id' => $this->primaryKey(),
            
            'title' => $this->string(),
            'description' => $this->string(),
            'url' => $this->text(),
            
            'type' => $this->smallInteger(),
            'status' => $this->smallInteger()->defaultValue(10),
            
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            
            'created_by' => $this->string(),
            'updated_by' => $this->string(),
            
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('media');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181012_041059_create_table_media cannot be reverted.\n";

        return false;
    }
    */
}
