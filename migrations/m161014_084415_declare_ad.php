<?php

use yii\db\Migration;

class m161014_084415_declare_ad extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('declare_ad', [
            'id' => $this->primaryKey(),
            'ad_partner' => $this->string(),
            'ad_type' => $this->integer(),
            'ad_from_time' => $this->integer(),
            'ad_to_time' => $this->integer(),
            'ad_note' => $this->text(),
            'ad_price' => $this->double(),
            'ad_created_by' => $this->integer(),
            'ad_pay_status' => $this->integer(),
            'ad_pay_note' => $this->text(),
            'remind_status' => $this->integer(),
            'status' => $this->integer(),
            'created_by' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);
    }

    public function down()
    {
        return $this->dropTable('declare_ad');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
