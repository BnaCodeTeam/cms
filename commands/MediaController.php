<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use \Yii;


use Google_Client;
use Google_Service_Drive;
use Google_Service_Drive_DriveFile;
use Google_Service_Drive_Permission;

use app\models\Media;
use app\models\MediaAlbum;
use app\models\MediaCategory;

ini_set('max_execution_time', 3000); 

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
Yii::setAlias('@webdir', realpath(dirname(__FILE__).'/../web'));
class MediaController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";
    }
    
    public function actionList() {
        $client = $this->getClient();
        $service = new Google_Service_Drive($client);
        $optParams = array(
            'pageSize' => 10,
            'fields' => 'nextPageToken, files(id, name, webContentLink, webViewLink)'
        );
        $results = $service->files->listFiles($optParams);
        if(count($results->getFiles()) == 0) {
            print "No files found.\n";
        } else {
            print "Files:\n";
            foreach($results->getFiles() as $file) {
                printf("%s (%s) (%s)\n", $file->getName(), $file->getId(), $file->getWebViewLink());
            }
        }
    }
    
    public function actionGetImage($fileId='1fNKkqx9b-hO51baQ7M7gseI1EwEuAlU6') {
        $client = $this->getClient();
        $service = new Google_Service_Drive($client);
        $response = $service->files->get($fileId, array('alt' => 'media'));
        $content = $response->getBody()->getContents();
        $imgThumb = $this->thumbnail($content);
        //file_put_contents('test-img/test-image-112.jpg', $imgThumb);
        exit($imgThumb);
        //exit($content);
    }
    
    protected function thumbnail($content, $width = 900, $height = true) {
        // download and create gd image
        $image = ImageCreateFromString($content);
        // calculate resized ratio
        // Note: if $height is set to TRUE then we automatically calculate the height based on the ratio
        $height = $height === true ? (ImageSY($image) * $width / ImageSX($image)) : $height;
        // create image 
        $output = ImageCreateTrueColor($width, $height);
        ImageCopyResampled($output, $image, 0, 0, 0, 0, $width, $height, ImageSX($image), ImageSY($image));

        ob_start();
        imagepng($output);
        $contents =  ob_get_contents();
        ob_end_clean();

        imagedestroy($image);
        return $contents;
   }


    /**
     * 
     */
    public function actionSync() {
        echo "\n\nStart at: ".date("d-m-Y H:i:s", time())."\n";
        $client = $this->getClient();
        $service = new Google_Service_Drive($client);
        $this->createCategory($service);
        $this->createAlbum($service);
        $this->createFile($service);
    }
    
    /**
     * Cập nhật quyền anyone cho category trên google drive
     */
    public function actionUpdatePermission() {
        $categories = MediaCategory::find()->where(['AND', ['status' => MediaCategory::STATUS_ACTIVE], ['IS NOT', 'google_drive_id', NULL]])->all();
        if($categories && !empty($categories)) {
            $client = $this->getClient();
            $service = new Google_Service_Drive($client);
            foreach($categories as $cate) {
                $this->setPermission($service, $cate->google_drive_id, ['type' => 'anyone', 'role' => 'reader']);
            }
        }
    }
    
    /*
     * Remove all file has been deleted
     */
    public function actionDeleteMedia() {
        $mediaUrls = \app\models\Media::find()->select(['url'])->where(['status' => \app\models\Media::STATUS_DELETE])->all();
        $this->deleteMediaByUrls($mediaUrls);
    }
    
    /**
     * Remove all file has been sync
     */
    public function actionMoveStore() {
        $mediaUrls = \app\models\Media::find()
                ->select(['url'])
                ->where(['AND', 
                    ['status' => \app\models\Media::STATUS_ACTIVE], 
                    ['IS NOT', 'google_drive_id', NULL], 
                    ['IS NOT', 'google_drive_link', NULL]])
                ->all();
        
        $this->deleteMediaByUrls($mediaUrls);
    }
    
    
    protected function deleteMediaByUrls($mediaUrls) {
        if($mediaUrls && !empty($mediaUrls)) {
            foreach ($mediaUrls as $url) {
                $file = Yii::getAlias('@webdir'.$url->url);
                echo $file . "\n";
                if(file_exists($file)) {
                    echo ((unlink($file))? 'Deleted':'')."\n";
                }
            }
        }
    }

    protected function createCategory($service) {
        $categories = MediaCategory::find()->where(['AND', ['status' => MediaCategory::STATUS_ACTIVE], ['IS', 'google_drive_id', NULL]])->all();
        if($categories && !empty($categories)) {
            foreach($categories as $cate) {
                $fileMetadata = new Google_Service_Drive_DriveFile(['name' => $cate->name, 'mimeType' => 'application/vnd.google-apps.folder']);
                $folder = $service->files->create($fileMetadata, array('fields' => 'id, webViewLink'));
                if($folder) {
                    $cate->google_drive_link = $folder->webViewLink;
                    $cate->google_drive_id = $folder->id;
                    $cate->save();
                    printf("Folder ID (CATEGORY): %s\n", $folder->id);
                    $this->setPermission($service, $folder->id, ['type' => 'anyone', 'role' => 'reader']);
                }
            }
        }
    }
    
    protected function setPermission($service, $id, $permission) {
        $newPermission = new Google_Service_Drive_Permission();
        $newPermission->setType($permission['type']);
        $newPermission->setRole($permission['role']);
        try {
            $service->permissions->create($id, $newPermission);
        } catch (Exception $e) {
            print "An error occurred (SET PERMISSIONS): " . $e->getMessage(). "\n";
        }
    }

    protected function createAlbum($service) {
        $albums = (new \yii\db\Query())
                ->select(['{{media_album}}.*', '{{media_category}}.`google_drive_id` AS category_google_drive_id', '{{media_category}}.`google_drive_link` AS category_google_drive_link'])
                ->from('media_album')
                ->join('LEFT JOIN','media_category','media_album.`category_id` = media_category.`id`')
                ->where(['AND', 
                    ['media_album.`status`' => MediaAlbum::STATUS_ACTIVE], 
                    ['IS NOT', 'media_category.google_drive_id', NULL], 
                    ['IS', 'media_album.`google_drive_id`', NULL]
                ])->all();
        if($albums && !empty($albums)) {
            foreach ($albums as $al) {
                $fileMetadata = new Google_Service_Drive_DriveFile([
                    'name' => $al['name'], 
                    'mimeType' => 'application/vnd.google-apps.folder',
                    'parents' => array($al['category_google_drive_id'])
                ]);
                $folder = $service->files->create($fileMetadata, array('fields' => 'id, webViewLink'));
                if($folder) {
                    MediaAlbum::updateAll(['google_drive_link' => $folder->webViewLink, 'google_drive_id' => $folder->id], ['id' => $al['id']]);
                    printf("Folder ID (ALBUM): %s\n", $folder->id);
                }
            }
        }
    }
    
    protected function createFile($service) {
        $page = 1;
        $limit = 10;
        while(true) {
            $offset = ($page - 1) * $limit;
            echo "### PAGE ".$page."\n";
            
            $media = (new \yii\db\Query())
                ->select(['{{media}}.*', '{{media_album}}.`google_drive_id` AS album_google_drive_id', '{{media_album}}.`google_drive_link` AS album_google_drive_link'])
                ->from('media')
                ->join('LEFT JOIN', 'media_album','media.`album_id` = media_album.`id`')
                ->where(['AND',
                    ['media.`status`' => Media::STATUS_ACTIVE],
                    ['IS NOT', 'media_album.google_drive_id', NULL], 
                    ['IS', 'media.`google_drive_id`', NULL]
                ])
                ->offset($offset)
                ->limit($limit)
                ->all();
            
            if($media && !empty($media)) {
                foreach($media as $med) {
                    $author = '';
                    if($med['author_id'] && $med['author_id'] != NULL) {
                        $author = \app\models\Employee::getAliasAuthorById($med['author_id']);
                    }
                    if($author != '') {
                        $author = ' (Ảnh: '.$author.')';
                    }
                    $fileMetadata = new Google_Service_Drive_DriveFile(array(
                        'name' => $med['title'].$author,
                        'parents' => array($med['album_google_drive_id'])
                    ));
                    $content = file_get_contents(Yii::getAlias('@webdir'.$med['url']));
                    $file = $service->files->create($fileMetadata, array(
                        'data' => $content,
                        'mimeType' => 'image/jpeg',
                        'uploadType' => 'multipart',
                        'fields' => 'id, webViewLink'));
                    if($file) {
                        Media::updateAll(['google_drive_link' => $file->webViewLink, 'google_drive_id' => $file->id], ['id' => $med['id']]);
                        printf("File ID: %s\n", $file->id);
                    }
                }
            } else {
                break;
            }
            
            $page ++;
        }
    }
    
    protected function getClient() {
        $client = new Google_Client();
        $client->setApplicationName('Google Drive API PHP');
        $client->setScopes(Google_Service_Drive::DRIVE_METADATA_READONLY);
        $client->addScope(Google_Service_Drive::DRIVE_FILE);
        $client->addScope(Google_Service_Drive::DRIVE_READONLY);
        $client->setAuthConfig(\Yii::$app->params['credentials_dir']);
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');

        $tokenPath = \Yii::$app->params['token_dir'];
        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $client->setAccessToken($accessToken);
        }
        
        // If there is no previous token or it's expired.
        if ($client->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                // Request authorization from the user.
                $authUrl = $client->createAuthUrl();
                printf("Open the following link in your browser:\n%s\n", $authUrl);
                print 'Enter verification code: ';
                $authCode = trim(fgets(STDIN));

                // Exchange authorization code for an access token.
                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                $client->setAccessToken($accessToken);

                // Check to see if there was an error.
                if (array_key_exists('error', $accessToken)) {
                    throw new Exception(join(', ', $accessToken));
                }
            }
            // Save the token to a file.
            if (!file_exists(dirname($tokenPath))) {
                mkdir(dirname($tokenPath), 0700, true);
            }
            file_put_contents($tokenPath, json_encode($client->getAccessToken()));
        }
        return $client;
    }
}
