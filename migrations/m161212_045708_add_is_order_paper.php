<?php

use yii\db\Migration;

class m161212_045708_add_is_order_paper extends Migration
{
    public function up()
    {
        $this->addColumn('declare_ad', 'is_order_paper', $this->integer());
        $this->addColumn('declare_ad', 'order_paper_number', $this->integer());
        $this->addColumn('declare_ad', 'order_paper_price', $this->double());
    }

    public function down()
    {
        $this->dropColumn('declare_ad', 'is_order_paper');
        $this->dropColumn('declare_ad', 'order_paper_number');
        $this->dropColumn('declare_ad', 'order_paper_price');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
