<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DeclareAd */

$this->title = '#' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Quản lý quảng cáo', 'url' => ['index2']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="declare-ad-view">

    <h1><?= Html::encode('Chi tiết hợp đồng: ' . $this->title) ?></h1>
    <?php
    $originString = '';
    if(isset($dataOrigin) && !empty($dataOrigin)) {
        $originString = 'Hợp đồng yêu cầu ';
        foreach($dataOrigin as $type => $val) {
            $originString = $originString. $val . ' ' .app\models\DeclareAdPakageInfo::getAdTypeLabels()[$type] . ', '; 
        }
        $originString = "<b style='color:brown'>" . trim($originString, ", ") . "</b>";
    }
    
    $devployString = '';
    $listString = '';
    if(isset($dataDeploy) && !empty($dataDeploy)) {
        $devployString = 'Đã thực hiện ';
        $listString = "Danh sách đã thực hiện: ";
        foreach ($dataDeploy as $devType => $devArray) {
            $number = sizeof($devArray);
            $devployString = $devployString . $number . ' ' . app\models\DeclareAdPakageInfo::getAdTypeLabels()[$devType] . ', ';
            foreach($devArray as $idx) {
                $listString = $listString . "<span class='label label-success'><a href='".Yii::$app->urlManager->createUrl(['/declare-ad-child/view', 'id' => $idx])."'>#".$idx."</a></span>, ";
            }
            //$listString = implode("</a></span>, <span class='label label-success'><a href='".Yii::$app->urlManager->createUrl(['/declare-ad-child/view', 'id' => ''])."'>", $devArray);
        }
       
        $devployString = "<b style='color:blue'>". trim($devployString, ", "). "</b>";
        $listString = trim($listString, ", ");
        
    }
    
    ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'declare_ad_code',
            //'ad_partner',
            [
                'label' => 'Đối tác', 
                'format' => 'raw', 
                'value' =>  $model->adPartner->name
            ],
            //'ad_type',
            [
                'label' => 'Loại hình',
                'format' => 'raw',
                'value' => (isset(app\models\DeclareAd::getAdTypeLabels()[$model->ad_type]))? 
                app\models\DeclareAd::getAdTypeLabels()[$model->ad_type] : ''
            ],
            //'ad_from_time:datetime',
            [
                'label' => 'Bắt đầu',
                'format' => 'raw',
                'value' => date('d-m-Y', $model->ad_from_time)
            ],
            //'ad_to_time:datetime',
            [
                'label' => 'Kết thúc',
                'format' => 'raw',
                'value' => date('d-m-Y', $model->ad_to_time)
            ],
            'ad_note:ntext',
            //'ad_price',
            [
                'label' => 'Giá',
                'format' => 'raw',
                //'value' => number_format($model->ad_price, 0, ',', '.') . 'đ'
                'value' => number_format($model->ad_price + (($model->is_order_paper == app\models\DeclareAd::IS_ORDER_PAPER)?($model->order_paper_number*$model->order_paper_price):0), 0, ',', '.') . 'đ' . ' = ' . 
                            number_format($model->ad_price, 0, ',', '.') . 'đ + ' . 
                            number_format((($model->is_order_paper == app\models\DeclareAd::IS_ORDER_PAPER)?($model->order_paper_number*$model->order_paper_price):0), 0, ',', '.') . 'đ'
            ],
            
            //'ad_created_by',
            [
                'label' => 'Người làm hợp đồng',
                'format' => 'raw',
                'value' => $model->getEmployeeCreateInfo()
            ],
            //'deploy_employee',
            [
                'label' => 'Người triển khai',
                'format' => 'raw',
                'value' => $model->getEmployeeDeployInfo()
            ],
            //'ad_pay_status',
            /*[
                'label' => 'Trạng thái thanh toán',
                'format' => 'raw',
                'value' => (isset(app\models\DeclareAd::getPaidStatusLabels()[$model->ad_pay_status]))? 
                app\models\DeclareAd::getPaidStatusLabels()[$model->ad_pay_status] : ''
            ],*/
            
            [
                'label' => 'Mẫu hợp đồng',
                'format' => 'raw',
                'value' => $model->viewDeclareTemplate()
            ],
            
            [
                'label' => 'Trạng thái thanh toán',
                'format' => 'raw',
                'value' => $model->viewPaidStatus()
            ],
            
            'ad_pay_note:ntext',
            //'remind_status',
            [
                'label' => 'Tình trạng thông báo',
                'format' => 'raw',
                'value' => (isset(app\models\DeclareAd::getRemindStatusLabels()[$model->remind_status]))? 
                app\models\DeclareAd::getRemindStatusLabels()[$model->remind_status] : 'Chưa thông báo'
            ],
            ($model->ad_type == \app\models\DeclareAd::AD_TYPE_PACKAGE)?
            [
                'label' => 'Tình trạng thực hiện hợp đồng',
                'format' => 'raw',
                'value' => $originString . "<br/>" . $devployString . "<br/>" .$listString
            ]:['label' => 'Tình trạng thực hiện hợp đồng',
                'format' => 'raw',
                'value' => isset(\app\models\DeclareAd::getStatusLabels()[$model->status])?\app\models\DeclareAd::getStatusLabels()[$model->status]:'',
            ],
            //'created_by',
            //'updated_by',
            [
                'label' => 'Đăng ký đặt báo',
                'format' => 'raw',
                'value' => (isset(app\models\DeclareAd::getIsOrderPaperLabels()[$model->is_order_paper]))?
                '<p><span class="label label-info">'.app\models\DeclareAd::getIsOrderPaperLabels()[$model->is_order_paper].'</span></p>'
                .(($model->is_order_paper == app\models\DeclareAd::IS_ORDER_PAPER)
                        ?'<p>Số lượng: '.$model->order_paper_number.'</p><p> Giá tiền: '.number_format($model->order_paper_price, 0, ',', '.').'đ'.'</p>':''):''
            ],
            [
                'label' => 'Tạo bởi',
                'format' => 'raw',
                'value' => $model->created_by.' <i style="color:brown;">('.date('d-m-Y H:i:s', $model->created_at).')</i>'
            ],
            [
                'label' => 'Cập nhật bởi',
                'format' => 'raw',
                'value' => ((isset($model->updated_by))?$model->updated_by.' <i style="color:brown;">('. date('d-m-Y H:i:s', $model->updated_at).')</i>':'')
            ],
        ],
    ]) ?>

</div>
