<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\web\View;

use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $model app\models\DeclareAd */

$this->title = '#' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Quản lý quảng cáo', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="declare-ad-view">

    <h1><?= Html::encode('Chi tiết hợp đồng: ' . $this->title) ?></h1>
    <?php if(Yii::$app->session->hasFlash('error')):?>
    <div class="alert alert-danger" role="alert">
        <?= Yii::$app->session->getFlash('error'); ?>
    </div>
    <?php endif;?>
    <?php if(Yii::$app->session->hasFlash('success')):?>
    <div class="alert alert-success" role="alert">
        <?= Yii::$app->session->getFlash('success'); ?>
    </div>
    <?php endif;?>
   
    <p>
        <?= Html::a('Sửa', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        
        <?php if($model->ad_type == \app\models\DeclareAd::AD_TYPE_PACKAGE):?>
        <?= Html::a('Triển khai mục con', ['create-child-ad', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?php endif;?>
        
        <?php 
        if($model->ad_type != \app\models\DeclareAd::AD_TYPE_PACKAGE 
                && $model->status != \app\models\DeclareAd::STATUS_DONE):
            echo Html::a('Đã triển khai', ['deploy', 'id' => $model->id], 
                [
                    'class' => 'btn btn-info',
                    'data' => [
                        'confirm' => 'Bạn có chắc hợp đồng này đã được triển khai?',
                    ],
                ]);
        endif;
        ?>
        <?php 
        if($model->ad_pay_status != \app\models\DeclareAd::PAY_STATUS_PAID):
            echo Html::a('Đã thanh toán', ['paid', 'id' => $model->id], 
                [
                    'class' => 'btn btn-warning',
                    'data' => [
                        'confirm' => 'Bạn có chắc hợp đồng này đã được thanh toán?',
                    ],
                ]);
        endif;
        ?>
        
        <?= Html::a('Xóa', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Bạn có chắc muốn xóa hợp đồng này',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php
    $originString = '';
    if(isset($dataOrigin) && !empty($dataOrigin)) {
        $originString = 'Hợp đồng yêu cầu ';
        foreach($dataOrigin as $type => $val) {
            $originString = $originString. $val . ' ' .app\models\DeclareAdPakageInfo::getAdTypeLabels()[$type] . ', '; 
        }
        $originString = "<b style='color:brown'>" . trim($originString, ", ") . "</b>";
    }
    
    $devployString = '';
    $listString = '';
    if(isset($dataDeploy) && !empty($dataDeploy)) {
        $devployString = 'Đã thực hiện ';
        $listString = 'Danh sách đã thực hiện: <button type="button" id="x" class="btn btn-success btn-xs" data-toggle="modal" >Xem</button>';
        foreach ($dataDeploy as $devType => $devArray) {
            $number = sizeof($devArray);
            $devployString = $devployString . $number . ' ' . app\models\DeclareAdPakageInfo::getAdTypeLabels()[$devType]. ', ';
        }
       
        $devployString = "<b style='color:blue'>". trim($devployString, ", "). "</b>";
    }
    
    ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'declare_ad_code',
            //'ad_partner',
            [
                'label' => 'Đối tác', 
                'format' => 'raw', 
                'value' =>  $model->adPartner->name
            ],
            //'ad_type',
            [
                'label' => 'Loại hình',
                'format' => 'raw',
                'value' => (isset(app\models\DeclareAd::getAdTypeLabels()[$model->ad_type]))? 
                app\models\DeclareAd::getAdTypeLabels()[$model->ad_type] : ''
            ],
            //'ad_from_time:datetime',
            [
                'label' => 'Bắt đầu',
                'format' => 'raw',
                'value' => date('d-m-Y', $model->ad_from_time)
            ],
            //'ad_to_time:datetime',
            [
                'label' => 'Kết thúc',
                'format' => 'raw',
                'value' => date('d-m-Y', $model->ad_to_time)
            ],
            'ad_note:ntext',
            //'ad_price',
            [
                'label' => 'Giá = Giá HĐ + Giá báo (Nếu có)',
                'format' => 'raw',
                'value' => number_format($model->ad_price + (($model->is_order_paper == app\models\DeclareAd::IS_ORDER_PAPER)?($model->order_paper_number*$model->order_paper_price):0), 0, ',', '.') . 'đ' . ' = ' . 
                            number_format($model->ad_price, 0, ',', '.') . 'đ + ' . 
                            number_format((($model->is_order_paper == app\models\DeclareAd::IS_ORDER_PAPER)?($model->order_paper_number*$model->order_paper_price):0), 0, ',', '.') . 'đ'
            ],
            
            //'ad_created_by',
            [
                'label' => 'Người làm hợp đồng',
                'format' => 'raw',
                'value' => $model->getEmployeeCreateInfo()
            ],
            //'deploy_employee',
            [
                'label' => 'Người triển khai',
                'format' => 'raw',
                'value' => $model->getEmployeeDeployInfo()
            ],
            //'ad_pay_status',
            /*[
                'label' => 'Trạng thái thanh toán',
                'format' => 'raw',
                'value' => (isset(app\models\DeclareAd::getPaidStatusLabels()[$model->ad_pay_status]))? 
                app\models\DeclareAd::getPaidStatusLabels()[$model->ad_pay_status] : ''
            ],*/
            
            [
                'label' => 'Mẫu hợp đồng',
                'format' => 'raw',
                'value' => $model->viewDeclareTemplate()
            ],
            
            [
                'label' => 'Trạng thái thanh toán',
                'format' => 'raw',
                'value' => $model->viewPaidStatus()
            ],
            
            'ad_pay_note:ntext',
            //'remind_status',
            [
                'label' => 'Tình trạng thông báo',
                'format' => 'raw',
                'value' => (isset(app\models\DeclareAd::getRemindStatusLabels()[$model->remind_status]))? 
                app\models\DeclareAd::getRemindStatusLabels()[$model->remind_status] : 'Chưa thông báo'
            ],
            ($model->ad_type == \app\models\DeclareAd::AD_TYPE_PACKAGE)?
            [
                'label' => 'Tình trạng thực hiện hợp đồng',
                'format' => 'raw',
                'value' => $originString . "<br/>" . $devployString . "<br/>" .$listString
            ]:['label' => 'Tình trạng thực hiện hợp đồng',
                'format' => 'raw',
                'value' => isset(\app\models\DeclareAd::getStatusLabels()[$model->status])?\app\models\DeclareAd::getStatusLabels()[$model->status]:'',
            ],
            //'created_by',
            //'updated_by',
            [
                'label' => 'Đăng ký đặt báo',
                'format' => 'raw',
                'value' => (isset(app\models\DeclareAd::getIsOrderPaperLabels()[$model->is_order_paper]))?
                '<p><span class="label label-info">'.app\models\DeclareAd::getIsOrderPaperLabels()[$model->is_order_paper].'</span></p>'
                .(($model->is_order_paper == app\models\DeclareAd::IS_ORDER_PAPER)
                        ?'<p>Số lượng: '.$model->order_paper_number.'</p><p> Giá tiền: '.number_format($model->order_paper_price, 0, ',', '.').'đ'.'</p>':''):''
            ],
            [
                'label' => 'Tạo bởi',
                'format' => 'raw',
                'value' => $model->created_by.' <i style="color:brown;">('.date('d-m-Y H:i:s', $model->created_at).')</i>'
            ],
            [
                'label' => 'Cập nhật bởi',
                'format' => 'raw',
                'value' => ((isset($model->updated_by))?$model->updated_by.' <i style="color:brown;">('. date('d-m-Y H:i:s', $model->updated_at).')</i>':'')
            ],
        ],
    ]) ?>
    
    <?php
    if($model->ad_type == app\models\DeclareAd::AD_TYPE_PACKAGE):
    $urlGetChildList = Yii::$app->urlManager->createUrl(['declare-ad/ajax-get-child-list','id' => $model->id]);
    $js = '
    function loadModal(){
        $("#x").click(function() {
            $.ajax({
                url: "'.$urlGetChildList.'",
                beforeSend: function() {}
            }).done(function(data) {
                if(data.length > 0) {
                    var dataShow = "";
                    for(var i in data) {
                        var tt = parseInt(i)+1;
                        dataShow = dataShow + "<tr>" + 
                            "<th scope=\"row\">"+tt+"</th>" + 
                            "<td><a title=\"Sửa mục này\" href=\"/declare-ad-child/update?id="+data[i].id+"\">"+data[i].title+"</a></td>" +
                            "<td>"+data[i].ad_type_label+"</td>" +
                            "<td>"+data[i].ad_from_date+" - "+data[i].ad_to_date+"</td>" +
                            "<td>"+data[i].deploy_employee_full_name+"</td>" +
                            "<td>"+data[i].total_price_format+"</td>" +
                        "</tr>";
                    }
                    $("#data-show").html(dataShow);
                    $("#myModal").modal("show");
                }
            });
            
        });
    };
    loadModal(); 
    ';
    $this->registerJs($js, View::POS_END);
    ?>
    
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><b><?php echo $model->ad_partner ?></b></h4>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered"> 
                        <thead> 
                            <tr> 
                                <th>#</th> 
                                <th>Tên tác phẩm</th> 
                                <th>Loại hình</th> 
                                <th>Ngày đăng</th> 
                                <th>Tác giả</th> 
                                <th>Giá</th> 
                            </tr> 
                        </thead> 
                        <tbody id="data-show"> 
                            <tr> 
                                <th scope="row">1</th> 
                                <td>Mark</td> 
                                <td>Otto</td> 
                                <td>@mdo</td> 
                                <td>@mdo</td> 
                            </tr>  
                        </tbody> 
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
                    <?php if(isset($dataExport['provider']) && isset($dataExport['gridColumns'])):
                        echo ExportMenu::widget([
                            'dataProvider' => $dataExport['provider'],
                            'columns' => $dataExport['gridColumns'],
                            'fontAwesome' => true,
                            'target' => ExportMenu::TARGET_SELF,
                            'showColumnSelector' => false,
                            'dropdownOptions' => [
                                'class' => 'btn btn-primary',
                                'label' => 'Xuất phiếu'
                            ],
                            'exportConfig' => [
                                ExportMenu::FORMAT_TEXT => false,
                                ExportMenu::FORMAT_HTML => false,
                                ExportMenu::FORMAT_CSV => false,
                                ExportMenu::FORMAT_PDF => false
                            ]
                        ]);
                    endif;?>
                </div>
            </div>
        </div>
    </div>
    <?php endif;?>
</div>
