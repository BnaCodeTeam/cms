<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ad_partner".
 *
 * @property integer $id
 * @property string $name
 * @property string $created_by
 * @property string $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 */
class AdPartner extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ad_partner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['created_by', 'updated_by', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Tên đối tác',
            'created_by' => 'Tạo bởi',
            'updated_by' => 'Cập nhật bởi',
            'created_at' => 'Tạo lúc',
            'updated_at' => 'Cập nhật lúc',
        ];
    }
    
    public static function getAllAdPartner() {
        $partNers = AdPartner::find()->orderBy(['name'=>SORT_ASC])->all();
        $returnData = [];
        if(!empty($partNers)) {
            foreach($partNers as $part) {
                $returnData[$part->id] = $part->name;
            }
        }
        return $returnData;
    }
}
