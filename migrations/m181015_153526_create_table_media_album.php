<?php

use yii\db\Migration;

/**
 * Class m181015_153526_create_table_media_album
 */
class m181015_153526_create_table_media_album extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('media_album', [
            'id' => $this->primaryKey(),
            
            'name' => $this->string(),
            'description' => $this->string(),
            
            'status' => $this->smallInteger()->defaultValue(10),
            
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            
            'created_by' => $this->string(),
            'updated_by' => $this->string(),
            
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('media_album');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181015_153526_create_table_media_album cannot be reverted.\n";

        return false;
    }
    */
}
