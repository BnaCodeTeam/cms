<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DeclareAdChild */

$this->title = 'Create Declare Ad Child';
$this->params['breadcrumbs'][] = ['label' => 'Declare Ad Children', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="declare-ad-child-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
