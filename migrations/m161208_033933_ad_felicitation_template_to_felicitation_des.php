<?php

use yii\db\Migration;

class m161208_033933_ad_felicitation_template_to_felicitation_des extends Migration
{
    public function up()
    {
        $this->addColumn('felicitation_discipline', 'felicitation_template', $this->string());
    }

    public function down()
    {
        $this->dropColumn('felicitation_discipline', 'felicitation_template');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
