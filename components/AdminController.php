<?php

namespace app\components;

use yii\web\Controller;
use Yii;

class AdminController extends Controller {
    
    public function beforeAction($event) {
        //return true;
        $controller = Yii::$app->controller->id;
        $action = Yii::$app->controller->action->id;
        $userId = Yii::$app->user->id;
        $auth = Yii::$app->authManager;
        $operation = $controller . '/' . $action; 
        
        /*
        if(!isset(Yii::$app->user->identity->id) || !Yii::$app->user->identity->id) {            
            if($operation != 'site/login' 
                    && $operation != 'site/index') {
                return $this->redirect(['/site/login']);
            }
        }
        */
        /**
         * Bỏ check phân quyền tất cả action trong note
         */
        $auth->authNotRequire = [
            'site/login',
            'site/logout',
            'site/error',
            'site/index',
            'employee/reset-password'
        ];       

        if(Yii::$app->user->isGuest && $operation != 'site/login') {
            return $this->redirect(['/site/login']);
        }
        
        if (!$auth->checkAccess($userId, $operation)) {
            echo $this->render('/site/error', [
                'name' => 'Quyền truy cập bị từ chối',
                'message' => 'Bạn không được quyền truy cập khu vực này, Vui lòng liên hệ với quản trị viên để được giúp đỡ.'
            ]);
            return false;
        }
        return parent::beforeAction($event);
    }

    /**
     * Kiểm tra xem người dùng đã đăng nhập vào hệ thống chưa.
     * Nếu chưa đăng nhập thì chuyển sang trang đăng nhập.
     */
    public function checkLogin() {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/site/login']);
        }
    }
}