<?php

use yii\db\Migration;

/**
 * Class m181119_091335_add_column_is_template_2_FC_DOCUMENT
 */
class m181119_091335_add_column_is_template_2_FC_DOCUMENT extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181119_091335_add_column_is_template_2_FC_DOCUMENT cannot be reverted.\n";

        return false;
    }
    */
}
