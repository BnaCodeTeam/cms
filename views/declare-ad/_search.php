<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DeclareAdSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="declare-ad-search">

    <?php $form = ActiveForm::begin([
        'action' => [$actionGet],
        'method' => 'get',
        'options' => [
            'class' => 'form-inline'
        ]
    ]); ?>

    <?php // echo $form->field($model, 'id') ?>

    <?php //echo $form->field($model, 'ad_partner')->textInput(['placeholder' => 'Đối tác'])->label(false)?>
    <?= $form->field($model, 'ad_partner_name')->textInput(['placeholder' => 'Đối tác'])->label(false)?>

    <?= $form->field($model, 'ad_type')->dropDownList(app\models\DeclareAd::getAdTypeLabels(), ['prompt'=>'---- Chọn loại hình quảng cáo ----'])->label(false) ?>

    <?= $form->field($model, 'ad_pay_status')->dropDownList(app\models\DeclareAd::getPaidStatusLabels(), ['prompt' => '---- Trạng thái thanh toán ----'])->label(false) ?>
    
    <div class="form-group">
        <?= Html::submitButton('Lọc', ['class' => 'btn btn-primary', 'style' => 'margin-bottom:10px;']) ?>
        <?php // echo Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
