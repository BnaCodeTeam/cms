<?php

use yii\db\Migration;

/**
 * Class m171213_024637_create_table_royalty
 */
class m171213_024637_create_table_royalty extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('royalty', [
            'id' => $this->primaryKey(),
            'full_name' => $this->string(),
            'royalty_month' => $this->string(),
            'royalty_week' => $this->string(),
            'address' => $this->text(),
            'article_count' => $this->integer(),
            'post_count' => $this->integer(),
            'photo_count' => $this->integer(),
            'rate' => $this->float(),
            'royalty_amount' => $this->double(),
            'tax_amount' => $this->double(),
            'royalty_total_amount' => $this->double(),
            'paid_type' => $this->string(),
            'email' => $this->string(),
            
            
            'created_by' => $this->string(),
            'updated_by' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'is_sended' => $this->integer(),
            'sended_at' => $this->integer()
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        return $this->dropTable('royalty');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171213_024637_create_table_royalty cannot be reverted.\n";

        return false;
    }
    */
}
