<?php

use yii\db\Migration;

class m170309_093335_add_ad_partner_id_to_declare_ad extends Migration
{
    public function up()
    {
        $this->addColumn('declare_ad', 'ad_partner_id', $this->integer());
    }

    public function down()
    {
        return $this->dropColumn('declare_ad', 'ad_partner_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
