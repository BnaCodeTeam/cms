<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RoyaltySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="royalty-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'full_name') ?>

    <?= $form->field($model, 'royalty_month') ?>

    <?= $form->field($model, 'royalty_week') ?>

    <?= $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'article_count') ?>

    <?php // echo $form->field($model, 'post_count') ?>

    <?php // echo $form->field($model, 'photo_count') ?>

    <?php // echo $form->field($model, 'rate') ?>

    <?php // echo $form->field($model, 'royalty_amount') ?>

    <?php // echo $form->field($model, 'tax_amount') ?>

    <?php // echo $form->field($model, 'royalty_total_amount') ?>

    <?php // echo $form->field($model, 'paid_type') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'is_sended') ?>

    <?php // echo $form->field($model, 'sended_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
