<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MediaAlbum */

$this->title = 'Update Media Album: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Media', 'url' => ['/media']];
$this->params['breadcrumbs'][] = ['label' => 'Albums', 'url' => ['/media/album']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="media-album-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form-ajax', [
        'model' => $model,
        'media' => $media
    ]) ?>

</div>
