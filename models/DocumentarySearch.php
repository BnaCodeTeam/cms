<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Documentary;

/**
 * DocumentarySearch represents the model behind the search form about `app\models\Documentary`.
 */
class DocumentarySearch extends Documentary
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'send_by_id', 'receive_by_id', 'send_time', 'receive_time', 'sign', 'created_at', 'updated_at'], 'integer'],
            [['name', 'send_by_name', 'receive_by_name', 'note', 'created_by', 'updated_by'], 'safe'],
            [['send_number', 'receive_number', 'searchByNumber'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Documentary::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            //'send_number' => $this->send_number,
            //'receive_number' => $this->receive_number,
            'send_by_id' => $this->send_by_id,
            'receive_by_id' => $this->receive_by_id,
            'send_time' => $this->send_time,
            'receive_time' => $this->receive_time,
            'sign' => $this->sign,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        if($this->searchByNumber && $this->searchByNumber != '') {
            $query->andFilterWhere([
                'OR',
                ['receive_number' => $this->searchByNumber,'type' => Documentary::TYPE_RECEIVE],
                ['send_number' => $this->searchByNumber, 'type' => Documentary::TYPE_SEND],
            ]);
        }
        
        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'send_by_name', $this->send_by_name])
            ->andFilterWhere(['like', 'receive_by_name', $this->receive_by_name])
            ->andFilterWhere(['like', 'note', $this->note])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by]);

        return $dataProvider;
    }
}
