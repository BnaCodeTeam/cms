<?php

use yii\db\Migration;

/**
 * Class m181023_031116_add_column_category_id_2_MEDIA
 */
class m181023_031116_add_column_category_id_2_MEDIA extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('media', 'category_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return $this->dropColumn('media', 'category_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181023_031116_add_column_category_id_2_MEDIA cannot be reverted.\n";

        return false;
    }
    */
}
