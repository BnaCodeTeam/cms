<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style>
        /*.form-control{height: auto !important;}*/
    </style>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'BNA-ERP',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    
    $auth = Yii::$app->authManager;
    
    $navbarNs = '';
    if(!Yii::$app->user->isGuest) {
        if($auth->checkAccess(Yii::$app->user->identity->id, 'employee/index')) {
            $navbarNs = [
                'label' => 'Nhân sự',                                        
                'items' => [
                    ['label' => 'Danh sách nhân sự', 'url' => ['/employee/index']],
                    ['label' => 'Khen thưởng kỷ luật', 'url' => ['/felicitation-discipline/index']],
                ],
            ];
        } else if($auth->checkAccess(Yii::$app->user->identity->id, 'employee/index2')){
            $navbarNs = [
                'label' => 'Nhân sự',                                        
                'items' => [
                    ['label' => 'Danh sách nhân sự', 'url' => ['/employee/index2']],
                    ['label' => 'Khen thưởng kỷ luật', 'url' => ['/felicitation-discipline/index']],// thêm vào để làm hàng
                ],
            ];
        }
    }
    
    $navbarSys = '';
    if(!Yii::$app->user->isGuest) {
        if($auth->checkAccess(Yii::$app->user->identity->id, 'auth-management/manage')) {
            $navbarSys = [
                'label' => 'Hệ thống',                                        
                'items' => [
                    ['label' => 'Quản lý nhóm quyền', 'url' => ['/auth-management/manage']],
                    ['label' => 'Danh sách quyền', 'url' => ['/auth-management/index']],
                    ['label' => 'Log người dùng', 'url' => ['/action-log/index']],
                ],
            ];
        }
    }
    
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Trang chủ', 'url' => ['/site/index']],
            //['label' => 'About', 'url' => ['/site/about']],
            //['label' => 'Contact', 'url' => ['/site/contact']],
            
            Yii::$app->user->isGuest ? '': $navbarNs,
            
            Yii::$app->user->isGuest ? '': [
                'label' => 'Tài liệu',                                        
                'items' => [
                    ['label' => 'Quản lý công văn', 'url' => ['/documentary/index']],
                    ['label' => 'Đa phương tiện', 'url' => ['/media']],
                ],
            ],
            
            Yii::$app->user->isGuest ? '': [
                'label' => 'Quản lý quảng cáo',                                        
                'items' => [
                    ($auth->checkAccess(Yii::$app->user->identity->id, 'declare-ad'))?['label' => 'Danh sách quảng cáo', 'url' => ['/declare-ad']]:['label' => 'Danh sách quảng cáo', 'url' => ['/declare-ad/index2']],
                    ['label' => 'Quản lý đối tác', 'url' => ['/ad-partner']],
                    ['label' => 'Thống kê quảng cáo', 'url' => ['/declare-ad/statistic']],
                ],
            ],
            
            Yii::$app->user->isGuest ? '': $navbarSys,
            
            Yii::$app->user->isGuest ?
            ['label' => 'Đăng nhập', 'url' => ['/site/login']] :
            [
                'label' => 'Hi! (' . Yii::$app->user->identity->username .')',                                        
                'items' => [
                    ['label' => 'Đăng xuất', 'url' => ['/site/logout'],'linkOptions' => ['data-method' => 'post']],                        
                    ['label' => 'Đổi mật khẩu', 'url' => '/employee/reset-password'],
                ],
            ],
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Báo Nghệ An <?= date('Y') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
