<?php

namespace app\controllers;

use Yii;
use app\models\DeclareAd;
use app\models\DeclareAdSearch;
use app\models\DeclareAdPakageInfo;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;

use yii\data\ActiveDataProvider;
use yii\helpers\Html;

/**
 * DeclareAdController implements the CRUD actions for DeclareAd model.
 */
class DeclareAdController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    /**
     * Lists all DeclareAd models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DeclareAdSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'actionGet' => Yii::$app->controller->action->id,
        ]);
    }
    
    /**
     * Lists all DeclareAd models.
     * @return mixed
     */
    public function actionIndex2()
    {
        $searchModel = new DeclareAdSearch();
        $dataProvider = $searchModel->search2(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'actionGet' => Yii::$app->controller->action->id,
        ]);
    }

    /**
     * Displays a single DeclareAd model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $m = $this->findModel($id);
        $dataInfo = [];
        $dataDeploy = [];
        $dataExport = [];
        if($m->ad_type == DeclareAd::AD_TYPE_PACKAGE) {
            $infoNumbers = DeclareAdPakageInfo::find()->where('declare_ad_id='.$id)->all();
            if($infoNumbers) {
                foreach ($infoNumbers as $inf) {
                    $dataInfo[$inf->ad_type] = $inf->number;
                }
            }
            $deployNumbers = \app\models\DeclareAdChild::find()->where('declare_ad_id='.$id)->all();
            if($deployNumbers) {
                foreach ($deployNumbers as $dep) {
                    $dataDeploy[$dep->ad_type][] = $dep->id;
                }
                
                //Export excel
                $queryExport = \app\models\DeclareAdChild::find()->where('declare_ad_id='.$id);
                $dataProviderExport = new ActiveDataProvider([
                    'query' => $queryExport
                ]);
                $gridColumnsExport = [
                    ['class' => 'kartik\grid\SerialColumn'],
                    'title',
                    [
                        'attribute'=>'ad_type',
                        'label'=>'Loại hình',
                        'vAlign'=>'middle',
                        'value'=>function ($model, $key, $index, $widget) { 
                            return (isset(DeclareAdPakageInfo::getAdTypeLabels()[$model->ad_type])?DeclareAdPakageInfo::getAdTypeLabels()[$model->ad_type]:'');
                        },
                        'format'=>'raw'
                    ],
                    [
                        'attribute'=>'ad_from_time',
                        'label'=>'Ngày bắt đầu đăng',
                        'vAlign'=>'middle',
                        'value'=>function ($model, $key, $index, $widget) { 
                            return (($model->ad_from_time != null)?date('d/m/Y', $model->ad_from_time):'');
                        },
                        'format'=>'raw'
                    ],
                    [
                        'attribute'=>'ad_to_time',
                        'label'=>'Ngày kết thúc đăng',
                        'vAlign'=>'middle',
                        'value'=>function ($model, $key, $index, $widget) { 
                            return (($model->ad_to_time != null)?date('d/m/Y', $model->ad_to_time):'');
                        },
                        'format'=>'raw'
                    ],
                    [
                        'attribute'=>'deploy_employee',
                        'label'=>'Tác giả',
                        'vAlign'=>'middle',
                        'value'=>function ($model, $key, $index, $widget) { 
                            return \app\models\Employee::getEmployeeDeployInfo(false, $model->deploy_employee);
                        },
                        'format'=>'raw'
                    ],
                    'total_price',
                    'ad_note'
                ];
                $dataExport['provider'] = $dataProviderExport;
                $dataExport['gridColumns'] = $gridColumnsExport;
            }
        }
        
        return $this->render('view', [
            'model' => $m,
            'dataOrigin' => $dataInfo,
            'dataDeploy' => $dataDeploy,
            'dataExport' => $dataExport
        ]);
    }
    
    
    /**
     * Displays a single DeclareAd model.
     * @param integer $id
     * @return mixed
     */
    public function actionView2($id)
    {
        if(Yii::$app->user->isGuest) {
            return $this->redirect(['/site/login']);
        }
        $m = $this->findModel($id);
        if((!Yii::$app->user->isGuest) && ($m->isOwner() || \Yii::$app->user->identity->username == \app\models\Employee::DEFULT_SUPPER_USER)) {
            $dataInfo = [];
            $dataDeploy = [];
            if($m->ad_type == DeclareAd::AD_TYPE_PACKAGE) {
                $infoNumbers = DeclareAdPakageInfo::find()->where('declare_ad_id='.$id)->all();
                if($infoNumbers) {
                    foreach ($infoNumbers as $inf) {
                        $dataInfo[$inf->ad_type] = $inf->number;
                    }
                }
                $deployNumbers = \app\models\DeclareAdChild::find()->where('declare_ad_id='.$id)->all();
                if($deployNumbers) {
                    foreach ($deployNumbers as $dep) {
                        $dataDeploy[$dep->ad_type][] = $dep->id;
                    }
                }
            }

            return $this->render('view2', [
                'model' => $m,
                'dataOrigin' => $dataInfo,
                'dataDeploy' => $dataDeploy
            ]);
        } else {
            echo $this->render('/site/error', [
                'name' => 'Quyền truy cập bị từ chối',
                'message' => 'Bạn không được quyền truy cập khu vực này, Vui lòng liên hệ với quản trị viên để được giúp đỡ.'
            ]);
            return false;
        }
    }

    /**
     * Creates a new DeclareAd model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DeclareAd();
        
        if ($model->load(Yii::$app->request->post())) {
            $model->created_at = time();
            $model->updated_at = time();
            $model->created_by = Yii::$app->user->identity->username;
            $post = Yii::$app->request->post('DeclareAd');
            $nuberClip = isset($post['number_clip'])?intval($post['number_clip']):0;
            $nuberArticle = isset($post['number_article'])?intval($post['number_article']):0;
            $nuberBanner = isset($post['number_banner'])?intval($post['number_banner']):0;
            $nuberEArticle = isset($post['number_e_article'])?intval($post['number_e_article']):0;
            
            if(isset($post['EmployeeDeployString']) && !empty($post['EmployeeDeployString'])) {
                $model->deploy_employee = implode(',', $post['EmployeeDeployString']);
            }
            $model->templateFile = UploadedFile::getInstance($model, 'templateFile');
            if($model->templateFile) {
                $model->upload();
            }
            if($model->save()) {
                if($nuberClip > 0) {
                    $infoOne = new DeclareAdPakageInfo();
                    $infoOne->ad_type = DeclareAdPakageInfo::AD_TYPE_VIDEO;
                    $infoOne->declare_ad_id = $model->id;
                    $infoOne->number = $nuberClip;
                    
                    $infoOne->save();
                }
                if($nuberArticle > 0) {
                    $infoTwo = new DeclareAdPakageInfo();
                    $infoTwo->ad_type = DeclareAdPakageInfo::AD_TYPE_ARTICLE;
                    $infoTwo->declare_ad_id = $model->id;
                    $infoTwo->number = $nuberArticle;
                    
                    $infoTwo->save();
                }
                if($nuberBanner > 0) {
                    $infoThree = new DeclareAdPakageInfo();
                    $infoThree->ad_type = DeclareAdPakageInfo::AD_TYPE_BANNER;
                    $infoThree->declare_ad_id = $model->id;
                    $infoThree->number = $nuberBanner;
                    
                    $infoThree->save();
                }
                if($nuberEArticle > 0) {
                    $infoFour = new DeclareAdPakageInfo();
                    $infoFour->ad_type = DeclareAdPakageInfo::AD_TYPE_E_ARTICLE;
                    $infoFour->declare_ad_id = $model->id;
                    $infoFour->number = $nuberEArticle;
                    
                    $infoFour->save();
                }
                $data = ['id' => $model->id, 'ad_partner' => $model->ad_partner];
                \app\models\ActionLog::trackActionLog(['result' => \app\models\ActionLog::RESULT_SUCCESS, 'object' => json_encode($data)]);
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } 
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing DeclareAd model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $numberInfoModel = DeclareAdPakageInfo::find()->where(['declare_ad_id' => $id])->all();
        
        $dataNumber = [];
        foreach($numberInfoModel as $info) {
            $dataNumber[$info->ad_type] = $info->number;
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->updated_at = time();
            $model->updated_by = Yii::$app->user->identity->username;
            $post = Yii::$app->request->post('DeclareAd');
            $nuberClip = isset($post['number_clip'])?intval($post['number_clip']):0;
            $nuberArticle = isset($post['number_article'])?intval($post['number_article']):0;
            $nuberBanner = isset($post['number_banner'])?intval($post['number_banner']):0;
            $nuberEArticle = isset($post['number_e_article'])?intval($post['number_e_article']):0;
            
            if(isset($post['EmployeeDeployString']) && !empty($post['EmployeeDeployString'])) {
                $model->deploy_employee = implode(',', $post['EmployeeDeployString']);
            }
            $model->templateFile = UploadedFile::getInstance($model, 'templateFile');
            if($model->templateFile) {
                $model->upload();
            }
            if($model->save()) {
                DeclareAdPakageInfo::deleteAll("declare_ad_id={$model->id}");
                if($nuberClip > 0) {
                    $infoOne = new DeclareAdPakageInfo();
                    $infoOne->ad_type = DeclareAdPakageInfo::AD_TYPE_VIDEO;
                    $infoOne->declare_ad_id = $model->id;
                    $infoOne->number = $nuberClip;
                    
                    $infoOne->save();
                }
                if($nuberArticle > 0) {
                    $infoTwo = new DeclareAdPakageInfo();
                    $infoTwo->ad_type = DeclareAdPakageInfo::AD_TYPE_ARTICLE;
                    $infoTwo->declare_ad_id = $model->id;
                    $infoTwo->number = $nuberArticle;
                    
                    $infoTwo->save();
                }
                if($nuberBanner > 0) {
                    $infoThree = new DeclareAdPakageInfo();
                    $infoThree->ad_type = DeclareAdPakageInfo::AD_TYPE_BANNER;
                    $infoThree->declare_ad_id = $model->id;
                    $infoThree->number = $nuberBanner;
                    
                    $infoThree->save();
                }
                if($nuberEArticle > 0) {
                    $infoFour = new DeclareAdPakageInfo();
                    $infoFour->ad_type = DeclareAdPakageInfo::AD_TYPE_E_ARTICLE;
                    $infoFour->declare_ad_id = $model->id;
                    $infoFour->number = $nuberEArticle;
                    
                    $infoFour->save();
                }
                $data = ['id' => $model->id, 'ad_partner' => $model->ad_partner];
                \app\models\ActionLog::trackActionLog(['result' => \app\models\ActionLog::RESULT_SUCCESS, 'object' => json_encode($data)]);
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } 
        return $this->render('update', [
            'model' => $model,
            'dataNumber' => $dataNumber,
        ]);
    }

    /**
     * Deletes an existing DeclareAd model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $m = $this->findModel($id);
        if($m->ad_type == DeclareAd::AD_TYPE_PACKAGE) {
            DeclareAdPakageInfo::deleteAll("declare_ad_id={$id}");
            \app\models\DeclareAdChild::deleteAll("declare_ad_id={$id}");
        } 
        $m->delete();
        $data = ['id' => $m->id];
        \app\models\ActionLog::trackActionLog(['result' => \app\models\ActionLog::RESULT_SUCCESS, 'object' => json_encode($data)]);
        return $this->redirect(['index']);
    }
    
    public function actionCreateChildAd($id) {
        $modelParent = $this->findModel($id);
        if($modelParent->ad_type == DeclareAd::AD_TYPE_PACKAGE) {
            $model = new \app\models\DeclareAdChild();
            if ($model->load(Yii::$app->request->post())) {
                $model->declare_ad_id = $id;
                $model->created_at = time();
                $model->updated_at = time();
                $model->created_by = Yii::$app->user->identity->username;
                
                $post = Yii::$app->request->post('DeclareAdChild');
                if(isset($post['EmployeeDeployString']) && !empty($post['EmployeeDeployString'])) {
                    $model->deploy_employee = implode(',', $post['EmployeeDeployString']);
                }
                
                if($model->save()) {
                    $data = ['id' => $model->id];
                    \app\models\ActionLog::trackActionLog(['result' => \app\models\ActionLog::RESULT_SUCCESS, 'object' => json_encode($data)]);
                    return $this->redirect(['view', 'id' => $modelParent->id]);
                }
            }
            return $this->render('create-child-ad', [
                'model' => $model,
                'modelParent' => $modelParent
            ]);
        } else {
            return $this->redirect(['view', 'id' => $modelParent->id]);
        }
    }

    public function actionDeploy($id) {
        $model = $this->findModel($id);
        if($model) {
            if($model->status != DeclareAd::STATUS_DONE) {
                $model->status = DeclareAd::STATUS_DONE;
                $model->updated_by = Yii::$app->user->identity->username;
                $model->updated_at = time();
                if($model->save(true, ['status','updated_by','updated_at'])) {
                    $data = ['id' => $model->id];
                    \app\models\ActionLog::trackActionLog(['result' => \app\models\ActionLog::RESULT_SUCCESS, 'object' => json_encode($data)]);
                    Yii::$app->session->setFlash('success', 'Cập nhật thành công');
                } else {
                    Yii::$app->session->setFlash('error', 'Cập nhật không thành công');
                }
            }
        }
        return $this->redirect(['view', 'id' => $id]);
    }
    
    public function actionPaid($id) {
        $model = $this->findModel($id);
        if($model) {
            if($model->ad_pay_status != DeclareAd::PAY_STATUS_PAID) {
                $model->ad_pay_status = DeclareAd::PAY_STATUS_PAID;
                $model->updated_by = Yii::$app->user->identity->username;
                $model->updated_at = time();
                if($model->save(true, ['ad_pay_status','updated_by','updated_at'])) {
                    $data = ['id' => $model->id];
                    \app\models\ActionLog::trackActionLog(['result' => \app\models\ActionLog::RESULT_SUCCESS, 'object' => json_encode($data)]);
                    Yii::$app->session->setFlash('success', 'Cập nhật thành công');
                } else {
                    Yii::$app->session->setFlash('error', 'Cập nhật không thành công');
                }
            }
        }
        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionStatistic() {
        return $this->redirect(['index']);
        //return $this->render('statistic');
    }
    
    public function actionAjaxGetChildList($id) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ($this->getAdChild($id,true));
    }

    /**
     * Finds the DeclareAd model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DeclareAd the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DeclareAd::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function getAdChild($id, $isArray = true) {
        $deployNumbers = \app\models\DeclareAdChild::find()->where('declare_ad_id='.$id)->all();
        if($isArray) {
            $returnValue = [];
            if($deployNumbers) {
                foreach($deployNumbers as $deploy) {
                    $x = $deploy->toArray();
                    $x['ad_from_date'] = date('d/m/Y', $deploy->ad_from_time);
                    $x['ad_to_date'] = date('d/m/Y', $deploy->ad_to_time);
                    $x['total_price_format'] = (($deploy->total_price)?number_format($deploy->total_price, 0, ',', '.'):0).'đ';
                    $x['ad_type_label'] = \app\models\DeclareAdPakageInfo::getAdTypeLabels()[$deploy->ad_type];
                    $x['deploy_employee_full_name'] = ($deploy->deploy_employee)?\app\models\Employee::getEmployeeDeployInfo(false,$deploy->deploy_employee):'';
                    $returnValue[] = $x;
                }
            }
            return $returnValue;
        } else {
            return $deployNumbers;
        }
    }
}
