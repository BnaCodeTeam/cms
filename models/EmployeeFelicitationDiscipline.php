<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "employee_felicitation_discipline".
 *
 * @property integer $id
 * @property integer $employee_id
 * @property integer $felicitation_discipline_id
 * @property integer $felicitation_discipline_type
 * @property string $felicitation_discipline_number
 * @property integer $created_at
 * @property integer $updated_at
 */
class EmployeeFelicitationDiscipline extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employee_felicitation_discipline';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['employee_id', 'felicitation_discipline_id', 'felicitation_discipline_type', 'felicitation_discipline_number', 'created_at', 'updated_at'], 'required'],
            [['employee_id', 'felicitation_discipline_id', 'felicitation_discipline_type', 'created_at', 'updated_at'], 'integer'],
            [['felicitation_discipline_number'], 'string', 'max' => 255],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employee_id' => 'Employee ID',
            'felicitation_discipline_id' => 'Felicitation Discipline ID',
            'felicitation_discipline_type' => 'Type',
            'felicitation_discipline_number' => 'Number',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
