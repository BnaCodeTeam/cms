<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DeclareAd;

/**
 * DeclareAdSearch represents the model behind the search form about `app\models\DeclareAd`.
 */
class DeclareAdSearch extends DeclareAd
{
    public $ad_partner_name;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ad_type', 'ad_from_time', 'ad_to_time', 'ad_created_by', 'ad_pay_status', 'remind_status', 'created_at', 'updated_at', 'ad_partner_id'], 'integer'],
            [['ad_partner', 'ad_note', 'ad_pay_note', 'created_by'], 'safe'],
            [['ad_price'], 'number'],
            [['ad_partner_name'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DeclareAd::find()->joinWith('adPartner');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 30,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ad_type' => $this->ad_type,
            'ad_from_time' => $this->ad_from_time,
            'ad_to_time' => $this->ad_to_time,
            'ad_price' => $this->ad_price,
            'ad_created_by' => $this->ad_created_by,
            'ad_pay_status' => $this->ad_pay_status,
            'remind_status' => $this->remind_status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'ad_partner', $this->ad_partner])
            ->andFilterWhere(['like', 'ad_note', $this->ad_note])
            ->andFilterWhere(['like', 'ad_pay_note', $this->ad_pay_note])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'ad_partner.name', $this->ad_partner_name]);

        return $dataProvider;
    }
    
    public function search2($params)
    {
        $query = DeclareAd::find()
                ->where('FIND_IN_SET('.Yii::$app->user->identity->id.', deploy_employee)')
                ->orWhere('ad_created_by='.Yii::$app->user->identity->id);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ad_type' => $this->ad_type,
            'ad_from_time' => $this->ad_from_time,
            'ad_to_time' => $this->ad_to_time,
            'ad_price' => $this->ad_price,
            'ad_created_by' => $this->ad_created_by,
            'ad_pay_status' => $this->ad_pay_status,
        ]);

        $query->andFilterWhere(['like', 'ad_partner', $this->ad_partner])
            ->andFilterWhere(['like', 'created_by', $this->created_by]);

        return $dataProvider;
    }
}
