<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DocumentarySearch */
/* @var $form yii\widgets\ActiveForm */
use app\models\Documentary;
use yii\web\View;
?>

<div class="documentary-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => ['class' => 'form-inline']
    ]); ?>

    <?= $form->field($model, 'type')->dropDownList(Documentary::getTypeLabels(),['prompt'=>'---- Chọn loại công văn ----'])->label(false) ?>

    <?php //echo $form->field($model, 'send_number') ?>

    <?php //echo $form->field($model, 'receive_number') ?>
    
    <?= $form->field($model, 'searchByNumber')->textInput(['placeholder' => $model->getAttributeLabel('searchByNumber')])->label(false) ?>

    <?php // echo $form->field($model, 'send_by_id') ?>

    <?php // echo $form->field($model, 'receive_by_id') ?>

    <?php // echo $form->field($model, 'send_by_name') ?>

    <?php // echo $form->field($model, 'receive_by_name') ?>

    <?php // echo $form->field($model, 'send_time') ?>

    <?php // echo $form->field($model, 'receive_time') ?>

    <?php // echo $form->field($model, 'sign') ?>

    <?php // echo $form->field($model, 'note') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group" style="margin-bottom: 10px;">
        <?= Html::submitButton('Tìm kiếm', ['class' => 'btn btn-primary']) ?>
        <?php //echo Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
