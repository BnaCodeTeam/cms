<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DeclareAd */

$this->title = 'Triển khai cho hợp đồng gói #'.$modelParent->id;
$this->params['breadcrumbs'][] = ['label' => 'Quản lý quảng cáo', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="declare-ad-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <i><b><a href=<?php echo Yii::$app->urlManager->createUrl(['/declare-ad/view','id' => $modelParent->id])?>><h3>(<?php echo $modelParent->ad_partner;?>)</h3></a></b></i>

    <?= $this->render('//declare-ad-child/_form', [
        'model' => $model
    ]) ?>

</div>
