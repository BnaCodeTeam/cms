<?php

use yii\db\Migration;

class m161208_093425_table_declare_ad_pakage_info extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('declare_ad_pakage_info', [
            'id' => $this->primaryKey(),
            'declare_ad_id' => $this->integer(),
            'ad_type' => $this->integer(),
            'number' => $this->integer()
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('declare_ad_pakage_info');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
