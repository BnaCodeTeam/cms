<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\web\JsExpression;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\MediaAlbum */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Media', 'url' => ['/media']];
$this->params['breadcrumbs'][] = ['label' => 'Albums', 'url' => ['/media/album']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="media-album-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Sửa', ['update-ajax', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Xóa', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php $cate = '';?>
    <?php if($model->category_id):?>
    <?php $cate = '<p><b><span class="label label-info">Chuyên mục:</span> <span class="label label-warning">'.$model->mediaCategory->name.'</span></b></p>';?>
    <?php endif;?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
            'description',
            [
                'label' => 'Chi tiết',
                'format' => 'raw',
                'value' => 
                '<p><b> <span class="label label-info">Được tạo bởi:</span> <span class="label label-danger">'.$model->created_by.'</span></b> <i style="color:#c00;">('.date('d-m-Y h:i:s',$model->created_at).')</i></p>'
                .'<p><b><span class="label label-info">Cập nhật lần cuối:</span> <span class="label label-danger">'.$model->updated_by.'</span></b> <i style="color:#c00;">('.date('d-m-Y h:i:s',$model->updated_at).')</i></p>'
                .$cate
                .'<p><b> <span class="label label-info">Tác giả:</span> <span class="label label-primary">'. app\models\Employee::getAliasAuthorById($model->author_id).'</span></b></p>'
            ],
        ],
    ]) ?>
    <?php if(!empty($model->media)):?>
    <div class="row">
        <?php foreach($model->media as $media):?>
        <?php echo $this->render('//media/_img', ['model' => $media]); ?>
        <?php endforeach;?>
    </div>
    <?php endif;?>
</div>

<?php $this->registerJsFile('/js/media-item.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>