<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FelicitationDisciplineSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Khen thưởng kỷ luật';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="felicitation-discipline-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Tạo quyết định', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'employee_id',
            //'type',
            'title',
            'number',
            ['attribute' => 'Loại quyết định', 'format' => 'raw', 'value' => function($model) {
                $class = '';
                if($model->type == app\models\FelicitationDiscipline::TYPE_FELICITATION) {
                    $class = 'success';
                } elseif($model->type == app\models\FelicitationDiscipline::TYPE_DISCIPLINE) {
                    $class = 'danger';
                }
                return (isset(app\models\FelicitationDiscipline::getTypeLabels()[$model->type]))? 
                '<span class="label label-'.$class.'">'. app\models\FelicitationDiscipline::getTypeLabels()[$model->type].'</span>' : '';                
            }],
            ['attribute' => 'Trạng thái', 'format' => 'raw', 'value' => function($model) {
                $class = '';
                if($model->status == app\models\FelicitationDiscipline::STATUS_ACTIVE) {
                    $class = 'info';
                } elseif($model->status == app\models\FelicitationDiscipline::STATUS_INACTIVE) {
                    $class = 'warning';
                }
                return (isset(app\models\FelicitationDiscipline::getStatusLabels()[$model->status]))? 
                '<span class="label label-'.$class.'">'. app\models\FelicitationDiscipline::getStatusLabels()[$model->status].'</span>' : '';                
            }],
            // 'sign_time:datetime',
            // 'title',
            // 'content:ntext',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
        'options' => ['class' => 'grid-view table-responsive']
    ]); ?>
</div>
