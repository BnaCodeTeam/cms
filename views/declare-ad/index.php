<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DeclareAdSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ($actionGet=='index2')?'Quảng cáo của bạn':'Quản lý quảng cáo';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="declare-ad-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel, 'actionGet' => $actionGet]); ?>

    <p>
        <?php echo ($actionGet=='index2')?'':Html::a('Tạo hợp đồng quảng cáo', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'ad_partner',
            [
                'attribute' => 'Đối tác', 'format' => 'raw', 'value' => function($model) {
                    return $model->adPartner->name;
                }
            ],
            ['attribute' => 'Loại hình', 'format' => 'raw', 'value' => function($model) {
                $class = '';
                switch ($model->ad_type) {
                    case app\models\DeclareAd::AD_TYPE_TIN_BAI:
                        $class = 'info';
                        break;
                    
                    case app\models\DeclareAd::AD_TYPE_BANNER:
                        $class = 'primary';
                        break;
                        
                    case app\models\DeclareAd::AD_TYPE_CLIP:
                        $class = 'default';
                        break;
                    default:
                        $class = 'danger';
                }
                
                return (isset(app\models\DeclareAd::getAdTypeLabels()[$model->ad_type]))? 
                '<span class="label label-'.$class.'">'. app\models\DeclareAd::getAdTypeLabels()[$model->ad_type]. '</span>' : '';                
            }],
            ['attribute' => 'Thanh toán', 'format' => 'raw', 'value' => function($model) {
                $class = '';
                switch ($model->ad_pay_status) {
                    case app\models\DeclareAd::PAY_STATUS_UNPAID:
                        $class = 'danger';
                        break;
                    
                    case app\models\DeclareAd::PAY_STATUS_WAIT:
                        $class = 'warning';
                        break;
                        
                    case app\models\DeclareAd::PAY_STATUS_PAID:
                        $class = 'success';
                        break;
                }
                
                $classStatus = '';
                switch ($model->status) {
                    case \app\models\DeclareAd::STATUS_WAIT:
                        $classStatus = 'label-default';
                        break;
                    
                    case \app\models\DeclareAd::STATUS_DONE:
                        $classStatus = 'label-info';
                        break;
                    
                    case \app\models\DeclareAd::STATUS_THANH_LY:
                        $classStatus = 'label-primary';
                        break;
                }
                
                return (isset(app\models\DeclareAd::getPaidStatusLabels()[$model->ad_pay_status]))? 
                '<span class="label label-'.$class.'">'. app\models\DeclareAd::getPaidStatusLabels()[$model->ad_pay_status]. '</span>' . 
                        ((isset(\app\models\DeclareAd::getStatusLabels()[$model->status]) && $model->ad_type != \app\models\DeclareAd::AD_TYPE_PACKAGE)?'<br/><span class="label '.$classStatus.'">'.\app\models\DeclareAd::getStatusLabels()[$model->status].'</span>':'') : '';                
            }],
            ['attribute' => 'Bắt đầu', 'format' => 'raw', 'value' => function($model) {
                return date('d-m-Y', $model->ad_from_time);
            }],  
            ['attribute' => 'Kết thúc', 'format' => 'raw', 'value' => function($model) {
                if($model->ad_to_time != null) {
                    $valx = date('d-m-Y', $model->ad_to_time);
                    $ex = $model->isExpired();
                    if($ex != false && ($model->ad_pay_status != \app\models\DeclareAd::PAY_STATUS_PAID || !$model->isDeployed())) {
                        $valx = $valx . '<p><span class="label label-danger"> Đã quá hạn ' .$ex. ' ngày</span></p>';
                    }
                    return $valx;
                }
                return '<p style="color: #c00; font-style: italic;">Chưa khai báo</p>';
            }], 

            ['class' => 'yii\grid\ActionColumn']+(($actionGet=='index2')?[
                'template' =>'{view}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', '/declare-ad/view2?id='.$model->id, ['title' => Yii::t('app', 'Xem')]);
                    }
                ]]:[]),
        ],
        'options' => ['class' => 'grid-view table-responsive']
    ]); ?>
</div>
