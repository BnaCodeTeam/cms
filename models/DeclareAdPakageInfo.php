<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "declare_ad_pakage_info".
 *
 * @property integer $id
 * @property integer $declare_ad_id
 * @property integer $ad_type
 * @property integer $number
 */
class DeclareAdPakageInfo extends \yii\db\ActiveRecord
{
    const AD_TYPE_VIDEO = 1;
    const AD_TYPE_ARTICLE = 2;
    const AD_TYPE_BANNER = 3;
    const AD_TYPE_E_ARTICLE = 4;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'declare_ad_pakage_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['declare_ad_id', 'ad_type', 'number'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'declare_ad_id' => 'Declare Ad ID',
            'ad_type' => 'Ad Type',
            'number' => 'Number',
        ];
    }
    
    public static function getAdTypeLabels() {
        return [
            self::AD_TYPE_VIDEO => 'Clip',
            self::AD_TYPE_ARTICLE => 'Bài viết (Báo in)',
            self::AD_TYPE_BANNER => 'Banner',
            self::AD_TYPE_E_ARTICLE => 'Bài viết (Báo điện tử)',
        ];
    }
}
