<?php

namespace app\models;

use Yii;
use \yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "media_category".
 *
 * @property int $id
 * @property string $name
 * @property int $parent_id
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property string $created_by
 * @property string $updated_by
 * @property string $google_drive_id
 * @property string $google_drive_link
 */
class MediaCategory extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 10;
    const STATUS_DELETE = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'media_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'created_by', 'updated_by','google_drive_link','google_drive_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'parent_id' => 'Parent ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'google_drive_link' => 'Drive link',
            'google_drive_id' => 'Drive id'
        ];
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className()
            ],
        ];
    }
    
    public function getMedia() {
        return $this->hasMany(Media::className(), ['category_id' => 'id'])->where('media.status=:status',[':status' => Media::STATUS_ACTIVE]);
    }
    
    public function getMediaAlbum() {
        return $this->hasMany(MediaAlbum::className(), ['category_id' => 'id'])->where('media_album.status=:status',[':status' => MediaAlbum::STATUS_ACTIVE]);
    }
    
    public static function getListCategory() {
        $models = MediaCategory::find()->select(['id','name'])->where(['status' => MediaCategory::STATUS_ACTIVE])->all();
        $return = [];
        foreach($models as $m) {
            $return[$m->id] = $m->name;
        }
        return $return;
    }
}
