<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = "Nhân viên: ".$model->full_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Nhân viên'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Cập nhật'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Xóa'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Mã nhân viên',
                'format' => 'raw',
                'value' => '<b>'.$model->id.'</b>'
            ],
            [
                'label' => 'Loại nhân viên',
                'format' => 'raw',
                'value' => '<b>'.  (($model->user_type)?app\models\Employee::getUserTypeLabels()[$model->user_type]:'------').'</b>'
            ],
            [
                'label' => 'Họ tên',
                'format' => 'raw',
                'value' => '<b>'.$model->full_name.'</b>'
            ],
            //'user_type',
            [
                'label' => 'Ảnh đại diện',
                'format' => 'raw',
                'value' => '<img src="/'.(($model->image)?$model->image:'uploads/images/users/no-user-image.png').'" height="64" width="64">'
            ],
            [
                'label' => 'Tên đăng nhập',
                'format' => 'raw',
                'value' => '<b>'.$model->username.'</b>'
            ],
            [
                'label' => 'Ngày sinh',
                'format' => 'raw',
                'value' => '<b>'.$model->birthString.'</b>'
            ],
            [
                'label' => 'Giới tính',
                'format' => 'raw',
                'value' => '<b>'.(isset(\app\models\Employee::getSexLabels()[$model->sex])?\app\models\Employee::getSexLabels()[$model->sex]:'------').'</b>'
            ],
            [
                'label' => 'CMND',
                'format' => 'raw',
                'value' => '<span class="label label-danger" style="margin-right:5px;">Số</span><b>'.
                $model->id_number . '</b><span class="label label-danger" style="margin-right:5px; margin-left:10px;">Ngày cấp</span><b>'. 
                $model->idNumberTimeString . '</b><span class="label label-danger" style="margin-right:5px; margin-left:10px;">Nơi cấp</span><b>' . $model->id_number_place . '</b>',
            ],
            'email:email',
            [
                'label' => 'Điện thoại',
                'format' => 'raw',
                'value' => '<b>'.$model->phone.'</b>'
            ],
            'birthplace',
            'resident',
            'native_land',
            'ethnic_group',
            'religion',
            'employee_coefficient',
            'height',
            'weight',
            'health',
            'blood_group',
            'joinedDateString',
            'address',    
            [
                'label' => 'Khen thưởng',
                'format' => 'raw',
                'value' => (isset($model->getFelicitationDiscipline()[app\models\FelicitationDiscipline::TYPE_FELICITATION]))?$model->getFelicitationDiscipline()[app\models\FelicitationDiscipline::TYPE_FELICITATION]:'Không'
            ],
            [
                'label' => 'Kỷ luật',
                'format' => 'raw',
                'value' => (isset($model->getFelicitationDiscipline()[app\models\FelicitationDiscipline::TYPE_DISCIPLINE]))?$model->getFelicitationDiscipline()[app\models\FelicitationDiscipline::TYPE_DISCIPLINE]:'Không'
            ],
            [
                'label' => 'Trạng thái',
                'format' => 'raw',
                'value' => '<span class="label label-primary">'.\app\models\Employee::getStatusLabels()[$model->status].'</span>'
            ]
        ],
    ]) ?>

</div>
