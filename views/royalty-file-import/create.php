<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RoyaltyFileImport */

$this->title = 'Upload file nhuận bút';
$this->params['breadcrumbs'][] = ['label' => 'File nhuận bút', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="royalty-file-import-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
