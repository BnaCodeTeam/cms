<?php
use yii\grid\GridView;

$this->title = 'Quản lý phân quyền';

//$this->params['breadcrumbs'][] = ['label' => 'Website', 'url' => ['/site/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-management-index">
    <?php if(Yii::$app->session->hasFlash('success')):?>
    <div class="alert alert-success" role="alert">
        <?= Yii::$app->session->getFlash('success'); ?>
    </div>
    <?php endif;?>
    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'header' => 'Name',
                        'content' => function($data){
                            return $data->name;
                        },
                        'contentOptions' => [
                            'style' => 'width: 45%'
                        ]
                    ],
                    'attribute' => 'description',
                ],
                'pager' => [
                    'firstPageLabel' => 'Trang đầu',
                    'lastPageLabel' => 'Trang cuối',
                    'options' => [
                        'class' => 'pagination',
                        'style' => 'margin-bottom: 0;'
                    ]
                ]
            ]); ?>
        </div>
    </div>
</div>
<?php
$js = <<< EOD
    $(function(){
        $('.update-action').click(function(){
            var _this = $(this);
            _this.html('Đang cập nhật...').addClass('disabled');
            $.post(_this.attr('href'), function(result){
                location.reload();
            });
            return false;
        });
    });
EOD;
$this->registerJs($js);
?>