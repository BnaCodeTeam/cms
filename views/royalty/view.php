<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Royalty */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Royalties', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="royalty-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'full_name',
            'royalty_month',
            'royalty_week',
            'address:ntext',
            'article_count',
            'post_count',
            'photo_count',
            'rate',
            'royalty_amount',
            'tax_amount',
            'royalty_total_amount',
            'paid_type',
            'email:email',
            'created_by',
            'updated_by',
            'created_at',
            'is_sended',
            'sended_at',
        ],
    ]) ?>

</div>
