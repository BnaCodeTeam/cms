<?php

return [
    'adminEmail' => 'admin@example.com',
    
    'token_dir' => __DIR__ . '/token.json',
    'credentials_dir' =>  __DIR__ . '/credentials.json',
    'domain' => require(__DIR__ . '/local.php'),
];
