<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FelicitationDiscipline */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Khen thưởng kỷ luật', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="felicitation-discipline-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Cập nhật', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= ($model->status == app\models\FelicitationDiscipline::STATUS_ACTIVE)?Html::a('Xóa', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]):''; ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            //'employee_id',
            //'type',
            [
                'label' => 'Loại quyết định',
                'format' => 'raw',
                'value' => app\models\FelicitationDiscipline::getTypeLabels()[$model->type]
            ],
            'number',
            //'status',
            [
                'label' => 'Trạng thái',
                'format' => 'raw',
                'value' => app\models\FelicitationDiscipline::getStatusLabels()[$model->status]
            ],
            //'sign_time:datetime',
            'SignDateString',
            'title',
            'content:ntext',
            [
                'label' => 'Người thi hành',
                'format' => 'raw',
                'value' => $employeeAssign
            ],
            [
                'label' => 'Mẫu quyết định',
                'format' => 'raw',
                'value' => $model->viewTemplate()
            ],
            //'created_at',
            //'updated_at',
        ],
    ]) ?>

</div>
