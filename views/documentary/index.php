<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DocumentarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Quản lý công văn';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="documentary-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Thêm công văn', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            ['attribute' => 'Thông tin', 'headerOptions' => ['style' => 'width:30%'], 'format' => 'raw', 'value' => function($model) {
                $receiveDateStr = '';
                $receiveNumberStr = '';
                $sendDateStr = '';
                $sendNumberStr = '';
                $receiveNameStr = '';
                $sendNameStr = '';
                $class = '';
                switch ($model->type) {
                    case app\models\Documentary::TYPE_SEND:
                        $sendDateStr = '<tr><th scope="row"><span class="label label-success">Ngày gửi</span></th><td><span class="label label-info">'.$model->sendDateString.'</span></td></tr>';
                        $sendNumberStr = '<tr><th scope="row"><span class="label label-success">Số hiệu</span></th><td><span class="label label-info">'.$model->send_number.'</span></td></tr>';
                        $receiveNameStr = '<tr><th scope="row"><span class="label label-success">Nơi nhận</span></th><td><span style="color:#5bc0de;"><b>'.$model->receive_by_name.'</b></span></td></tr>';
                        $class = 'success';
                        break;
                    
                    case app\models\Documentary::TYPE_RECEIVE:
                        $receiveDateStr = '<tr><th scope="row"><span class="label label-primary">Ngày đến</span></th><td><span class="label label-info">'.$model->receiveDateString.'</span></td></tr>';
                        $receiveNumberStr = '<tr><th scope="row"><span class="label label-primary">Số đến</span></th><td><span class="label label-info">'.$model->receive_number.'</span></td></tr>';
                        $sendNameStr = '<tr><th scope="row"><span class="label label-primary">Nơi gửi</span></th><td><span style="color:#5bc0de;"><b>'.$model->send_by_name.'</b></span></td></tr>';
                        $class = 'primary';
                        break;
                    
                    default:
                        break;
                }
               
                $docTypeStr = (isset(app\models\Documentary::getTypeLabels()[$model->type]))? 
                        '<tr><th scope="row" colspan="2" style="text-align:center;"><span class="label label-'.$class.'">'.app\models\Documentary::getTypeLabels()[$model->type].'</span></th></tr>' : '';
                return '<table class="table table-bordered"><tbody>'.$docTypeStr.$sendDateStr.$receiveDateStr.$receiveNumberStr.$sendNumberStr.$receiveNameStr.$sendNameStr.'</tbody></table>';             
            }],
            //'type',
            
            //'send_number',
            //'receive_number',
            // 'send_by_id',
            // 'receive_by_id',
            // 'send_by_name',
            // 'receive_by_name',
            // 'send_time:datetime',
            // 'receive_time:datetime',
            // 'sign',
            'note:ntext',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
