<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "documentary".
 *
 * @property integer $id
 * @property string $name
 * @property integer $type
 * @property string $send_number
 * @property string $receive_number
 * @property integer $send_by_id
 * @property integer $receive_by_id
 * @property string $send_by_name
 * @property string $receive_by_name
 * @property integer $send_time
 * @property integer $receive_time
 * @property string $sign
 * @property string $note
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $created_by
 * @property string $updated_by
 * @property string $attachment 
 */
class Documentary extends \yii\db\ActiveRecord
{
    const TYPE_SEND = 1;
    const TYPE_RECEIVE = 2;
    
    const SEND_NAME_DEFAULT = 'BÁO NGHỆ AN';
    const RECEIVE_NAME_DEFAULT = 'BÁO NGHỆ AN';
    
    public $searchByNumber;
    
    public function getSendDateString(){
        if($this->send_time) {
            return date('d-m-Y',$this->send_time);
        }
        return '';
    }
    public function setSendDateString($sendDateString) {
        if($sendDateString && trim($sendDateString) != '') {
            $this->send_time = strtotime($sendDateString);
        }
    }
    
    public function getReceiveDateString(){
        if($this->receive_time) {
            return date('d-m-Y',$this->receive_time);
        }
        return '';
    }
    public function setReceiveDateString($receiveDateString) {
        if($receiveDateString && trim($receiveDateString) != '') {
            $this->receive_time = strtotime($receiveDateString);
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'documentary';
    }
    
    /**
     * @var UploadedFile[]
     */
    public $documentFiles;

    public function upload()
    {
        if ($this->validate()) {
            $filePathArrays = [];
            foreach ($this->documentFiles as $file) {
                $path = 'uploads/attack_file/documentary/' . time() . '_' .$file->baseName . '.' . $file->extension;
                $file->saveAs($path);
                $filePathArrays[] = '/'.$path;
            }
            if(!empty($filePathArrays)) {
                $this->attachment = implode(",", $filePathArrays);
            }
            
            $this->documentFiles = null;
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'send_by_id', 'receive_by_id', 'send_time', 'receive_time', 'created_at', 'updated_at'], 'integer'],
            [['name', 'send_by_name', 'receive_by_name', 'note','attachment', 'send_number', 'receive_number', 'sign'], 'string'],
            [['created_by', 'updated_by'], 'string', 'max' => 255],
            [['documentFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'maxFiles' => 100],
            [['name', 'type'], 'required'],
            [['receiveDateString','sendDateString', 'searchByNumber'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Tên công văn',
            'type' => 'Loại công văn',
            'send_number' => 'Số hiệu đi',
            'receive_number' => 'Số hiệu đến',
            'send_by_id' => 'Send By ID',
            'receive_by_id' => 'Receive By ID',
            'send_by_name' => 'Đơn vị gửi',
            'receive_by_name' => 'Đơn vị nhận',
            'send_time' => 'Ngày gửi',
            'receive_time' => 'Ngày nhận',
            'sign' => 'Ký nhận',
            'note' => 'Ghi chú',
            'created_at' => 'Tạo lúc',
            'updated_at' => 'Cập nhật lúc',
            'created_by' => 'Tạo bởi',
            'updated_by' => 'Cập nhật bởi',
            'documentFiles' => 'Tải lên',
            'attachment' => 'Bản sao lưu',
            'receiveDateString' => 'Ngày nhận',
            'sendDateString' => 'Ngày gửi',
            'searchByNumber' => 'Số hiệu'
        ];
    }
    
    public function getTitle() {
        $typeStr = '';
        $number = '';
        $sendNo = '';//for receive document
        switch ($this->type) {
            case self::TYPE_SEND:
                $typeStr = ' gửi đi ';
                $number = 'số hiệu ' . $this->send_number;
                break;
            case self::TYPE_RECEIVE:
                $typeStr = ' gửi đến ';
                $number = 'số hiệu ' . $this->receive_number;
                break;
            default :
                break;
        }
        return 'Công văn'.$typeStr.$number;
    }
    
    public static function getTypeLabels() {
        return [
            self::TYPE_SEND => 'Công văn đi',
            self::TYPE_RECEIVE => 'Công văn đến'
        ];
    }
    
    
}
