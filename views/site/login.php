<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

//$this->title = 'Đăng nhập';
//$this->params['breadcrumbs'][] = $this->title;
use yii\authclient\widgets\AuthChoice;
?>
<div class="site-login">
    <h1><?php //echo Html::encode($this->title) ?></h1>
    <div class="col-md-4"></div>
    <div class="col-md-4">
    <div class="panel panel-default">
        <div class="panel-heading">Đăng nhập</div>
        <div class="panel-body">
            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'options' => ['class' => ''],
                
            ]); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'rememberMe')->checkbox() ?>

                <div class="form-group">
                    <div class="">
                        <?= Html::submitButton('Đăng nhập', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                    </div>
                </div>

            <?php ActiveForm::end(); ?>
            
        </div>
        <div class="panel-footer">
            <p class="bg-danger" style="text-align: center; line-height: 40px;">Hoặc đăng nhập bằng tài khoản Google</p>
            <?php $authAuthChoice = AuthChoice::begin([
                'baseAuthUrl' => ['site/auth'],
                'popupMode' => false
            ]); ?>
            
            <?php foreach ($authAuthChoice->getClients() as $client): ?>
                <?= $authAuthChoice->clientLink($client) ?>
            <?php endforeach; ?>
            
            <?php AuthChoice::end(); ?>
        </div>
    </div>
    </div>
    <div class="col-md-4"></div>
</div>
