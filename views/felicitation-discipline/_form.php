<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

//use kartik\widgets\Select2;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\web\View;
use app\models\FelicitationDiscipline;
use yii\jui\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\FelicitationDiscipline */
/* @var $form yii\widgets\ActiveForm */
?>

<?php 
$formatJs = <<< 'JS'
function IsNumeric(input){
    var RE = /^-{0,1}\d*\.{0,1}\d+$/;
    return (RE.test(input));
};
var formatEmployee = function(employee){
        if(employee.id != 'undefined' && IsNumeric(employee.id)) {
            var imx = 'uploads/images/users/no-user-image.png';
            if(employee.image != 'undefined' && employee.image != null) {
                imx = employee.image;
            }
            var ma = '<div class="row">' + 
                    '<div class="col-sm-5">' +
                        '<img data-holder-rendered="true" style="width: 64px; height: 64px; data-src="holder.js/64x64" src="/' + imx + '" class="img-rounded" />' + 
                        '<b style="margin-left:5px">' + employee.text + '</b>' + 
                    '</div>' + 
                '</div>';
            return '<div style="overflow:hidden;">' + ma + '</div>';
        }
    };
    
var formatSelectionEmployee = function(employee) {
    return employee.full_name || employee.text;
};

function setRemoveTag(eId) {
    $('#employee-tag-' + eId).remove();
    $("#assign-id-" + eId).remove();
}

var eventSelectEmployee = function(item) {
    if(item.params.data.selected) {
        $('#tags-name').append('<span onclick="setRemoveTag(' + item.params.data.id + ')" class="label label-info tag-name-label" style="margin-right:3px; cursor:pointer" data-id="' + item.params.data.id + '" id="employee-tag-' + item.params.data.id + '" >' + item.params.data.text + '</span>');
        $('#tags-name').append('<input type="hidden" value="' + item.params.data.id + '" id="assign-id-' + item.params.data.id + '" name="FelicitationDiscipline[EmployeeAssignId][]">');
    }
};
JS;

$this->registerJs($formatJs, View::POS_HEAD);
?>

<div class="felicitation-discipline-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="table-responsive"> 
        <table class="table table-bordered table-striped">
            <tbody>
                <tr> 
                    <td>
                        <div class="form-group">
                            <?= $form->field($model, 'type')->dropDownList(FelicitationDiscipline::getTypeLabels(), ['prompt' => '------ Chọn loại -----']) ?>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="form-group">
                            <?php $url = Yii::$app->urlManager->createUrl(['employee/ajax-get-employee-list']);?>
                            <?php echo $form->field($model, 'EmployeeSearch')->widget(Select2::classname(), [
                                'initValueText' => '',
                                'options' => ['placeholder' => 'Tìm kiếm nhân viên...'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 2,
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return 'Đang tìm dữ liệu...'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => $url,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                                        'delay' => 250,
                                    ],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult' => new JsExpression('formatEmployee'),
                                    'templateSelection' => new JsExpression('formatSelectionEmployee'),
                                    'tags' => true,
                                ],
                                'pluginEvents' => [
                                    'select2:select' => new JsExpression('eventSelectEmployee'),
                                ]

                            ]);?>
                        </div>
                    </td>
                    <td>
                        <div class="form-group" id="tags-name">
                            <?php if(isset($assignData) && !empty($assignData)):?>
                            <?php foreach($assignData as $emp):?>
                                <?php 
                                echo '<span onclick="setRemoveTag('.$emp['employee_id'].')" '
                                        . 'class="label label-info tag-name-label" '
                                        . 'style="margin-right:3px; cursor:pointer" '
                                        . 'data-id="'.$emp['employee_id'].'" '
                                        . 'id="employee-tag-'.$emp['employee_id'].'" >'.$emp['full_name'].''
                                        . '</span>';
                                
                                echo '<input type="hidden" value="'.$emp['employee_id'].'" id="assign-id-'.$emp['employee_id'].'" name="FelicitationDiscipline[EmployeeAssignId][]">';
                                
                                ?>
                            <?php endforeach;?>
                            <?php endif;?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="form-group">
                            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <?= $form->field($model,'SignDateString')->widget(DatePicker::className(),['dateFormat' => 'dd-MM-yyyy','options' => ['class' => 'form-control'],'clientOptions' => ['defaultDate' => '01-01-2016']]) ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="form-group">
                            <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                        <div class="form-group">
                            <?= $form->field($model, 'templateFile')->fileInput() ?>
                        </div>
                    </td>
                    <td colspan="1">
                        <?php echo $model->felicitation_template?'<a type="button" class="btn btn-primary" href="/'.$model->felicitation_template.'"><span class="glyphicon glyphicon-cloud-download" aria-hidden="true"></span> Tải hợp quyết định</a>':'<label class="label label-default">Chưa có file quyết định</label>' ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? 'Tạo mới' : 'Cập nhật', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <?php ActiveForm::end(); ?>

</div>
