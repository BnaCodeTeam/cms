<?php

use yii\db\Migration;

class m160829_081241_add_image_to_employee extends Migration
{
    public function up()
    {
        $this->addColumn('employee', 'image', yii\db\Schema::TYPE_STRING);
    }

    public function down()
    {
        return $this->dropColumn('employee', 'image');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
