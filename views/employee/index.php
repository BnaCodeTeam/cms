<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Nhân viên');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <div class="btn-group"> 
            <button type="button" class="btn btn-success">Thêm mới</button> 
            <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                <span class="caret"></span> 
                <span class="sr-only">Thêm mới</span> 
            </button> 
            <ul class="dropdown-menu"> 
                <li><a href="<?php echo Yii::$app->urlManager->createUrl(['/employee/create'])?>">Nhân viên</a></li> 
                <li><a href="<?php echo Yii::$app->urlManager->createUrl(['/employee/create','user_type' => app\models\Employee::TYPE_CTV])?>">Cộng tác viên</a></li> 
            </ul> 
        </div>
    </p>
    <?php if(Yii::$app->session->hasFlash('success')):?>
    <div class="alert alert-success" role="alert">
        <?= Yii::$app->session->getFlash('success'); ?>
    </div>
    <?php endif;?>
    
    <!--<p>
        <?php //echo Html::a(Yii::t('app', 'Thêm nhân viên'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'full_name',
            'username',           
            'email:email',
            'phone',           
            ['attribute' => 'Trạng thái', 'format' => 'raw', 'value' => function($model) {
                $class = '';
                if($model->status == app\models\Employee::STATUS_ACTIVE) {
                    $class = 'success';
                } elseif($model->status == app\models\Employee::STATUS_DELETED) {
                    $class = 'warning';
                } else {
                    $class = 'danger';
                }
                return (isset(app\models\Employee::getStatusLabels()[$model->status]))? 
                '<span class="label label-'.$class.'">'. app\models\Employee::getStatusLabels()[$model->status].'</span>' : '';                
            }],
            ['attribute' => 'Phân quyền', 'format' => 'raw', 'value' => function($model) {
                $url = Yii::$app->urlManager->createUrl(['auth-management/assign', 'id'=>$model->id]);
                return '<a href="'.$url.'" style="text-decoration:none"><span class="glyphicon glyphicon-queen" aria-hidden="true"></span></a>';
            }],
            
            ['class' => 'yii\grid\ActionColumn', 'template' => '{view} {update} {delete}', 'contentOptions' => ['style' => 'width:80px;']],
        ],
        'options' => ['class' => 'grid-view table-responsive']
    ]); ?>

</div>
