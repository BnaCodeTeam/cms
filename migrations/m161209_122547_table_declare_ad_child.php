<?php

use yii\db\Migration;

class m161209_122547_table_declare_ad_child extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('declare_ad_child', [
            'id' => $this->primaryKey(),
            'declare_ad_id' => $this->integer(),
            'ad_type' => $this->integer(),
            'ad_from_time' => $this->integer(),
            'ad_to_time' => $this->integer(),
            'ad_note' => $this->text(),
            'created_by' => $this->string(),
            'updated_by' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('declare_ad_child');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
