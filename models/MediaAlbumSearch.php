<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MediaAlbum;

/**
 * MediaAlbumSearch represents the model behind the search form of `app\models\MediaAlbum`.
 */
class MediaAlbumSearch extends MediaAlbum
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at', 'category_id', 'author_id'], 'integer'],
            [['name', 'description', 'created_by', 'updated_by'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MediaAlbum::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if($this->category_id && $this->category_id > 0) {
            $query->andFilterWhere([
                'category_id' => $this->category_id,
            ]);
        }
        
        if($this->author_id && $this->author_id > 0) {
            $query->andFilterWhere([
                'author_id' => $this->author_id,
            ]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            //'id' => $this->id,
            'status' => MediaAlbum::STATUS_ACTIVE,
            //'created_at' => $this->created_at,
            //'updated_at' => $this->updated_at,
            //'category_id' => $this->category_id
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description]);
            //->andFilterWhere(['like', 'created_by', $this->created_by])
            //->andFilterWhere(['like', 'updated_by', $this->updated_by]);

        return $dataProvider;
    }
}
