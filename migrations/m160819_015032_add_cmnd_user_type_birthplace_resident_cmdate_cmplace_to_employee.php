<?php

use yii\db\Migration;
use \yii\db\Schema;

class m160819_015032_add_cmnd_user_type_birthplace_resident_cmdate_cmplace_to_employee extends Migration
{
    public function up()
    {
        $this->addColumn("employee", "id_number", Schema::TYPE_STRING);
        $this->addColumn("employee", "id_number_time", Schema::TYPE_INTEGER);
        $this->addColumn("employee", "id_number_place", Schema::TYPE_STRING);
        $this->addColumn("employee", "user_type", Schema::TYPE_INTEGER);
        $this->addColumn("employee", "birthplace", Schema::TYPE_STRING);
        $this->addColumn("employee", "resident", Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn("employee", "id_number");
        $this->dropColumn("employee", "id_number_time");
        $this->dropColumn("employee", "id_number_place");
        $this->dropColumn("employee", "user_type");
        $this->dropColumn("employee", "birthplace");
        $this->dropColumn("employee", "resident");
        
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
