<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Royalty;

/**
 * RoyaltySearch represents the model behind the search form about `app\models\Royalty`.
 */
class RoyaltySearch extends Royalty
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'article_count', 'post_count', 'photo_count', 'created_at', 'is_sended', 'sended_at'], 'integer'],
            [['full_name', 'royalty_month', 'royalty_week', 'address', 'paid_type', 'email', 'created_by', 'updated_by'], 'safe'],
            [['rate', 'royalty_amount', 'tax_amount', 'royalty_total_amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Royalty::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'article_count' => $this->article_count,
            'post_count' => $this->post_count,
            'photo_count' => $this->photo_count,
            'rate' => $this->rate,
            'royalty_amount' => $this->royalty_amount,
            'tax_amount' => $this->tax_amount,
            'royalty_total_amount' => $this->royalty_total_amount,
            'created_at' => $this->created_at,
            'is_sended' => $this->is_sended,
            'sended_at' => $this->sended_at,
        ]);

        $query->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'royalty_month', $this->royalty_month])
            ->andFilterWhere(['like', 'royalty_week', $this->royalty_week])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'paid_type', $this->paid_type])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by]);

        return $dataProvider;
    }
}
