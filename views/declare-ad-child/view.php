<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DeclareAdChild */


$backUrl = Yii::$app->urlManager->createUrl(['declare-ad/view', 'id' => $model->declare_ad_id]);
$this->title = ((isset(app\models\DeclareAdPakageInfo::getAdTypeLabels()[$model->ad_type]))? 
                app\models\DeclareAdPakageInfo::getAdTypeLabels()[$model->ad_type] : '') . ' của ' . $declare->ad_partner . ' #' . $declare->id;
$this->params['breadcrumbs'][] = ['label' => 'Hợp đồng quảng cáo #'.$model->declare_ad_id, 'url' => [$backUrl]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="declare-ad-child-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Sửa', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Xóa', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            //'declare_ad_id',
            [
                'label' => 'Thuộc hợp đồng',
                'format' => 'raw',
                'value' => '<a href="'.$backUrl.'">' . $declare->ad_partner . ' #' . $declare->id . '</a>',
            ],
            //'ad_type',
            [
                'label' => 'Loại hình',
                'format' => 'raw',
                'value' => (isset(app\models\DeclareAdPakageInfo::getAdTypeLabels()[$model->ad_type]))? 
                app\models\DeclareAdPakageInfo::getAdTypeLabels()[$model->ad_type] : ''
            ],
            'title',
            [
                'label' => 'Ngày bắt đầu đăng',
                'format' => 'raw',
                'value' => date('d-m-Y', $model->ad_from_time)
            ],
            //'ad_to_time:datetime',
            [
                'label' => 'Ngày kết thúc đăng',
                'format' => 'raw',
                'value' => date('d-m-Y', $model->ad_to_time)
            ],
            'ad_note:ntext',
            [
                'label' => 'Tạo bởi',
                'format' => 'raw',
                'value' => $model->created_by.' <i style="color:brown;">('.date('d-m-Y H:i:s', $model->created_at).')</i>'
            ],
            [
                'label' => 'Cập nhật bởi',
                'format' => 'raw',
                'value' => $model->updated_by.' <i style="color:brown;">('. date('d-m-Y H:i:s', $model->updated_at).')</i>'
            ]
        ],
    ]) ?>

</div>
