<?php

namespace app\models;

use Yii;
use app\models\Employee;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "felicitation_discipline".
 *
 * @property integer $id
 * @property integer $type
 * @property string $number
 * @property integer $status
 * @property integer $sign_time
 * @property string $title
 * @property string $content
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $EmployeeAssign
 * @property string $felicitation_template 
 */
class FelicitationDiscipline extends \yii\db\ActiveRecord
{
    const TYPE_FELICITATION = 1;
    const TYPE_DISCIPLINE = 2;
    
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'felicitation_discipline';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'number', 'created_at', 'updated_at'], 'required'],
            [['type', 'status', 'sign_time', 'created_at', 'updated_at'], 'integer'],
            [['content','EmployeeSearch', 'SignDateString', 'felicitation_template'], 'string'],
            [['number', 'title'], 'string', 'max' => 255],
            [['number'], 'unique'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    private $_employeeSearch;
    private $_employeeAssignId;
    private $_signDateString;
    
    public function getEmployeeAssignId(){
        return $this->_employeeAssignId;
    }
    
    public function setEmployeeAssignId($employeeAssignId) {
        $this->_employeeAssignId = $employeeAssignId;
    }
    
    public function setEmployeeSearch($employeeSearch) {
        $this->_employeeSearch = $employeeSearch;
    }
    
    public function getEmployeeSearch() {
        return $this->_employeeSearch;
    }
    
    public function getSignDateString(){
        if($this->sign_time) {
            return date('d-m-Y',$this->sign_time);
        }
        return '';
    }
    
    public function setSignDateString($signString) {
        if($signString && trim($signString) != '') {
            $this->sign_time = strtotime($signString);
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Mã quyết định',
            'type' => 'Loại quyết định',
            'number' => 'Quyết định số',
            'status' => 'Trạng thái',
            'sign_time' => 'Ngày ký',
            'title' => 'Tiêu đề',
            'content' => 'Nội dung',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'EmployeeSearch' => 'Người thi hành',
            'SignDateString' => 'Ngày ký',
            'felicitation_template' => 'Mẫu quyết định',
            'templateFile' => 'Mẫu quyết định'
        ];
    }
    
    public function getEmployeeAssign() {
        $m = new EmployeeFelicitationDiscipline();
        $data = $m->findAll(['felicitation_discipline_id' => $this->id]);
        $returnData = [];
        if($data) {
            foreach($data as $dat) {
                $returnData[$dat->employee_id] = $dat->employee_id;
            }
        }
        return $returnData;
    }
    
    /**
    * @var UploadedFile
    */
    public $templateFile;
    
    public function upload()
    {
        if ($this->validate()) {
            $path = 'uploads/attack_file/felicitation_discipline/' . md5(Yii::$app->user->identity->username.'_'.time().'_'.$this->templateFile->baseName).'.'. $this->templateFile->extension;
            $this->templateFile->saveAs($path, false);
            $this->felicitation_template = $path;
            return true;
        } else {
            return false;
        }
    }

    public function viewTemplate() {
        return $this->felicitation_template?'<a type="button" class="btn btn-primary" href="/'.$this->felicitation_template.'"><span class="glyphicon glyphicon-cloud-download" aria-hidden="true"></span> Tải quyết định</a>':'<label class="label label-default">Chưa có file quyết định</label>';
    }

    public static function getTypeLabels() {
        return [
            self::TYPE_FELICITATION => 'Khen thưởng',
            self::TYPE_DISCIPLINE => 'Kỷ luật'
        ];
    }
    
    public static function getStatusLabels() {
        return [
            self::STATUS_ACTIVE => 'Hiệu lực',
            self::STATUS_INACTIVE => 'Hết hiệu lực'
        ];
    }
    
}
