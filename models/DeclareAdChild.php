<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "declare_ad_child".
 *
 * @property integer $id
 * @property integer $declare_ad_id
 * @property integer $ad_type
 * @property integer $ad_from_time
 * @property integer $ad_to_time
 * @property string $ad_note
 * @property string $created_by
 * @property string $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $title
 */
class DeclareAdChild extends \yii\db\ActiveRecord
{
    private $_employeeDeployString;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'declare_ad_child';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['declare_ad_id', 'ad_type', 'ad_from_time', 'ad_to_time', 'created_at', 'updated_at'], 'integer'],
            [['ad_note','AdFromTimeString', 'AdToTimeString', 'title','deploy_employee'], 'string'],
            [['total_price'], 'number'],
            [['created_by', 'updated_by'], 'string', 'max' => 255],
            [['declare_ad_id','ad_type'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'declare_ad_id' => 'Declare Ad ID',
            'ad_type' => 'Loại hình',
            'ad_from_time' => 'Ngày bắt đầu đăng',
            'ad_to_time' => 'Ngày kết thúc đăng',
            'ad_note' => 'Ghi chú',
            'created_by' => 'Tạo bởi',
            'updated_by' => 'Cập nhật bởi',
            'created_at' => 'Tạo lúc',
            'updated_at' => 'Cập nhật lúc',
            'AdFromTimeString' => 'Ngày bắt đầu đăng',
            'AdToTimeString' => 'Ngày kết thúc đăng',
            'title' => 'Tiêu đề',
            'deploy_employee' => 'Tác giả',
            'total_price' => 'Giá',
            'EmployeeDeployString' => 'Tác giả',
        ];
    }
    
    public function getAdFromTimeString() {
        if($this->ad_from_time) {
            return date('d-m-Y',$this->ad_from_time);
        }
        return '';
    }
    
    public function setAdFromTimeString($signString) {
        if($signString && trim($signString) != '') {
            $this->ad_from_time = strtotime($signString);
        }
    }
    
    public function getAdToTimeString() {
        if($this->ad_to_time) {
            return date('d-m-Y',$this->ad_to_time);
        }
        return '';
    }
    
    public function setAdToTimeString($timeString) {
        if($timeString && trim($timeString) != '') {
            $this->ad_to_time = strtotime($timeString);
        }
    }
    
    public function setEmployeeDeployString($employeeDeploy) {
        $this->_employeeDeployString = $employeeDeploy;
    }
    
    public function getEmployeeDeployString() {
        return $this->_employeeDeployString;
    }
}
