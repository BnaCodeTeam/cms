<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'language' => 'vi_VI',
    'timeZone' => 'Asia/Ho_Chi_Minh',
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '_t0huH2zsrjLLYv-JvUZ7hs4KzmvfoWW',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\Employee',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
//        'mailer' => [
//            'class' => 'yii\swiftmailer\Mailer',
//            // send all mails to a file by default. You have to set
//            // 'useFileTransport' to false and configure a transport
//            // for the mailer to send real emails.
//            'useFileTransport' => true,
//        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        'authManager' => [
            'class' => 'app\components\DbManager',
        ],
     
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'bna.notify@gmail.com',
                'password' => 'hungminhon231',
                'port' => '25', 
                'encryption' => 'tls', //depends if you need it
                'streamOptions' => [ 
                    'ssl' => [ 
                        'allow_self_signed' => true,
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                    ],
                ],
            ],
        ],
        
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => require(__DIR__ . '/authclient.php'),
        ],
        
        'googleDrive' => [
            'class' => 'lhs\Yii2FlysystemGoogleDrive\GoogleDriveFilesystem',
            'clientId'     => '',
            'clientSecret' => '',
            'refreshToken' => '',
            // 'rootFolderId' => 'xxx ROOT FOLDER ID xxx'
        ],
        'ftpFs' => [
            'class' => 'creocoder\flysystem\FtpFilesystem',
            'host' => 'oluxe.sakura.ne.jp',
            // 'port' => 21,
            'username' => 'oluxe',
            'password' => 'av97y6ng9e',
            // 'ssl' => true,
            // 'timeout' => 60,
            'root' => 'test',
            // 'permPrivate' => 0700,
            // 'permPublic' => 0744,
            // 'passive' => false,
            // 'transferMode' => FTP_TEXT,
        ],
    ],
    'params' => $params,
    'modules' => [ 
        'gridview' => [ 'class' => '\kartik\grid\Module' , 'downloadAction' => 'gridview/export/download' ] 
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
