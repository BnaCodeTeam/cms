<?php

use yii\db\Migration;

class m170103_092008_alter_status_col_in_declare_ad extends Migration
{
    public function up()
    {
        $this->alterColumn('declare_ad', 'status', \yii\db\Schema::TYPE_INTEGER.' DEFAULT '.\app\models\DeclareAd::STATUS_WAIT);
    }

    public function down()
    {
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
