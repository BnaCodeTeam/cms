<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DeclareAd */

$this->title = 'Cập nhật hợp đồng quảng cáo: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Quản lý quảng cáo', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Sửa';
?>
<div class="declare-ad-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'dataNumber' => $dataNumber
    ]) ?>

</div>
