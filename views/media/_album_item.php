<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>
<style>
    .sq:after {
        content: "";
        display: block;
        padding-bottom: 10px;
    }
</style>
<div class="post" style="margin-top: 5px;">
    <div class="col-xs-6 col-md-3">
        <div class="thumbnail">
            <div class="row">
                <div class="col-xs-6 col-md-6 sq" style="padding-right: 5px;">
                    <?php if(isset($model->getCoverImage()[0]) && $model->getCoverImage()[0]->getThumbName(false,'/cover_') && $model->getCoverImage()[0]->getThumbName(false,'/cover_') != ''):?>
                    <img style="width: 100%; display: block;" src="<?=$model->getCoverImage()[0]->getThumbName(false,'/cover_');?>" alt="">
                    <?php else:?>
                    <img alt="100%x180" 
                         data-src="holder.js/100%x180" 
                         style="width: 100%; display: block;" 
                         src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTY2N2ZlYzM0YzQgdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNjY3ZmVjMzRjNCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41NTYyNSI+MTcxeDE4MDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" 
                         data-holder-rendered="true">
                    <?php endif;?>
                </div>
                <div class="col-xs-6 col-md-6 sq" style="padding-left: 5px;">
                    <?php if(isset($model->getCoverImage()[1]) && $model->getCoverImage()[1]->getThumbName(false,'/cover_') && $model->getCoverImage()[1]->getThumbName(false,'/cover_') != ''):?>
                    <img style="width: 100%; display: block;" src="<?=$model->getCoverImage()[1]->getThumbName(false,'/cover_');?>" alt="">
                    <?php else:?>
                    <img alt="100%x180" 
                         data-src="holder.js/100%x180" 
                         style="width: 100%; display: block;" 
                         src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTY2N2ZlYzM0YzQgdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNjY3ZmVjMzRjNCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41NTYyNSI+MTcxeDE4MDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" 
                         data-holder-rendered="true">
                    <?php endif;?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 col-md-6" style="padding-right: 5px;">
                    <?php if(isset($model->getCoverImage()[2]) && $model->getCoverImage()[2]->getThumbName(false,'/cover_') && $model->getCoverImage()[2]->getThumbName(false,'/cover_') != ''):?>
                    <img style="width: 100%; display: block;" src="<?=$model->getCoverImage()[2]->getThumbName(false,'/cover_');?>" alt="">
                    <?php else:?>
                    <img alt="100%x180" 
                         data-src="holder.js/100%x180" 
                         style="width: 100%; display: block;" 
                         src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTY2N2ZlYzM0YzQgdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNjY3ZmVjMzRjNCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41NTYyNSI+MTcxeDE4MDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" 
                         data-holder-rendered="true">
                    <?php endif;?>
                </div>
                <div class="col-xs-6 col-md-6" style="padding-left: 5px;">
                    <a href="/media-album/view?id=<?=$model->id;?>">
                        <img alt="100%x180" data-src="holder.js/100%x180" style="width: 100%; display: block;" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIgogICB4bWxuczppbmtzY2FwZT0iaHR0cDovL3d3dy5pbmtzY2FwZS5vcmcvbmFtZXNwYWNlcy9pbmtzY2FwZSIKICAgd2lkdGg9IjE3MSIKICAgaGVpZ2h0PSIxODAiCiAgIHZpZXdCb3g9IjAgMCAxNzEgMTgwIgogICBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJub25lIgogICB2ZXJzaW9uPSIxLjEiCiAgIGlkPSJzdmcxMyIKICAgc29kaXBvZGk6ZG9jbmFtZT0ibW9yZS5zdmciCiAgIGlua3NjYXBlOnZlcnNpb249IjAuOTIuMyAoMjQwNTU0NiwgMjAxOC0wMy0xMSkiPgogIDxtZXRhZGF0YQogICAgIGlkPSJtZXRhZGF0YTE3Ij4KICAgIDxyZGY6UkRGPgogICAgICA8Y2M6V29yawogICAgICAgICByZGY6YWJvdXQ9IiI+CiAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+CiAgICAgICAgPGRjOnR5cGUKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIiAvPgogICAgICAgIDxkYzp0aXRsZT48L2RjOnRpdGxlPgogICAgICA8L2NjOldvcms+CiAgICA8L3JkZjpSREY+CiAgPC9tZXRhZGF0YT4KICA8c29kaXBvZGk6bmFtZWR2aWV3CiAgICAgcGFnZWNvbG9yPSIjZmZmZmZmIgogICAgIGJvcmRlcmNvbG9yPSIjNjY2NjY2IgogICAgIGJvcmRlcm9wYWNpdHk9IjEiCiAgICAgb2JqZWN0dG9sZXJhbmNlPSIxMCIKICAgICBncmlkdG9sZXJhbmNlPSIxMCIKICAgICBndWlkZXRvbGVyYW5jZT0iMTAiCiAgICAgaW5rc2NhcGU6cGFnZW9wYWNpdHk9IjAiCiAgICAgaW5rc2NhcGU6cGFnZXNoYWRvdz0iMiIKICAgICBpbmtzY2FwZTp3aW5kb3ctd2lkdGg9IjE5MjAiCiAgICAgaW5rc2NhcGU6d2luZG93LWhlaWdodD0iMTAwMSIKICAgICBpZD0ibmFtZWR2aWV3MTUiCiAgICAgc2hvd2dyaWQ9ImZhbHNlIgogICAgIGlua3NjYXBlOnpvb209IjEuMzExMTExMSIKICAgICBpbmtzY2FwZTpjeD0iLTQzLjAxNjk0OSIKICAgICBpbmtzY2FwZTpjeT0iOTAiCiAgICAgaW5rc2NhcGU6d2luZG93LXg9Ii05IgogICAgIGlua3NjYXBlOndpbmRvdy15PSItOSIKICAgICBpbmtzY2FwZTp3aW5kb3ctbWF4aW1pemVkPSIxIgogICAgIGlua3NjYXBlOmN1cnJlbnQtbGF5ZXI9ImhvbGRlcl8xNjY3ZmVjMzRjNCIgLz4KICA8IS0tClNvdXJjZSBVUkw6IGhvbGRlci5qcy8xMDAleDE4MApDcmVhdGVkIHdpdGggSG9sZGVyLmpzIDIuNi4wLgpMZWFybiBtb3JlIGF0IGh0dHA6Ly9ob2xkZXJqcy5jb20KKGMpIDIwMTItMjAxNSBJdmFuIE1hbG9waW5za3kgLSBodHRwOi8vaW1za3kuY28KLS0+CiAgPGRlZnMKICAgICBpZD0iZGVmczQiPgogICAgPHN0eWxlCiAgICAgICB0eXBlPSJ0ZXh0L2NzcyIKICAgICAgIGlkPSJzdHlsZTIiPjwhW0NEQVRBWyNob2xkZXJfMTY2N2ZlYzM0YzQgdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT4KICA8L2RlZnM+CiAgPGcKICAgICBpZD0iaG9sZGVyXzE2NjdmZWMzNGM0Ij4KICAgIDxyZWN0CiAgICAgICB3aWR0aD0iMTcxIgogICAgICAgaGVpZ2h0PSIxODAiCiAgICAgICBmaWxsPSIjRUVFRUVFIgogICAgICAgaWQ9InJlY3Q2IiAvPgogICAgPGcKICAgICAgIGlkPSJnMTAiPgogICAgICA8dGV4dAogICAgICAgICB4PSI0Mi43ODI4NDEiCiAgICAgICAgIHk9Ijk1LjMxODk2MiIKICAgICAgICAgaWQ9InRleHQ4IgogICAgICAgICBzdHlsZT0iZm9udC13ZWlnaHQ6Ym9sZDtmb250LXNpemU6MTMuMzMzMzMzMDJweDtmb250LWZhbWlseTpBcmlhbCwgSGVsdmV0aWNhLCAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZpbGw6I2FhYWFhYSI+CiAgICAgICAgPHRzcGFuCiAgICAgICAgICAgc3R5bGU9ImZvbnQtc2l6ZToxNy4zMzMzMzM5N3B4IgogICAgICAgICAgIGlkPSJ0c3BhbjM3MzUiPlhlbSB0aMOqbS4uLjwvdHNwYW4+CiAgICAgIDwvdGV4dD4KICAgIDwvZz4KICA8L2c+Cjwvc3ZnPgo=" data-holder-rendered="true">
                    </a>
                </div>
            </div>
            <div class="caption">
                <p class="hls-caption" style="margin-bottom: 0px !important; display: block; line-height: 30px; max-height: 30px; overflow: hidden; height: 30px;">
                    <span class="label label-info"><?=$model->name;?></span>
                </p>
            </div>
        </div>
    </div>
</div>
