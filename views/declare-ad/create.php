<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DeclareAd */

$this->title = 'Tạo hợp đồng quảng cáo';
$this->params['breadcrumbs'][] = ['label' => 'Quản lý quảng cáo', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="declare-ad-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
