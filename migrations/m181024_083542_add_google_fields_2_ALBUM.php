<?php

use yii\db\Migration;

/**
 * Class m181024_083542_add_google_fields_2_ALBUM
 */
class m181024_083542_add_google_fields_2_ALBUM extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('media_album', 'google_drive_id', $this->string());
        $this->addColumn('media_album', 'google_drive_link', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('media_album', 'google_drive_id');
        $this->dropColumn('media_album', 'google_drive_link');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181024_083542_add_google_fields_2_ALBUM cannot be reverted.\n";

        return false;
    }
    */
}
