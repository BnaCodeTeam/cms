<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Documentary */

$this->title = $model->getTitle();
$this->params['breadcrumbs'][] = ['label' => 'Quản lý công văn', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
table.detail-view th {
    width: 20%;
}

table.detail-view td {
    width: 80%;
}
</style>
<div class="documentary-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Sửa', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Xóa', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Bạn có chắc muốn xóa công văn này?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            [
                'label' => 'Loại công văn',
                'format' => 'raw',
                'value' => isset(app\models\Documentary::getTypeLabels()[$model->type])?app\models\Documentary::getTypeLabels()[$model->type]:'',
            ],
            'send_number',
            'receive_number',
            'send_by_name',
            'receive_by_name',
            'sendDateString',
            'receiveDateString',
            'sign',
            'note:ntext',
            [
                'label' => 'Bản sao lưu',
                'format' => 'raw',
                'value' => function ($model) {
                    $arrayAttachment = [];
                    if($model->attachment && $model->attachment != '') {
                        $arrayAttachment = explode(",", $model->attachment);
                    }
                    if(!empty($arrayAttachment)) {
                        $htmlRow = '';
                        for($i = 0; $i < sizeof($arrayAttachment); $i++) {
                            $htmlRow = $htmlRow . '<div class="col-xs-6 col-md-3">'
                                    . '<a href="'.$arrayAttachment[$i].'" class="thumbnail">'
                                    . '<img style="height: 180px; width: 100%; display: block;" src="'.$arrayAttachment[$i].'" >'
                                    . '</a>'
                                    . '</div>';
                            if((($i+1)%4) == 0) {
                                $htmlRow = $htmlRow.'</div><div class="row">';
                            }
                        }
                        return '<div class="row">' . $htmlRow.'</div>';
                    }
                    return '';
                }
            ],
            [
                'label' => 'Tạo bởi',
                'format' => 'raw',
                'value' => $model->created_by . ' <i>('.date('d-m-Y h:i:s', $model->created_at).')</i>'
            ],
        ],
    ]) ?>

</div>
