<?php

use yii\db\Migration;

class m161117_032512_add_declare_template_to_declare_ad extends Migration
{
    public function up()
    {
        $this->addColumn('declare_ad', 'declare_template', $this->string());
    }

    public function down()
    {
        $this->dropColumn('declare_ad', 'declare_template');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
