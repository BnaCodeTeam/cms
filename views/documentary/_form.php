<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Documentary */
/* @var $form yii\widgets\ActiveForm */
use kartik\file\FileInput;
use app\models\Documentary;
use yii\web\View;
use yii\jui\DatePicker;
?>


<?php 
$formatJs = "
function showByType() {
    var docType = $('#documentary-type').val();
    if(docType && typeof docType != 'undefined' && docType != '') {
        if(docType == ".Documentary::TYPE_SEND.") {
            $('#documentary-send_by_name').val('".Documentary::SEND_NAME_DEFAULT."');
            $('#documentary-send_by_name').attr('readonly', true);
            $('#documentary-receive_by_name').val('".((!$model->isNewRecord)?$model->receive_by_name:'')."');
            $('#documentary-receive_by_name').attr('readonly', false);
            
            $('.td-receive-by-name').attr('colspan',2);
            $('.td-send-by-name').attr('colspan',1);
            $('.receive-by-name').show();
            $('.send-by-name').show();
            
            $('.receive-number').hide();
            $('.receive-time').hide();
        }
        if(docType == ".Documentary::TYPE_RECEIVE.") {
            $('.receive-number').show();
            $('.receive-time').show();
            
            $('#documentary-receive_by_name').val('".Documentary::RECEIVE_NAME_DEFAULT."');
            $('#documentary-receive_by_name').attr('readonly', true);
            $('#documentary-send_by_name').val('".((!$model->isNewRecord)?$model->send_by_name:'')."');
            $('#documentary-send_by_name').attr('readonly', false);
            
            $('.td-receive-by-name').attr('colspan',1);
            $('.td-send-by-name').attr('colspan',2);
            $('.receive-by-name').show();
            $('.send-by-name').show();
        }
    }
}
showByType();
$('#documentary-type').on('change',function() {
    showByType();
});";

$this->registerJs($formatJs, View::POS_END);
?>

<div class="documentary-form">
    
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    
    <div class="table-responsive"> 
        <table class="table table-bordered table-striped">
            <tbody>
                <tr> 
                    <td colspan="3">
                        <div class="form-group">
                            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                        <div class="form-group">
                            <?= $form->field($model, 'type')->dropDownList(Documentary::getTypeLabels(),['prompt'=>'---- Chọn loại công văn ----']) ?>
                        </div>
                    </td>
                    <td colspan="1">
                        <div class="form-group">
                            <?= $form->field($model, 'send_number')->textInput() ?>
                        </div>
                        <div class="form-group receive-number" style="display: none;">
                            <?= $form->field($model, 'receive_number')->textInput() ?>
                        </div>
                    </td>
                    <td colspan="1">
                        <div class="form-group" >
                            <?= $form->field($model,'sendDateString')->widget(DatePicker::className(),
                                    ['dateFormat' => 'dd-MM-yyyy','options' => ['class' => 'form-control'],'clientOptions' => ['defaultDate' => '01-01-2018']]) ?>
                        </div>
                        <div class="form-group receive-time" style="display: none;">
                            <?= $form->field($model,'receiveDateString')->widget(DatePicker::className(),
                                    ['dateFormat' => 'dd-MM-yyyy','options' => ['class' => 'form-control'],'clientOptions' => ['defaultDate' => '01-01-2018']]) ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="td-send-by-name" colspan="1">
                        <div class="form-group send-by-name" style="display: none;">
                            <?= $form->field($model, 'send_by_name')->textInput(['maxlength' => true]) ?>
                        </div>
                    </td>
                    <td class="td-receive-by-name" colspan="2">
                        <div class="form-group receive-by-name" style="display: none;">
                            <?= $form->field($model, 'receive_by_name')->textInput(['maxlength' => true]) ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="form-group">
                            <?= $form->field($model, 'note')->textarea(['rows' => 6]) ?>
                        </div>
                    </td>
                    <td colspan="1">
                        <div class="form-group">
                            <?= $form->field($model, 'sign')->textInput() ?>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <?php 
        $pluginOptions = ['maxFileSize' => 2800];
        if(!$model->isNewRecord) {
            if($model->attachment && $model->attachment != null && $model->attachment != '') {
                $pluginOptions['initialPreview'] = explode(",", $model->attachment);
                $pluginOptions['initialPreviewAsData'] = true;
                $pluginOptions['initialCaption'] = $model->name;
                $initialPreviewConfig = [];
                if(!empty($pluginOptions['initialPreview'])) {
                    foreach ($pluginOptions['initialPreview'] as $imgUrl) {
                        $xData = explode("/", $imgUrl);
                        $caption = end($xData);
                        $initialPreviewConfig[] = ['caption' => $caption];
                    }
                }
                
                $pluginOptions['initialPreviewConfig'] = $initialPreviewConfig;
                $pluginOptions['overwriteInitial'] = true;
            }
        }
        echo $form->field($model, 'documentFiles[]')->widget(FileInput::classname(), [
            'options' => ['accept' => 'image/*','multiple' => true],
            'pluginOptions' => $pluginOptions
        ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Tạo mới' : 'Cập nhật', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    
</div>
