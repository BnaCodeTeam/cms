<?php

use yii\db\Migration;

/**
 * Class m181023_030749_create_table_media_category
 */
class m181023_030749_create_table_media_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('media_category', [
            'id' => $this->primaryKey(),
            
            'name' => $this->string(),
            
            'parent_id' => $this->integer(),
            
            'status' => $this->smallInteger()->defaultValue(10),
            
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            
            'created_by' => $this->string(),
            'updated_by' => $this->string(),
            
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return $this->dropTable('media_category');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181023_030749_create_table_media_category cannot be reverted.\n";

        return false;
    }
    */
}
