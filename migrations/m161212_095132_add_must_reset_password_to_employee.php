<?php

use yii\db\Migration;

class m161212_095132_add_must_reset_password_to_employee extends Migration
{
    public function up()
    {
        $this->addColumn('employee', 'reset_password_require', yii\db\Schema::TYPE_INTEGER.'(5) DEFAULT 1');
    }

    public function down()
    {
        $this->dropColumn('employee', 'reset_password_require');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
