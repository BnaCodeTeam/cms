<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Nhân viên');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'full_name',
            //'username',           
            'email:email',
            'phone',           
            ['attribute' => 'Trạng thái', 'format' => 'raw', 'value' => function($model) {
                $class = '';
                if($model->status == app\models\Employee::STATUS_ACTIVE) {
                    $class = 'success';
                } elseif($model->status == app\models\Employee::STATUS_DELETED) {
                    $class = 'warning';
                } else {
                    $class = 'danger';
                }
                return (isset(app\models\Employee::getStatusLabels()[$model->status]))? 
                '<span class="label label-'.$class.'">'. app\models\Employee::getStatusLabels()[$model->status].'</span>' : '';                
            }]
        ],
        'options' => ['class' => 'grid-view table-responsive']
    ]); ?>

</div>
