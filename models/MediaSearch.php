<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Media;

/**
 * MediaSearch represents the model behind the search form of `app\models\Media`.
 */
class MediaSearch extends Media
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'status', 'created_at', 'updated_at', 'category_id','author_id'], 'integer'],
            [['title', 'description', 'url', 'created_by', 'updated_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Media::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
        ]);

        $this->load($params);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if($this->category_id && $this->category_id > 0) {
            $query->andFilterWhere([
                'category_id' => $this->category_id,
            ]);
        }
        if($this->author_id && $this->author_id > 0) {
            $query->andFilterWhere([
                'author_id' => $this->author_id,
            ]);
        }
        $query ->andFilterWhere(['status' => MediaAlbum::STATUS_ACTIVE]);
        

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description]);
            //->andFilterWhere(['like', 'url', $this->url]);
            //->andFilterWhere(['like', 'created_by', $this->created_by])
            //->andFilterWhere(['like', 'updated_by', $this->updated_by]);

        return $dataProvider;
    }
}
