<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MediaAlbumSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Albums';
$this->params['breadcrumbs'][] = ['label' => 'Media', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .row .summary {
        margin-left: 15px;
    }
    .row .pagination {
        margin-left: 15px;
    }
    .row .empty {
        margin-left: 15px;
    }
</style>
<div class="media-album-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p><?php echo $this->render('_search-album', ['model' => $searchModel]); ?></p>

    <p>
        <?= Html::a('Tạo Media Album', ['create-album-ajax'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="row">
        <?php 
        echo ListView::widget([
        'dataProvider' => $dataProvider,
            'itemView' => '_album_item',
        ]);
        ?>
    </div>
</div>
