<?php

use yii\db\Migration;

/**
 * Class m171213_023334_create_table_royalty_file_import
 */
class m171213_023334_create_table_royalty_file_import extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('royalty_file_import', [
            'id' => $this->primaryKey(),
            'file_dir' => $this->string(),
            'royalty_date' => $this->string(),
            'created_by' => $this->string(),
            'updated_by' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        return $this->dropTable('royalty_file_import');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171213_023334_create_table_royalty_file_import cannot be reverted.\n";

        return false;
    }
    */
}
