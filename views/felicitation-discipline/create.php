<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FelicitationDiscipline */

$this->title = 'Tạo quyết định';
$this->params['breadcrumbs'][] = ['label' => 'Danh sách quyết định', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="felicitation-discipline-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
