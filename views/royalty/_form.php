<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Royalty */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="royalty-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'royalty_month')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'royalty_week')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'article_count')->textInput() ?>

    <?= $form->field($model, 'post_count')->textInput() ?>

    <?= $form->field($model, 'photo_count')->textInput() ?>

    <?= $form->field($model, 'rate')->textInput() ?>

    <?= $form->field($model, 'royalty_amount')->textInput() ?>

    <?= $form->field($model, 'tax_amount')->textInput() ?>

    <?= $form->field($model, 'royalty_total_amount')->textInput() ?>

    <?= $form->field($model, 'paid_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'updated_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'is_sended')->textInput() ?>

    <?= $form->field($model, 'sended_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
