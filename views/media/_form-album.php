<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Media */
/* @var $form yii\widgets\ActiveForm */
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\web\View;
?>


<?php 
$formatJs = <<< 'JS'
function IsNumeric(input){
    var RE = /^-{0,1}\d*\.{0,1}\d+$/;
    return (RE.test(input));
};
var formatEmployee = function(employee){
        if(employee.id != 'undefined' && IsNumeric(employee.id)) {
            var imx = 'uploads/images/users/no-user-image.png';
            if(employee.image != 'undefined' && employee.image != null) {
                imx = employee.image;
            }
            var ma = '<div class="row">' + 
                    '<div class="col-md-12">' +
                        '<img data-holder-rendered="true" style="width: 64px; height: 64px; data-src="holder.js/64x64" src="/' + imx + '" class="img-rounded" />' + 
                        '<b style="margin-left:5px">' + employee.text + '</b>' + 
                    '</div>' + 
                '</div>';
            return '<div style="overflow:hidden;">' + ma + '</div>';
        }
    };
    
var formatSelectionEmployee = function(employee) {
    return employee.full_name || employee.text;
};


var eventSelectEmployee = function(item) {
    if(item.params.data.selected) {
        //console.log(item.params.data);
    }
};
JS;

$this->registerJs($formatJs, View::POS_HEAD);
$this->registerJsFile('/js/media-item.js', ['depends' => [\yii\web\JqueryAsset::className()], 'position' => View::POS_END]); ?>

<div class="media-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6"><?= $form->field($album, 'name')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-6"><?= $form->field($album, 'description')->textInput(['maxlength' => true]) ?></div>
    </div>

    <div class="row">
        <div class="col-md-6"><?= $form->field($album, 'category_id')->dropDownList([''=>'---- Chọn chuyên mục ----'] + \app\models\MediaCategory::getListCategory()) ?></div>
        <div class="col-md-6">
            <?php $url = Yii::$app->urlManager->createUrl(['employee/ajax-get-employee-list']);?>
            <?php echo $form->field($album, 'author_id')->widget(Select2::classname(), [
                'initValueText' => app\models\Employee::getAliasAuthorById($album->author_id),
                'options' => ['placeholder' => 'Chọn tác giả', 'id' => 'media-select-author-id'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 2,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Đang tìm dữ liệu...'; }"),
                    ],
                    'ajax' => [
                        'url' => $url,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                        'delay' => 250,
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('formatEmployee'),
                    'templateSelection' => new JsExpression('formatSelectionEmployee'),
                    'tags' => true,
                    //'width' => '300px',
                ],
                'pluginEvents' => [
                    'select2:select' => new JsExpression('eventSelectEmployee'),
                ],
                'addon' => [
                    'append' => [
                        'content' => Html::button('<span class="glyphicon glyphicon-plus"></span>', [
                            'class' => 'btn btn-primary', 
                            'style' => 'height: 99%;',
                            'id' => 'btn-add-author',
                            'title' => 'Thêm tác giả', 
                            'data-toggle' => 'tooltip'
                        ]),
                        'asButton' => true
                    ]
                ]

            ]);?>
        </div>
    </div>

    <?php 
        $pluginOptions = ['maxFileSize' => 30000];
        echo $form->field($media, 'multiMediaFiles[]')->widget(FileInput::classname(), [
            'options' => ['multiple' => true,'accept' => 'image/*'],
            'pluginOptions' => $pluginOptions
        ]); 
    ?>
    
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
