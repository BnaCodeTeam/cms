<?php

namespace app\models;

use Yii;

use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use app\models\FelicitationDiscipline;

/**
 * This is the model class for table "employee".
 *
 * @property integer $id
 * @property string $username
 * @property string $full_name
 * @property string $email
 * @property string $phone
 * @property integer $birth
 * @property integer $sex
 * @property string $address
 * @property integer $joined_date
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $id_number
 * @property integer $id_number_time
 * @property string $id_number_place
 * @property integer $user_type
 * @property string $birthplace
 * @property string $resident
 * @property string $bank_account
 * @property integer $bank_id
 * @property string $native_land
 * @property string $ethnic_group
 * @property string $religion
 * @property string $employee_coefficient
 * @property string $height
 * @property string $weight
 * @property string $health
 * @property string $blood_group
 * @property string $image
 * @property integer $reset_password_require
 * @property integer $is_send_active
 * @property string $alias_author
 */
class Employee extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    
    const SEX_MALE = 1;
    const SEX_FEMALE = 0;
    
    const TYPE_CTV = 2;
    const TYPE_NV = 1;
    
    const BANK_VIB = 1;
    
    const DEFAULT_PASSWORD = 'abc123!@#';
    const DEFULT_SUPPER_USER = 'admin';
    
    const RESET_PWD_REQUIRE = 1;
    const RESET_PWD_NOT_REQUIRE = 2;
    
    const IS_SEND_ACTIVE = 1;
    const IS_NOT_SEND_ACTIVE = 0;

    private $_joinedDateString;
    private $_birthString;
    private $_idNumberTimeString;
    

    public function getJoinedDateString(){
        if($this->joined_date) {
            return date('d-m-Y',$this->joined_date);
        }
        return '';
    }
    
    public function getBirthString() {
        if($this->birth) {
            return date('d-m-Y',$this->birth);
        }
        return '';
    }
    
    public function getIdNumberTimeString() {
        if($this->id_number_time) {
            return date('d-m-Y',$this->id_number_time);
        }
        return '';
    }
   
    public function setJoinedDateString($joinedString) {
        if($joinedString && trim($joinedString) != '') {
            $this->joined_date = strtotime($joinedString);
        }
    }
    
    public function setBirthString($birthSring) {
        if($birthSring && trim($birthSring) != '') {
            $this->birth = strtotime($birthSring);
        }
    }
    
    public function setIdNumberTimeString($timeSring) {
        if($timeSring && trim($timeSring) != '') {
            $this->id_number_time = strtotime($timeSring);
        }
    }
    
    /**
    * @var UploadedFile
    */
    public $imageFile;
    
    public function upload()
    {
        if ($this->validate()) {
            $path = 'uploads/images/users/' . $this->username. '_' .$this->imageFile->baseName . '.' . $this->imageFile->extension;
            $this->imageFile->saveAs($path, false);
            $this->image = $path;
            return true;
        } else {
            return false;
        }
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employee';
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'email', 'auth_key', 'password_hash', 'created_at', 'updated_at', 
                'id_number', 'id_number_place', 'id_number_time', 'full_name', 'phone', 'idNumberTimeString', 'birthString'], 'required'],
            [['birth', 'sex', 'status', 'created_at', 'updated_at', 'id_number_time', 'user_type', 'bank_id', 'reset_password_require', 'is_send_active'], 'integer'],
            [['address'], 'string'],
            [['username', 'full_name', 'email', 'phone', 'password_hash', 'password_reset_token', 'bank_account', 'image', 'alias_author'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['phone'],'unique'],
            [['password_reset_token'], 'unique'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['joinedDateString','birthString', 'idNumberTimeString'], 'string'],
            [['id_number', 'id_number_place', 'birthplace', 'resident',
                'native_land','ethnic_group','religion','employee_coefficient','height','weight','health','blood_group' ], 'string'],
            [['idNumberTimeString','joinedDateString','birthString'], 'date', 'format'=>'php:d-m-Y'],
            ['email', 'email'],
            [['username', 'email'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
            ['phone','validatePhone'],
            ['id_number','validateIdNumber'],
            [['bank_account', 'bank_id'], 'unique', 'targetAttribute' => ['bank_account']],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['id_number'], 'unique']
        ];
    }
    
    public function validatePhone()
    {
        if(!is_numeric($this->phone) || strlen($this->phone) > 11 || strlen($this->phone) < 10 ) {
            $this->addError('phone',"Số điện thoại không hợp lệ");
        }
    }
    
    public function validateIdNumber() {
        if(!is_numeric($this->id_number)) {
            $this->addError('id_number',"Số chứng minh thư không hợp lệ");
        }
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Mã nhân viên',
            'username' => 'Tên đăng nhập',
            'full_name' => 'Họ tên',
            'email' => 'Email',
            'phone' => 'Điện thoại',
            'birth' => 'Ngày sinh',
            'sex' => 'Giới tính',
            'address' => 'Địa chỉ',
            'joined_date' => 'Ngày gia nhập cơ quan',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Mật khẩu',
            'password_reset_token' => 'Password Reset Token',
            'status' => 'Trạng thái',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'joinedDateString' => 'Ngày gia nhập cơ quan',
            'birthString' => 'Ngày sinh',
            'idNumberTimeString' => 'Ngày cấp',
            'id_number' => 'Số CMND', 
            'id_number_place' => 'Nơi cấp', 
            'id_number_time' => 'Ngày cấp', 
            'user_type' => 'Loại đối tượng',
            'birthplace' => 'Nơi sinh', 
            'resident' => 'Hộ khẩu thường trú',
            'bank_account' => 'Tài khoản ngân hàng',
            'bank_id' => 'Mã ngân hàng',
            'native_land' => 'Quê quán',
            'ethnic_group' => 'Dân tộc',
            'religion' => 'Tôn giáo',
            'employee_coefficient' => 'Ngạch công chức',
            'height' => 'Chiều cao',
            'weight' => 'Cân nặng',
            'health' => 'Tình trạng sức khỏe hiện tại',
            'blood_group' => 'Nhóm máu',
            'image' => 'Ảnh đại diện',
            'imageFile' => 'Ảnh đại diện',
            'reset_password_require' => 'Bắt buộc đổi mật khẩu',
            'is_send_active' => 'Gửi kích hoạt tài khoản',
            'alias_author' => 'Bút danh'
        ];
    }

    
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }
    
    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    
    
    public function getFelicitationDiscipline($withStyle = true) {
        $m = new EmployeeFelicitationDiscipline();
        $dataModel = $m->findAll(['employee_id' => $this->id]);
        $returnData = [];
        if($dataModel) {
            foreach($dataModel as $data) {
                $returnData[$data->felicitation_discipline_type][] = ['felicitation_discipline_id' => $data->felicitation_discipline_id, 'felicitation_discipline_number' => $data->felicitation_discipline_number];
            }
        }
        if($withStyle == true) {
            $returnDataStyle = [];
            foreach($returnData as $k => $v) {
                $itemStyle = [];
                foreach ($v as $vx) {
                    $itemStyle[] = '<span class="label label-info"><a href="'.\Yii::$app->urlManager->createUrl(['/felicitation-discipline/view','id' => $vx['felicitation_discipline_id']]).'">#'.$vx['felicitation_discipline_number'].'</a></span>';
                }
                
                $returnDataStyle[$k] = implode(" ", $itemStyle);
            }
            return $returnDataStyle;
        }
        return $dataModel;
    }
    
    public function generateUsernameByFullName($fullname) {
        $username = $this->vnToStr($fullname);
        $username = strtolower($username);
        $username = preg_replace('/\s+/', '', $username);
        return $username;
    }
    
    public function generateRandomPhoneNumber() {
        for ($randomNumber = mt_rand(1, 9), $i = 1; $i < 10; $i++) {
            $randomNumber .= mt_rand(0, 9);
        }
        return $randomNumber;
    }
    
    public function generateEmployee() {
        if($this->full_name != null) {
            $this->full_name = preg_replace('/\s+/', ' ', $this->full_name);
            if($this->isNewRecord && $this->full_name != '' && $this->full_name != ' ') {
                $username = $this->generateUsernameByFullName($this->full_name);
                $this->username = $username;
                $this->email = $username.'@gmail.com';
                $this->phone = $this->generateRandomPhoneNumber();
                $this->setPassword($username);
                $this->generateAuthKey();
                $this->status = Employee::STATUS_ACTIVE;
                $this->created_at = time();
                $this->updated_at = time();
                $this->bank_id = 1;
                $this->bank_account = Yii::$app->security->generateRandomString(8);
                $this->user_type = self::TYPE_CTV;
            }
        }
    }


    protected function vnToStr($str) {
        $unicode = array(
            'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
            'd'=>'đ',
            'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
            'i'=>'í|ì|ỉ|ĩ|ị',
            'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
            'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
            'y'=>'ý|ỳ|ỷ|ỹ|ỵ',

            'A'=>'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
            'D'=>'Đ',
            'E'=>'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
            'I'=>'Í|Ì|Ỉ|Ĩ|Ị',
            'O'=>'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
            'U'=>'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
            'Y'=>'Ý|Ỳ|Ỷ|Ỹ|Ỵ',
        );
        foreach($unicode as $nonUnicode=>$uni){
            $str = preg_replace("/($uni)/i", $nonUnicode, $str);
        }
        //$str = str_replace(' ','_',$str);
        return $str;
    }


    public static function getEmployeeDeployInfo($withId = false, $deployString) {
        $data = '';
        if($deployString 
                && $deployString != null 
                && trim($deployString) != '') {
            $eIds = explode(',', $deployString);
            if(!empty($eIds)) {
                $employees = Employee::find()
                        ->where(['id' => $eIds])
                        ->all();
                
                if($employees) {
                    $es = [];
                    foreach ($employees as $e) {
                        if($withId) {
                            $es[$e->id] = $e->full_name . ' (' . $e->username . ')';
                        } else {
                            $es[] = $e->full_name . ' (' . $e->username . ')';
                        }
                    }
                    if($withId) {
                        $data = $es;
                    } else {
                        $data = implode(', ', $es);
                    }
                }
            }
        }
        return $data;
    }


    public static function getStatusLabels() {
        return[
            self::STATUS_DELETED => 'Ngừng hoạt động',
            self::STATUS_ACTIVE => 'Đang hoạt động'
        ];
    }
    
    public static function getSexLabels() {
        return [
            self::SEX_FEMALE => 'Nữ',
            self::SEX_MALE => 'Nam'
        ];
    }
    
    public static function getUserTypeLabels() {
        return [
            self::TYPE_NV => 'Nhân viên',
            self::TYPE_CTV => 'Cộng tác viên'
        ];
    }
    
    public static function getBankLabels() {
        return [
            self::BANK_VIB => 'VIB'
        ];
    }
    
    public static function getBloodGroupLabels() {
        return [
            'O' => 'O',
            'A' => 'A',
            'B' => 'B',
            'AB' => 'AB'
        ];
    }

    public static function createUserSuperAdmin(){
        $passDefault = self::DEFAULT_PASSWORD;
        $model = new Employee();
        $model->username = self::DEFULT_SUPPER_USER;
        $model->password_hash = $passDefault;
        $model->email='admin@bna.erp.vn';
        $model->phone = '0246246810';
        $model->status = Employee::STATUS_ACTIVE;
        $model->sex = Employee::SEX_MALE;
        $model->full_name = 'Lê Admin';
        $model->created_at = time();
        $model->updated_at = time();
        $model->id_number = '123456';
        $model->id_number_place = 'Nghệ An';
        $model->idNumberTimeString = '01-01-2008';
        $model->birthString = '01-01-1988';
        $model->setPassword($model->password_hash);
        $model->generateAuthKey();        
        if ($model->save()) { 
            $auth= new \app\components\DbManager();
            $adminRole = $auth->createRole($auth->fullAccessName);
            //$auth->add($adminRole);
            if($auth->assign($adminRole,$model->id)) {
                echo 'Tạo tài khoản superAdmin thành công username:admin pasword: '.$passDefault;
            } else {
                $model->delete();
                echo 'Lỗi: không phân quyền được cho tài khoản superAdmin'; 
            }
        } else {
            print_r($model->getErrors());
        }      
    }
    
    public static function getAliasAuthorById($id) {
        $e = Employee::find()->where(['OR', ['id' => $id], ['username' => $id]])->one();
        if($e) {
            if($e->alias_author && $e->alias_author != NULL && $e->alias_author != '') {
                return $e->alias_author;
            }
            $e->full_name = preg_replace('/\s+/', ' ', $e->full_name);
            $a = explode(" ", $e->full_name);
            
            $s = sizeof($a);
            if($s >= 2) {
                $i1 = $s - 2;
                $i2 = $s - 1;
                return $a[$i1]. " " . $a[$i2];
            } else {
                return $e->full_name;
            }
        }
        return '';
    }

}
