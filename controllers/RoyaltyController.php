<?php

namespace app\controllers;

use Yii;
use app\models\Royalty;
use app\models\RoyaltySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;

/**
 * RoyaltyController implements the CRUD actions for Royalty model.
 */
class RoyaltyController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Royalty models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RoyaltySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Royalty model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Royalty model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        return $this->redirect(['index']);
        $model = new Royalty();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Royalty model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Royalty model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        return $this->redirect(['index']);
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionAjaxSendMail() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $ids = Yii::$app->request->post('ids');
        $out = ['error' => 0, 'error_mail' => []];
        if($ids && is_array($ids) && !empty($ids)) {
            $royaltySendMail = Royalty::find()
                    ->where(['id' => $ids])
                    ->andWhere(['is_sended' => Royalty::IS_NOT_SENDED])
                    ->andWhere('email != ""')
                    ->all();
            
            if($royaltySendMail) {
                foreach($royaltySendMail as $royalty) {
                    if (filter_var($royalty->email, FILTER_VALIDATE_EMAIL)) {
                        $mailContent = '<p>Xin chào ' . $royalty->full_name.'</p>'.
                            '<p>Báo Nghệ An gửi bảng kê nhuận bút '.$royalty->royalty_week.' tháng '.$royalty->royalty_month.'</p>'.
                            '<p>Tổng số bài: '.$royalty->article_count.'</p>'.
                            '<p>Tổng số tin: '.$royalty->post_count.'</p>'.
                            '<p>Tổng số ảnh: '.$royalty->photo_count.'</p>'.
                            '<p>Tiền nhuận bút: '.Yii::$app->formatter->asDecimal($royalty->royalty_amount,0) . 'đ'.'</p>'.
                            '<p>Trừ thuế: '.Yii::$app->formatter->asDecimal($royalty->tax_amount,0) . 'đ'.'</p>'.
                            '<p>Tổng tiền thực lĩnh: '.Yii::$app->formatter->asDecimal($royalty->royalty_total_amount,0) . 'đ'.'</p>'.
                            '<p><i>Đây là email được gửi tự động, bạn không cần phải hồi đáp.</i></p>'.
                            '<p><b>Nếu có bất kì thắc mắc nào xin vui lòng liên hệ Báo Nghệ An - số 3 Đại Lộ Lê Nin, TP Vinh</b></p>'.
                            '<p><b>Điện thoại: (023)83.588138</b></p>'.
                            '<p><b><i>Xin cảm ơn!</i></b></p>';
                        try {
                            $isSended = $this->sendMail($royalty->email,$mailContent);
                            if($isSended) {
                                $royalty->is_sended = Royalty::IS_SENDED;
                                $royalty->sended_at = time();
                                $royalty->updated_by = Yii::$app->user->identity->username;
                                $royalty->save(true,['is_sended','sended_at','updated_by']);
                            }
                        } catch (Exception $exc) {
                            echo $exc->getTraceAsString();
                        }
                    } else {
                        $out['error_mail'][] = $royalty->email;
                    }
                }
            }
        }
        return $out;
    }
    
    protected function sendMail($to,$content,$from='notify.bna@gmail.com', $subject='Báo Nghệ An gửi bảng kê nhuận bút') {
        return \Yii::$app->mailer->compose()
            ->setFrom(['notify.bna@gmail.com' => 'Báo Nghệ An'])
            ->setTo("{$to}")
            ->setSubject($subject)
            ->setTextBody('Xin chào!')
            ->setHtmlBody($content)
            ->send();
    }

    /**
     * Finds the Royalty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Royalty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Royalty::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
