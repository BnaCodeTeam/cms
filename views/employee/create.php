<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = Yii::t('app', 'Thêm nhân viên');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Danh sách nhân viên'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'userType' => $userType,
    ]) ?>

</div>
