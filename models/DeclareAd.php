<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "declare_ad".
 *
 * @property integer $id
 * @property string $ad_partner
 * @property integer $ad_type
 * @property integer $ad_from_time
 * @property integer $ad_to_time
 * @property string $ad_note
 * @property double $ad_price
 * @property integer $ad_created_by
 * @property integer $ad_pay_status
 * @property string $ad_pay_note
 * @property integer $remind_status
 * @property string $created_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $deploy_employee
 * @property double $amount_paid
 * @property string $declare_template
 * @property string $updated_by
 * @property string $is_order_paper
 * @property string $order_paper_number
 * @property string $order_paper_price
 * @property string $declare_ad_code
 * @property string $status
 * @property integer $ad_partner_id 
 */
class DeclareAd extends \yii\db\ActiveRecord
{
    const AD_TYPE_TIN_BAI = 1;
    const AD_TYPE_BANNER = 2;
    const AD_TYPE_CLIP = 3;
    const AD_TYPE_PACKAGE = 4;


    const PAY_STATUS_UNPAID = 1;//chưa thanh toán
    const PAY_STATUS_WAIT = 2;//đã thanh toán một phần
    const PAY_STATUS_PAID = 3;//đã thanh toán hết
    
    const REMIND_FAILED = 2;
    const REMIND_DONE = 1;
    
    const STATUS_WAIT = 1;
    const STATUS_DONE = 2;
    const STATUS_THANH_LY = 3;
    
    const IS_ORDER_PAPER = 1;
    const IS_NOT_ORDER_PAPER = 0;


    private $_employeeSearch;
    private $_employeeDeployString;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'declare_ad';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ad_type', 'ad_from_time', 'ad_to_time', 'ad_created_by', 'ad_pay_status', 
                'remind_status', 'status', 'created_at', 'updated_at', 'is_order_paper', 'order_paper_number', 'ad_partner_id'], 'integer'],
            
            [['ad_note', 'ad_pay_note', 'deploy_employee', 'AdFromTimeString', 
                'AdToTimeString', 'declare_template', 'declare_ad_code'], 'string'],
            
            [['ad_price', 'amount_paid', 'order_paper_price'], 'number'],
            [['ad_partner', 'created_by', 'updated_by'], 'string', 'max' => 255],
            [['ad_type', 'ad_partner_id'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ad_partner' => 'Đối tác',
            'ad_type' => 'Loại hình',
            'ad_from_time' => 'Bắt đầu',
            'ad_to_time' => 'Kết thúc',
            'ad_note' => 'Ghi chú nội dung triển khai',
            'ad_price' => 'Giá',
            'ad_created_by' => 'Người làm hợp đồng',
            'ad_pay_status' => 'Trạng thái thanh toán',
            'ad_pay_note' => 'Ghi chú thanh toán',
            'remind_status' => 'Tình trạng thông báo',
            'status' => 'Trạng thái',
            'created_by' => 'Tạo bởi',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'updated_by' => 'Cập nhật bởi',
            'deploy_employee' => 'Người triển khai',
            'is_order_paper' => 'Đăng ký đặt báo',
            'order_paper_number' => 'Số lượng báo đặt',
            'order_paper_price' => 'Giá báo đặt',
            
            'AdFromTimeString' => 'Ngày bắt đầu đăng',
            'AdToTimeString' => 'Ngày kết thúc đăng',
            'EmployeeSearch' => 'Người làm hợp đồng',
            'EmployeeDeployString' => 'Người triển khai',
            'amount_paid' => 'Số tiền đã thanh toán',
            'declare_template' => 'Mẫu hợp đồng',
            'templateFile' => 'Mẫu hợp đồng',
            'declare_ad_code' => 'Mã hợp đồng',
            'ad_partner_id' => 'Đối tác'
        ];
    }
    
    public function getAdFromTimeString() {
        if($this->ad_from_time) {
            return date('d-m-Y',$this->ad_from_time);
        }
        return '';
    }
    
    public function setAdFromTimeString($signString) {
        if($signString && trim($signString) != '') {
            $this->ad_from_time = strtotime($signString);
        }
    }
    
    public function getAdToTimeString() {
        if($this->ad_to_time) {
            return date('d-m-Y',$this->ad_to_time);
        }
        return '';
    }
    
    public function setAdToTimeString($timeString) {
        if($timeString && trim($timeString) != '') {
            $this->ad_to_time = strtotime($timeString);
        }
    }
    
    public function setEmployeeSearch($employeeSearch) {
        $this->_employeeSearch = $employeeSearch;
    }
    
    public function getEmployeeSearch() {
        return $this->_employeeSearch;
    }
    
    public function setEmployeeDeployString($employeeDeploy) {
        $this->_employeeDeployString = $employeeDeploy;
    }
    
    public function getEmployeeDeployString() {
        return $this->_employeeDeployString;
    }
    
    public function getEmployeeCreateInfo() {
        $data = '';
        $employee = Employee::findOne($this->ad_created_by);
        if($employee) {
            $data = $employee->full_name . ' (' . $employee->username . ')';
        }
        return $data;
    }
    
    public static function getRemindStatusLabels() {
        return [
            self::REMIND_DONE => 'Đã gửi thông báo',
            self::REMIND_FAILED =>'Gửi thông báo thất bại'
        ];
    }


    public function getEmployeeDeployInfo($withId = false) {
        $data = '';
        if($this->deploy_employee 
                && $this->deploy_employee != null 
                && trim($this->deploy_employee) != '') {
            $eIds = explode(',', $this->deploy_employee);
            if(!empty($eIds)) {
                $employees = Employee::find()
                        ->where(['id' => $eIds])
                        ->all();
                
                if($employees) {
                    $es = [];
                    foreach ($employees as $e) {
                        if($withId) {
                            $es[$e->id] = $e->full_name . ' (' . $e->username . ')';
                        } else {
                            $es[] = $e->full_name . ' (' . $e->username . ')';
                        }
                    }
                    if($withId) {
                        $data = $es;
                    } else {
                        $data = implode(', ', $es);
                    }
                }
            }
        }
        return $data;
    }
    
    public function viewPaidStatus() {
        if($this->ad_pay_status == DeclareAd::PAY_STATUS_WAIT) {
            $n = '';
            if($this->amount_paid && $this->amount_paid != null && $this->amount_paid > 0) {
                $n = ' (Đã thanh toán: '.number_format($this->amount_paid, 0, ',', '.').'đ)';
            }
            return DeclareAd::getPaidStatusLabels()[$this->ad_pay_status] . $n;
        } else {
            return (isset(DeclareAd::getPaidStatusLabels()[$this->ad_pay_status]))? 
                DeclareAd::getPaidStatusLabels()[$this->ad_pay_status] : '';
        }
    }
    
    
    /**
    * @var UploadedFile
    */
    public $templateFile;
    
    public function upload()
    {
        if ($this->validate()) {
            $path = 'uploads/attack_file/declare_ad/' . md5(Yii::$app->user->identity->username.
                    '_'.time().'_'.$this->templateFile->baseName).'.'. $this->templateFile->extension;
            $this->templateFile->saveAs($path, false);
            $this->declare_template = $path;
            return true;
        } else {
            return false;
        }
    }
    
    public function viewDeclareTemplate() {
        return $this->declare_template?'<a type="button" class="btn btn-primary" href="/'.
                $this->declare_template.'"><span class="glyphicon glyphicon-cloud-download" aria-hidden="true"></span> Tải hợp đồng</a>':
            '<label class="label label-default">Chưa có file hợp đồng</label>';
    }
    
    
    public function isOwner() {
        $userId = Yii::$app->user->identity->id;
        if($userId == $this->ad_created_by) {
            return true;
        } else {
            if($this->deploy_employee != null && $this->deploy_employee != '') {
                $deployStringArray = explode(',', $this->deploy_employee);
                $deployIntArray = [];
                if(!empty($deployStringArray)) {
                    foreach ($deployStringArray as $v) {
                        $deployIntArray[] = intval(trim($v));
                    }
                    if(in_array($userId, $deployIntArray)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    public function isDeployed() {
        if($this->status == DeclareAd::STATUS_DONE) {
            return true;
        } else {
            if($this->ad_type == DeclareAd::AD_TYPE_PACKAGE) {
                $declareInfo = DeclareAdPakageInfo::findAll(['declare_ad_id' => $this->id]);
                if($declareInfo) {
                    $arrayInfo = [];
                    $arrayChild = [];
                    foreach($declareInfo as $info) {
                        $arrayInfo[$info->ad_type] = $info->number;
                    }
                    $declareChild = DeclareAdChild::findAll(['declare_ad_id' => $this->id]);
                    if($declareChild) {
                        foreach($declareChild as $child) {
                            if(!isset($arrayChild[$child->ad_type])) {
                                $arrayChild[$child->ad_type] = 1;
                            } else {
                                $arrayChild[$child->ad_type] = $arrayChild[$child->ad_type] + 1;
                            }
                        }
                    }
                    
                    if(!empty($arrayInfo)) {
                        $res = true;
                        foreach($arrayInfo as $ki => $vi) {
                            if(!isset($arrayChild[$ki]) || $arrayChild[$ki] < $vi) {
                                $res = false;
                                break;
                            }
                        }
                        return $res;
                    }
                }
            }
        }
        return false;
    }
    
    public function isDone() {
        if($this->status == DeclareAd::STATUS_THANH_LY) {
            return true;
        }
        return false;
    }
    
    public function isExpired() {
        if($this->ad_to_time && $this->ad_to_time != null) {
            $today = strtotime('today');
            $exTime = $this->ad_to_time + 86400;
            if($today > $exTime) {
                $x = $today - $exTime;
                $n = ceil($x/86400);
                return $n;
            }
        }
        return false;
    }


    public static function getAdTypeLabels() {
        return [
            self::AD_TYPE_TIN_BAI => 'Tin bài lẻ',
            self::AD_TYPE_BANNER => 'Banner lẻ',
            self::AD_TYPE_CLIP => 'Clip lẻ',
            self::AD_TYPE_PACKAGE => 'Hợp đồng gói'
        ];
    }
    
    public static function getPaidStatusLabels() {
        return [
            self::PAY_STATUS_UNPAID => 'Chưa thanh toán',
            self::PAY_STATUS_WAIT => 'Đã thanh toán một phần',
            self::PAY_STATUS_PAID => 'Đã thanh toán đủ'
        ];
    }
    
    public static function getIsOrderPaperLabels() {
        return [
            self::IS_NOT_ORDER_PAPER => 'Không yêu cầu đặt báo',
            self::IS_ORDER_PAPER => 'Có yêu cầu đặt báo',
        ];
    }
    
    public static function getStatusLabels() {
        return [
            self::STATUS_WAIT => 'Chưa triển khai',
            self::STATUS_DONE => 'Đã triển khai',
            self::STATUS_THANH_LY => 'Đã thanh lý',
        ];
    }
    
    public function getAdPartner() {
        return $this->hasOne(AdPartner::className(), ['id' => 'ad_partner_id']);
    }
}
