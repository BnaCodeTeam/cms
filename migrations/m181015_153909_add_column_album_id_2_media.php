<?php

use yii\db\Migration;

/**
 * Class m181015_153909_add_column_album_id_2_media
 */
class m181015_153909_add_column_album_id_2_media extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('media', 'album_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('media', 'album_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181015_153909_add_column_album_id_2_media cannot be reverted.\n";

        return false;
    }
    */
}
