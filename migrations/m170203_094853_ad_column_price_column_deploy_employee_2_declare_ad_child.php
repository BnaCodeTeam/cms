<?php

use yii\db\Migration;

class m170203_094853_ad_column_price_column_deploy_employee_2_declare_ad_child extends Migration
{
    public function up()
    {
        $this->addColumn('declare_ad_child', 'deploy_employee', $this->string());
        $this->addColumn('declare_ad_child','total_price', $this->double());
    }

    public function down()
    {
        $this->dropColumn('declare_ad_child', 'deploy_employee');
        $this->dropColumn('declare_ad_child','total_price');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
