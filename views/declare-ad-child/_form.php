<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DeclareAdChild */
/* @var $form yii\widgets\ActiveForm */

use yii\jui\DatePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\web\View;

$todayString = date('d-m-Y', time());
?>

<?php 
$formatJs = <<< 'JS'
function IsNumeric(input){
    var RE = /^-{0,1}\d*\.{0,1}\d+$/;
    return (RE.test(input));
};

var formatEmployee = function(employee){
    if(employee.id != 'undefined' && IsNumeric(employee.id)) {
        var imx = 'uploads/images/users/no-user-image.png';
        if(employee.image != 'undefined' && employee.image != null) {
            imx = employee.image;
        }
        var ma = '<div class="row">' + 
                '<div class="col-sm-8">' +
                    '<img data-holder-rendered="true" style="width: 64px; height: 64px; data-src="holder.js/64x64" src="/' + imx + '" class="img-rounded" />' + 
                    '<b style="margin-left:5px">' + employee.text + '</b>' + 
                '</div>' + 
            '</div>';
        return '<div style="overflow:hidden;">' + ma + '</div>';
    }
};
 
var formatSelectionEmployee = function(employee) {
    return employee.full_name || employee.text;
};

function setRemoveDeployTag(eId) {
    $('#employee-deploy-tag-' + eId).remove();
    $('#employee-deploy-id-' + eId).remove();
}

var eventSelectEmployeeDeploy = function(item) {
    if(item.params.data.selected) {
        if(IsNumeric(item.params.data.id)) {
            $('#tags-deploy').append('<span onclick="setRemoveDeployTag(' + item.params.data.id + ')" class="label label-info tag-name-label" style="margin-right:3px; cursor:pointer" data-id="' + item.params.data.id + '" id="employee-deploy-tag-' + item.params.data.id + '" >' + item.params.data.text + '</span>');
            $('#tags-deploy').append('<input type="hidden" value="' + item.params.data.id + '" id="employee-deploy-id-' + item.params.data.id + '" name="DeclareAdChild[EmployeeDeployString][]">');
        }
    }
};

JS;

$this->registerJs($formatJs, View::POS_HEAD);
?>

<div class="declare-ad-child-form">

    <?php $form = ActiveForm::begin(); ?>
        <div class="table-responsive"> 
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <td>
                            <?= $form->field($model, 'ad_type')->dropDownList(app\models\DeclareAdPakageInfo::getAdtypeLabels(), ['prompt'=>'---- Chọn loại hình quảng cáo ----']) ?>
                        </td>
                        <td>
                            <?= $form->field($model,'AdFromTimeString')->widget(DatePicker::className(),['dateFormat' => 'dd-MM-yyyy','options' => ['class' => 'form-control'],'clientOptions' => ['defaultDate' => $todayString]]) ?>
                        </td>
                        <td>
                            <?= $form->field($model,'AdToTimeString')->widget(DatePicker::className(),['dateFormat' => 'dd-MM-yyyy','options' => ['class' => 'form-control'],'clientOptions' => ['defaultDate' => $todayString]]) ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1">
                            <?= $form->field($model, 'title')->textInput() ?>
                        </td>
                        <td colspan="1">
                            <?php $url = Yii::$app->urlManager->createUrl(['employee/ajax-get-employee-list']);?>
                            <?php echo $form->field($model, 'EmployeeDeployString')->widget(Select2::classname(), [
                                'initValueText' => '',
                                'options' => ['placeholder' => 'Tìm kiếm tác giả...'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 2,
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return 'Đang tìm dữ liệu...'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => $url,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                                        'delay' => 250,
                                    ],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult' => new JsExpression('formatEmployee'),
                                    'templateSelection' => new JsExpression('formatSelectionEmployee'),
                                    'tags' => true,
                                ],
                                'pluginEvents' => [
                                    'select2:select' => new JsExpression('eventSelectEmployeeDeploy'),
                                ]

                            ]);?>
                        </td>
                        <td colspan="1">
                            <div class="form-group" id="tags-deploy">
                                <?php
                                if($model->deploy_employee && $model->deploy_employee != null) {
                                    $deployment = app\models\Employee::getEmployeeDeployInfo(true,$model->deploy_employee);
                                    if(is_array($deployment) && !empty($deployment)) {
                                        foreach($deployment as $i => $de) {
                                            echo '<span onclick="setRemoveDeployTag('.$i.')" class="label label-info tag-name-label" style="margin-right:3px; cursor:pointer" data-id="' .$i. '" id="employee-deploy-tag-' .$i. '" >' .$de. '</span>';
                                            echo '<input type="hidden" value="' .$i. '" id="employee-deploy-id-' .$i. '" name="DeclareAdChild[EmployeeDeployString][]">';
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1">
                            <?= $form->field($model, 'total_price')->textInput() ?>
                        </td>
                        <td colspan="2">
                            <?= $form->field($model, 'ad_note')->textarea(['rows' => 6]) ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div class="form-group">
                                <?= Html::submitButton($model->isNewRecord ? 'Tạo mới' : 'Cập nhật', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

    <?php ActiveForm::end(); ?>

</div>
