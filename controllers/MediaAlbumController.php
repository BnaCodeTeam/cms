<?php

namespace app\controllers;

use Yii;
use app\models\MediaAlbum;
use app\models\MediaAlbumSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Media;
use yii\web\UploadedFile;
use app\components\AdminController;

ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3000); 
/**
 * MediaAlbumController implements the CRUD actions for MediaAlbum model.
 */
class MediaAlbumController extends AdminController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    
    /**
     * Displays a single MediaAlbum model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    /**
     * Updates an existing MediaAlbum model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $mediaModel = new Media();

        if ($model->load(Yii::$app->request->post())) {
            if($model->author_id == NULL) $model->author_id = Yii::$app->user->identity->id;
            $model->updated_by = Yii::$app->user->identity->username;
            if($model->save()) {
                $this->updateCategoryOfMediaByAlbum($model);
                $this->updateAuthorOfMediaByAlbum($model);
                $mediaModel->multiMediaFiles = UploadedFile::getInstances($mediaModel, 'multiMediaFiles');
                $uploadData = $mediaModel->multiUpload();
                if ($uploadData && is_array($uploadData) && !empty($uploadData)) {
                    foreach($uploadData as $data) {
                        if(isset($data['url']) && $data['url'] != '') {
                            $media = new Media();
                            $media->url = $data['url'];
                            $media->created_by = Yii::$app->user->identity->username;
                            $media->updated_by = Yii::$app->user->identity->username;
                            $media->album_id = $model->id;
                            $media->title = $data['base_name'];
                            $media->description = $model->description;
                            $media->category_id = $model->category_id;
                            $media->author_id = $model->author_id;
                            if(isset($data['type'])) {
                                $type = Media::TYPE_VIDEO;
                                if($data['type'] == 'images') {
                                    $type = Media::TYPE_PHOTO;
                                }
                                $media->type = $type;
                            }
                            $media->save();
                        }
                    }
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'media' => $mediaModel
        ]);
    }
    
    public function actionAjaxUpdateUpload() {
        $model = new Media();
        $model->multiMediaFiles = UploadedFile::getInstances($model, 'multiMediaFiles');
        $uploadData = $model->multiUpload();
        
        $return = [];
        if ($uploadData && is_array($uploadData) && !empty($uploadData)) {  
            foreach($uploadData as $data) {
                if(isset($data['url']) && $data['url'] != '') {
                    $media = new Media();
                    $media->load(['Media' => Yii::$app->request->post()]);
                    $media->url = $data['url'];
                    $media->created_by = Yii::$app->user->identity->username;
                    $media->updated_by = Yii::$app->user->identity->username;
                    $media->title = $data['base_name'];
                    if(isset($data['type'])) {
                        $type = Media::TYPE_VIDEO;
                        if($data['type'] == 'images') {
                            $type = Media::TYPE_PHOTO;
                        }
                        $media->type = $type;
                    }
                    if($media->save()){
                        $return[] = $media->id;
                    }
                }
            }         
        }
        return json_encode($return);
    }
    
    public function actionUpdateAjax($id) {
        $model = $this->findModel($id);
        $this->checkAccess($model, 'update');
        $mediaModel = new Media();

        if ($model->load(Yii::$app->request->post())) {
            if($model->author_id == NULL) $model->author_id = Yii::$app->user->identity->id;
            $model->updated_by = Yii::$app->user->identity->username;
            if($model->save()) {
                $this->updateCategoryOfMediaByAlbum($model);
                $this->updateAuthorOfMediaByAlbum($model);
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update-ajax', [
            'model' => $model,
            'media' => $mediaModel
        ]);
    }

    /**
     * Deletes an existing MediaAlbum model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if(Yii::$app->user->identity->username != 'admin' 
            && $model->created_by != Yii::$app->user->identity->username) {
            throw new \yii\web\ForbiddenHttpException('Bạn không có quyền xóa Album này'); 
        }
        $model->status = MediaAlbum::STATUS_DELETE;
        if($model->save()) {
            Media::updateAll(['status' => Media::STATUS_DELETE], ['album_id' => $model->id, 'status' => Media::STATUS_ACTIVE]);
        }

        return $this->redirect(['media/album']);
    }

    /**
     * Finds the MediaAlbum model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MediaAlbum the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MediaAlbum::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    protected function updateCategoryOfMediaByAlbum($album) {
        $condition = ['and',
            //['<>', 'category_id', $album->category_id],
            ['or', ['<>', 'category_id', $album->category_id], ['category_id' => null]],
            ['in', 'album_id', [$album->id]],
        ];
        Media::updateAll(['category_id' => $album->category_id], $condition);
    }
    
    protected function deleteMediaByAlbum($albumId) {
        
    }
    
    protected function updateAuthorOfMediaByAlbum($album){
        $condition = ['and',
            //['<>', 'category_id', $album->category_id],
            ['or', ['<>', 'author_id', $album->author_id], ['author_id' => null]],
            ['in', 'album_id', [$album->id]],
        ];
        Media::updateAll(['author_id' => $album->author_id], $condition);
    }
    
    protected function checkAccess($model, $action) {
        if(Yii::$app->user->identity->username != 'admin' 
            && $model->created_by != Yii::$app->user->identity->username) {
            throw new \yii\web\ForbiddenHttpException('Bạn không có quyền '.$action.' album này'); 
        }
    }
}
