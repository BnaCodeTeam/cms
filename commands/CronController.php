<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\db\Query;
use \Yii;

Yii::setAlias('@webdir', realpath(dirname(__FILE__).'/../web'));

class CronController extends Controller
{
    const DOMAIN = 'http://quanly.bna.vn';
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";
    }
    
    /**
     * Gui email cho nguoi dung khi quang cao cua nguoi do sap het han
     */
    public function actionSendAdNotify($type = 1) {
        $numberDay = 30;
        if($type == 0) {
            $numberDay = 7;
            //echo $numberDay;
        }
        $domain = self::DOMAIN;
        $todayTime = strtotime('today');
        $exTime = $todayTime + $numberDay*24*60*60;
        $q = \app\models\DeclareAd::find()
                ->where("ad_to_time <= {$exTime}")
                ->andWhere("remind_status IS NULL");
                
        if($type == 0) {
            $q->andWhere('ad_type != '.\app\models\DeclareAd::AD_TYPE_PACKAGE);
        } else {
            $q->andWhere('ad_type = '.\app\models\DeclareAd::AD_TYPE_PACKAGE);
        }
                
        $remindAd = $q->all();
        
                
        if($remindAd) {
            foreach($remindAd as $remind) {
                $employeeId = [];
                if($remind->ad_created_by && $remind->ad_created_by != null) {
                    $employeeId[$remind->ad_created_by] = $remind->ad_created_by;
                }
                if($remind->deploy_employee && $remind->deploy_employee != null) {
                    $tg = explode(",", $remind->deploy_employee);
                    if(is_array($tg) && !empty($tg)) {
                        foreach($tg as $id) {
                            $employeeId[$id] = $id;
                        }
                    }
                }
                $stsPaid = (isset(\app\models\DeclareAd::getPaidStatusLabels()[$remind->ad_pay_status]))? 
                                \app\models\DeclareAd::getPaidStatusLabels()[$remind->ad_pay_status] : '';
                
                if(!empty($employeeId)) {
                    $employees = \app\models\Employee::find()->where(['id' => $employeeId, 'status' => \app\models\Employee::STATUS_ACTIVE])->all();
                    if($employees) {
                        foreach($employees as $ep) {
                            \Yii::$app->mailer->compose()
                                ->setFrom(['notify.bna@gmail.com' => 'Quản lý quảng cáo Báo Nghệ An'])
                                ->setTo("{$ep->email}")
                                ->setSubject('Quảng cáo của bạn sắp hết hạn')
                                ->setTextBody('Xin chào!')
                                ->setHtmlBody(''
                                        . '<p><b>Xin chào!</b></p>'
                                        . '<p>Bạn có một quảng cáo sẽ hết hạn trong vòng '.$numberDay.' ngày nữa. '
                                        . 'Xin vui lòng kiểm tra lại hợp đồng với đối tác để thực hiện thủ tục thanh lý hợp đồng</p>'
                                        . '<p><b>Tóm tắt hợp đồng</b></p>'
                                        . '<p>Mã hợp đồng: <b>#' .$remind->id. '</b></p>'
                                        . '<p>Đơn vị: ' .$remind->ad_partner. '</p>'
                                        . '<p>Trạng thái thanh toán: ' .$stsPaid. '</p>'
                                        . '<p>Vui lòng click <a href="'.$domain.'/declare-ad/view2?id='.$remind->id.'">vào đây</a> để xem chi tiết</p>')
                                ->send(); 
                        }
                        $remind->remind_status = \app\models\DeclareAd::REMIND_DONE;
                        $remind->save();
                    }
                }
            }
        }
    }
    
    /**
     * Gui email khi tai khoan cua nguoi dung duoc giao nhiem vu lam quang cao
     * Chu thich: tai khoan nguoi dung da duoc tao roi, tuy nhien khi nguoi do 
     * co du an quang cao thi he thong se thong bao
     */
    public function actionSendUserLogin() {
        $stringWhere = $this->getResponsibleEmployee(true);
        if($stringWhere != '') {
            //$queryEmail = new Query;
            $queryEmail = \app\models\Employee::find()
                    ->where('is_send_active = '.\app\models\Employee::IS_NOT_SEND_ACTIVE)
                    ->andWhere('id IN('.$stringWhere.')')
                    ->andWhere('status = '.\app\models\Employee::STATUS_ACTIVE);
            
            $data = $queryEmail->all();
            //print_r($data);
            if(!empty($data)) {
                foreach($data as $ep) {
                    \Yii::$app->mailer->compose()
                        ->setFrom(['notify.bna@gmail.com' => 'Hệ thống Báo Nghệ An'])
                        ->setTo("{$ep->email}")
                        ->setSubject('Tài khoản của bạn đã được tạo trên hệ thống Báo Nghệ An')
                        ->setTextBody('Xin chào '.$ep->full_name.'!')
                        ->setHtmlBody(''
                                . '<p><b>Xin chào '.$ep->full_name.'!</b></p>'
                                . '<p>Tài khoản của bạn đã được tạo</p>'
                                . '<p>Vui lòng truy cập '.self::DOMAIN.'</p>'
                                . '<p>Tên đăng nhập: '.$ep['email'].'</p>'
                                . '<p>Mật khẩu: '.\app\models\Employee::DEFAULT_PASSWORD.'</p>'
                                . '<p><b>Vui lòng đổi mật khẩu sau lần đăng nhập đầu tiên</b></p>')
                        ->send(); 
                        
                    $ep->is_send_active = \app\models\Employee::IS_SEND_ACTIVE;
                    $ep->save(true, ['is_send_active']);
                }
            }
        }
    }
    
    /**
     * Nhac nho trien khai cho nhung hop dong chua trien khai va sap het han
     */
    public function actionSendAdDeployNotify() {
        $employeeIds = $this->getResponsibleEmployee(true, 'ad_type != '.
                \app\models\DeclareAd::AD_TYPE_PACKAGE.' AND remind_status = '.
                \app\models\DeclareAd::REMIND_DONE.' AND status = '.
                \app\models\DeclareAd::STATUS_WAIT);
        
        if($employeeIds != '') {
            $query = \app\models\Employee::find()
                    ->where('id IN('.$employeeIds.')')
                    ->andWhere('status = '.\app\models\Employee::STATUS_ACTIVE);
            
            $data = $query->all();
            
            if(!empty($data)) {
                foreach($data as $ep) {
                    \Yii::$app->mailer->compose()
                        ->setFrom(['notify.bna@gmail.com' => 'Quản lý quảng cáo Báo Nghệ An'])
                        ->setTo("{$ep->email}")
                        ->setSubject('Bạn có hợp đồng quảng cáo chưa triển khai')
                        ->setTextBody('Xin chào '.$ep->full_name.'!')
                        ->setHtmlBody(''
                                . '<p><b>Xin chào '.$ep->full_name.'!</b></p>'
                                . '<p>Bạn có hợp đồng quảng cáo chưa được triển khai, vui lòng triển khai hợp đồng trước khi hết hạn.</p>'
                                . '<p>Nếu bạn đã triển khai hợp đồng nhưng vẫn nhận được thông báo này, vui lòng liên hệ với phòng Phát Hành & Quảng Cáo</p>'
                                . '<p>Vui lòng truy cập '.self::DOMAIN.' để theo dõi hợp đồng của bạn</p>')
                        ->send();
                }
            }
        }
    }
    
    /**
     * Nhac nho thanh toan cho nhung hop dong da trien khai va da het han
     */
    public function actionSendAdPaidNotify() {
        
    }
    
    /**
     * Chuyển dữ liệu đối tác quảng cáo sang bảng quản lý đối tác. Action này chỉ chạy duy nhất một lần. Sau đó mãi mãi không dùng nữa
     */
    public function actionTransferAdPartner() {
        /*$query = new Query;
        $query->select('id, ad_partner')
            ->from('declare_ad');
        $rows = $query->all();*/
        $rows = \app\models\DeclareAd::find()->all();
        foreach($rows as $row) {
            $partnerModel = new \app\models\AdPartner();
            $partnerModel->name = $row->ad_partner;
            $partnerModel->created_at = time();
            $partnerModel->created_by = 'admin';
            $partnerModel->updated_at = time();
            $partnerModel->updated_by = 'admin';
            if($partnerModel->save()) {
                $row->ad_partner_id = $partnerModel->id;
                $row->save(true,['ad_partner_id']);
            }
        }
    }


    /**
     * 
     * @param type $isString
     * @return array or string of employee id
     * 
     */
    protected function getResponsibleEmployee($isString=false, $where='') {
        $query = new Query;
        $query->select('ad_created_by,deploy_employee')
            ->from(\app\models\DeclareAd::tableName());
        if($where != '') {
            $query->where($where);
        }
        $rows = $query->all();
        $employee = [];
        if($rows) {
            foreach($rows as $r) {
                if($r['ad_created_by'] != null && $r['ad_created_by'] != '') {
                    $employee[$r['ad_created_by']] = $r['ad_created_by'];
                }
                if($r['deploy_employee'] != null && $r['deploy_employee'] != '') {
                    $ar = explode(",", $r['deploy_employee']);
                    foreach($ar as $a) {
                        $x = trim($a);
                        $employee[$x] = $x;
                    }
                }
            }
        }
        if(!$isString) {
            return $employee;
        }
        $stringWhere = '';
        if(!empty($employee)) {
            $stringWhere = implode(",", $employee);
        }
        return $stringWhere;
    }
    
    
    public function actionDeleteMedia() {
        $mediaUrls = \app\models\Media::find()->select(['url'])->where(['status' => \app\models\Media::STATUS_DELETE])->all();
        if($mediaUrls && !empty($mediaUrls)) {
            foreach ($mediaUrls as $url) {
                $file = Yii::getAlias('@webdir'.$url->url);
                echo $file . "\n";
                if(file_exists($file)) {
                    echo ((unlink($file))? 'Deleted':'')."\n";
                }
            }
        }
    }
}
