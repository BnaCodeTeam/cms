<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DeclareAdChild */

$backUrl = Yii::$app->urlManager->createUrl(['declare-ad/view', 'id' => $model->declare_ad_id]);
$this->title = 'Cập nhật mục con: #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Hợp đồng quảng cáo #'.$model->declare_ad_id, 'url' => [$backUrl]];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Cập nhật';
?>
<div class="declare-ad-child-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
