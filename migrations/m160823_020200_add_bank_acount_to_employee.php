<?php

use yii\db\Migration;
use \yii\db\Schema;

class m160823_020200_add_bank_acount_to_employee extends Migration
{
    public function up()
    {
        $this->addColumn("employee", "bank_account", Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn("employee", "bank_account");
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
