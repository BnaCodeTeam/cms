<?php

use yii\db\Migration;

class m161024_034054_add_amount_paid_to_declare_ad extends Migration
{
    public function up()
    {
        $this->addColumn('declare_ad', 'amount_paid', $this->double());
    }

    public function down()
    {
        return $this->dropColumn('declare_ad', 'amount_paid');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
