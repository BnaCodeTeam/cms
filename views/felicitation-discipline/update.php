<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FelicitationDiscipline */

$this->title = 'Cập nhật quyết định số: ' . $model->number;
$this->params['breadcrumbs'][] = ['label' => 'Khen thưởng kỷ luật', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Cập nhật';
?>
<div class="felicitation-discipline-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'assignData' => $assignData
    ]) ?>

</div>
