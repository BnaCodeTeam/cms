<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DeclareAdChildSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Declare Ad Children';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="declare-ad-child-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Declare Ad Child', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'declare_ad_id',
            'ad_type',
            'ad_from_time:datetime',
            'ad_to_time:datetime',
            // 'ad_note:ntext',
            // 'created_by',
            // 'updated_by',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
