<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MediaAlbum */
/* @var $form yii\widgets\ActiveForm */

use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\web\View;
?>

<?php 
$formatJs = <<< 'JS'
function IsNumeric(input){
    var RE = /^-{0,1}\d*\.{0,1}\d+$/;
    return (RE.test(input));
};
var formatEmployee = function(employee){
        if(employee.id != 'undefined' && IsNumeric(employee.id)) {
            var imx = 'uploads/images/users/no-user-image.png';
            if(employee.image != 'undefined' && employee.image != null) {
                imx = employee.image;
            }
            var ma = '<div class="row">' + 
                    '<div class="col-md-12">' +
                        '<img data-holder-rendered="true" style="width: 64px; height: 64px; data-src="holder.js/64x64" src="/' + imx + '" class="img-rounded" />' + 
                        '<b style="margin-left:5px">' + employee.text + '</b>' + 
                    '</div>' + 
                '</div>';
            return '<div style="overflow:hidden;">' + ma + '</div>';
        }
    };
    
var formatSelectionEmployee = function(employee) {
    return employee.full_name || employee.text;
};


var eventSelectEmployee = function(item) {
    if(item.params.data.selected) {
        console.log(item.params.data);
    }
};
JS;

$this->registerJs($formatJs, View::POS_HEAD);?>

<div class="media-album-form">

    <?php $form = ActiveForm::begin(); ?>

    
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'category_id')->dropDownList([''=>'---- Chọn chuyên mục ----'] + \app\models\MediaCategory::getListCategory()) ?></div>
        <div class="col-md-6">
            <?php $url = Yii::$app->urlManager->createUrl(['employee/ajax-get-employee-list']);?>
            <?php echo $form->field($model, 'author_id')->widget(Select2::classname(), [
                'initValueText' => app\models\Employee::getAliasAuthorById($model->author_id),
                'options' => ['placeholder' => 'Chọn tác giả'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 2,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Đang tìm dữ liệu...'; }"),
                    ],
                    'ajax' => [
                        'url' => $url,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                        'delay' => 250,
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('formatEmployee'),
                    'templateSelection' => new JsExpression('formatSelectionEmployee'),
                    'tags' => true,
                    //'width' => '300px',
                ],
                'pluginEvents' => [
                    'select2:select' => new JsExpression('eventSelectEmployee'),
                ]

            ]);?>
        </div>
    </div>
    
    <?php 
        echo $form->field($media, 'multiMediaFiles[]')->widget(FileInput::classname(), [
            'options' => ['multiple' => true],
        ]); 
    ?>
    
    <?php if(!$model->isNewRecord && !empty($model->media)):?>
    <div class="row">
        <?php foreach($model->media as $media):?>
        <?php echo $this->render('//media/_img', ['model' => $media]); ?>
        <?php endforeach;?>
    </div>
    <?php endif;?>
    
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $this->registerJsFile('/js/media-item.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>