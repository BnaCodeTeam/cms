<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "action_log".
 *
 * @property integer $id
 * @property string $action
 * @property string $object_data
 * @property integer $result_data
 * @property string $action_user
 * @property integer $created_at
 */
class ActionLog extends \yii\db\ActiveRecord
{
    const RESULT_SUCCESS = 1;
    const RESULT_ERROR = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'action_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['result_data', 'created_at'], 'integer'],
            [['action', 'object_data', 'action_user'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'action' => 'Action',
            'object_data' => 'Object Data',
            'result_data' => 'Result Data',
            'action_user' => 'Action User',
            'created_at' => 'Created At',
        ];
    }
    
    public static function trackActionLog($data) {
        $action = Yii::$app->controller->id."/".Yii::$app->controller->action->id;
        $actionLogModel = new ActionLog();
        $actionLogModel->action = $action;
        $actionLogModel->action_user = Yii::$app->user->identity->username;
        $actionLogModel->created_at = time();
        if($data) {
            if(isset($data['result'])) {
                $actionLogModel->result_data = $data['result'];
            }
            if(isset($data['object'])) {
                $actionLogModel->object_data = $data['object'];
            }
        }
        $actionLogModel->save();
    }
    
    public static function getActionResultLabels() {
        return [
            self::RESULT_SUCCESS => 'Thành công',
            self::RESULT_ERROR => 'Thất bại'
        ];
    }
}
